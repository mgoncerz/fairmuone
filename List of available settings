At the moment, the document is meant to provide the list and description of settings that may be useful for the average user.
Some, which will be set or provided by people responsible for them (e.g. module digitization or alignment job settings) are hence omitted to keep things as transparent and breve as possible.
The analysis job settings are also omitted, as the current implementation is just a skeleton to be developed further in the future.

If a setting is not included in the configuration, a default optimized value will be used instead.

1. Detector configuration files

Stored in common/geometry.

  The detector configuration comprises 4 sections and a loose setting positioning the calorimeter. The first three sections define the default geometry and digitization settings for the tracking modules, calorimeter and targets. The last one defines the actual detector layout.
  All of the settings can be overriden on a per-element basis by repeating them in the configuration of that particular element.
  An exception to this rule are targets, which require adding an additional section to the station configuration (called "targetConfiguration"), in which the settings overriding target geometry for that station can be included.

  For a general configuration file layout, please see one of the included examples, which you can copy and modify to your needs.


  The materials for the detector are defined in the media.geo file. The format is explained on the FairRoot page -- https://fairroot.gsi.de/index.html%3Fq=node%252F34.html
  Detector cave dimensions are stored in cave.geo, which you generally shouldn't have to worry about.



a) modulesConfiguration/geometry

  - twoSensorsPerModule [default = true] -- boolean, if true (it is by default) each module is in the CMS 2S configuration, otherwise each module is a single sensor
  - sensorMaterial [no default value] -- string with name of one of the materials defined in the media.geo file
  - sensorSizeMeasurementDirection [no default value] -- double [cm], sensor length in the direction perpendicular to strips
  - sensorSizePerpendicularDirection [no default value] -- double [cm], sensor length in the direction palallel to strips
  - sensorThickness [no default value] -- double [cm], thickness of a single sensor
  - distanceBetweenSensorCenters [no default value, not used if twoSensorsPerModule is false] -- double [cm], distance between midpoints (in the beam direction) of both sensors in the module; note it corresponds to the spacing between the sensors + 2 * their thickness
  - numberOfStrips [no default value] -- integer, number of strips (value shared for both sensors in a module)


c) targetsConfiguration

  - targetMaterial [no default value] -- string with name of one of the materials defined in the media.geo file
  - width [no default value] -- double [cm], target length in the x direction
  - height [no default value] -- double [cm], target length in the y direction
  - thickness [no default value] -- double [cm], target thickness

d) calorimeterConfiguration/geometry

  - crystalMaterial [no default value] -- string with name of one of the materials defined in the media.geo file
  - crystalWidthUpstream [no default value] -- double [cm], length of the upstream crystal side in the x direction
  - crystalHeightUpstream [no default value] -- double [cm], length of the upstream crystal side in the y direction
  - crystalWidthDownstream [no default value] -- double [cm], length of the downstream crystal side in the x direction
  - crystalHeightDownstream [no default value] -- double [cm], length of the downstream crystal side in the y direction
  - crystalThickness [no default value] -- double [cm], crystal thickness
  - numberOfColumns [no default value] -- integer, number of columns in the calorimeter grid
  - numberOfRows [no default value] -- integer, number of rows in the calorimeter grid
  - distanceBetweenCrystalCenters [no default value] -- double [cm], distance between centers of consecutive crystals in the grid (assumed to be the same in x and y direction) 


f) detector/projections

  These refer to the measurement directions of the modules. They are fixed for all four (x, y, u, v), but two things can be changed:

  - tilt [default = 0], double [degrees], angle at which the module is tilted with respect to the beam axis
  - negateDataHitPosition [default = false], bool, used to align the direction of FairMUonE axes with directions assumed for collected data (it multiplies the corresponding position from hits in reconstruction input by -1)

g) detector/stations

  The detector layout is defined in the stations section. The topmost level is a list of stations.
  Each station is defined by its position, target position and optionally its configuration (if targetConfiguration is present), and a list of modules belonging to that station.
  If target position is not set, the station will be simulated without it.

  Throughout this section, everything can be defined in any order. Keep in mind though, that each station and each module will be later automatically sorted by the Z position and all indices in the output ROOT file will refer to a SORTED list.
  For example, if you define the first (based on position along the beam axis) station as last, it will still have stationIndex 0. Indexing always starts with 0.

  Each station has these settings available:

  - origin [default = 0] -- double [cm], the absolute origin of the station in Z direction (note it does not define anything by itself, but all other positions are relative to it)
  - targetRelativePosition [default = 0] -- double [cm], position of the target midpoint relative to station origin; if not set, the station is created without a target
  - targetConfiguration -- an optional list with settings from the default targetsConfiguration section, which will override the defaults for this target
  - modules -- list of modules in a given station, each with these settings available:
    - projection [default = 'x'] -- char [x, y, u or v], the measurement direction of the module
    - relativePosition [default = 0] -- double [cm], position in the Z direction, relative to the station origin
    - hitResolution [default = 0] -- double [cm], measurement resolution; used as uncertainty in track fitting and typically will be determined by the alignment and already set for you; note that it has to be set to a value other than default one and that it impacts only event reconstruction and NOT simulation (the resolution in simulation is a consequence of geometry and digitization settings)
    As before, each setting from the default settings sections can be included here to override it for that particular module.
    
h) detector/calorimeterPosition 

  This loose setting in the detector section positions the calorimeter (it is the geometrical midpoint in the beam direction and the position is absolute).
  If it is not present, the detector will be simulated without the calorimeter. There isn't any way of "overriding" the settings, as there is only one calorimeter in the detector and the settings are defined in the initial sections.
    

2. Geant4 configuration files

  Stored in common/gconfig.

  You generally don't have to worry about these as the default configuration provided in the repository is fine for most use cases.
  The structure mimics a Geant4 configuration, while giving access to all of its commands ran both as input and post initialization commands (see the default file).

  Potentially useful:
    - the MonteCarlo_Process section can be used to enable/disable simulation of select processes
    - the physics list can be changed with Geant4_PhysicsList, which may be used to switch to less accurate EM interactions when trying to simulate the calorimeter quickly without producing a myriad of tracks


3. Data processing jobs

  Stored in common/jobs.

  Only production jobs will be considered. The alignment will be done for you and analysis classes are not fully implemented at the moment.
  The production jobs are used to generate Monte Carlo samples and reconstruct real data. They will typically differ in content depending on what you want to achieve, but the general layout is always the same. The pipeline is flexible and may be run from and up to any step, provided it makes sense. For example, if you want to run reconstruction step only, you have to provide an ntuple with either digitization output or an ntuple of collected data. Similarily, you won't be able to run only generation and reconstruction, without the digitization step in-between.

  A small caveat is that if you don't run everything at once and want to use the output ntuple as input later, the things needed for the subsequent steps have to be saved in that ntuple. I will make a note about these in their description. If you're trying to run further processing of an input ntuple and you are getting weird crashes everytime, this is very likely the reason as it will crash if it can't find the necessary content in the ntuple. If you are instead running everything at once, you don't need to worry about this as the necessary branches will be passed through in RAM as needed.

  The error checking is pretty robust and should not let you run a job which does not make sense, while at the same time providing enough feedback to find the problem easily. Don't be afraid to try running a job (e.g. with just 1 event), if you're not sure if the config is fine. It's usually the easiest way to find out. The values used for each setting will also be printed to the terminal, before the run starts.

  It is also assumed that some settings will be ignored whether you set them or not, if they have no use for that particular job you're running. For example, if you disable reconstruction step, there is no need to remove the reconstruction settings completely.

  As always, it is best to look into the existing configuration files to get a sense of how they look and copy/modify one for your needs.

  The general config layout comprises a short section with the most general settings (e.g. output file name and number of events), followed by a series of on/off flags for each of the processing stages (generation, digitization, reconstruction, event filtering) and their corresponding configurations.

  The general settings are:

    - outputFile [no default value] -- string, name of the output ROOT file
    - saveParametersFile [default = true] -- bool, saves detector geometry parameters required for current event display (small in size, will let you draw the detector geometry in event display)
    - saveGeoTracks [default = false] -- bool, leave at false, can get big if enabled; was previously required to draw tracks in the event display, but since the display doesn't draw tracks at the moment... :)
    - numberOfEvents [default = 0] -- integer, number of events to generate/process; if you are processing an existing input ntuple, -1 means all available events
    - detectorConfiguration [no default value] -- string, name of the detector configuration file in common/geometry (without the .yaml extension)
    - seed [default = 42] -- integer, seed used to initialize random number generator; keep it relatively small when using Mesmer event generator as it is passed there (as per instruction in Mesmer repository)
    - inputDirectory [no default value] -- string, path to directory with input files (used when running without generation)
    - inputFiles [no default value] -- a comma-separated list of strings inside square brackets; filenames of files in inputDirectory that are to be processed; ['*'] can be be given as "all files in the directory", but note the necessary quote marks, as * is normally a YAML special character

  The flags controlling which stages will be run are:

    - runGeneration [default = false] -- bool, enables/disables the generation stage 
    - runDigitization [default = false] -- bool, enables/disables the digitization stage 
    - runReconstruction [default = false] -- bool, enables/disables the reconstruction stage
    - runEventFilter [default = false] -- bool, enables/disables the event filter stage

  The remaining part are sections with configuration for each step.

a) generationConfiguration

  - geantConfiguration [default = default config] -- string, name of the Geant4 configuration file (without the .yaml extension), in most cases the default one is fine and you don't have to worry about it
  - saveTrackerPoints [default = true] -- bool, enables/disables saving of Geant-level tracker points to the output ntuple (required for tracker digitization if starting from existing ntuple)
  - saveCalorimeterPoints [default = true] -- bool, enables/disables saving of Geant-level calorimeter points to the output ntuple (required for calorimeter digitization if starting from existing ntuple)
  - saveMCTracks [default = true] -- bool, enables/disables saving of Monte Carlo tracks container to the output ntuple
  - saveSignalTracks [default = true] -- bool, enables/disables saving of SignalTracks container to the output ntuple (it contains copies of all tracks added by the event generators, but not secondaries from detector simulation)
  - savePileupTracks [default = true] -- bool, enables/disables saving of PileupTracks container to the output ntuple (it contains copies of all pileup tracks added by the generators, excluding the primary track which is chosen to generate the interaction)
  - pileupMean [default = disabled] -- double, Poisson mean for pileup generation; set to 0 or negative to disable pileup

  - beamGenerator [no default value] -- string, selects one of the generator adding primary muon tracks, available values -- ParticleGunBox, ParticleGunBeamProfile, MuonGunBox, PionGunBox, MuonGunBeamProfile, PionGunBeamProfile
  The beam generator determines the kinematics of the beam particles. The two generic choices are ParticleGunBox (initial position drawn randomly from a rectangle) and ParticleGunBeamProfile (entire kinematics drawn from chosen beam profile ROOT file),
  the rest are exactly the same, but with some of the options fixed so that they don't have to be provided explicitly.

  The settings are defined in a generationConfiguration/beamGeneratorConfiguration subsection.

  The settings for box generators are (neither of them has a meaningful default value, all have to be defined):

    - xMin, xMax, yMin, yMax -- double [cm], size of the initial beam position rectangle
    - z -- double [cm], position at which the tracks are generated (i.e. where the rectangle is spanned)
    - pdg -- integer, particle code (fixed for named generators)
    - energy -- double [GeV], beam energy (box particle guns are monoenergetic)
    - mass -- double [GeV], mass of the particle (fixed for named generators)
    - thetaMin, thetaMax -- double [rad], the arc in which the tracks will be generated, a uniform distribution around the beam axis is always assumed for box generators

  The settings for BeamProfile generators are (likewise, the main three have to be provided):
    - pdg
    - mass
    - beamProfile -- string, name of the ROOT file with beam profile to be used
    - xOffset [default = 0] -- double [cm], shifts position distribution by a constant value
    - yOffset [default = 0] -- double [cm], shifts position distribution by a constant value

  named generators (e.g. MuonGunBox) do not require pdg and mass, as they are fixed.
  The Z position for beam profile generators is fixed to 0 and the detector has to be positioned accordingly.



  - forceInteraction [default = false] -- bool, enables/disables generating interactions using one of the vertex generators
  This setting controls whether you will be generating minimum bias events (i.e. only adding the beam track and letting Geant4 do the rest), or if you want to use one of the vertex generators (e.g. Mesmer) to produce chosen interaction in chosen target volume each event.

  - vertexStationIndex [no default value] -- integer, selects the station (or rather the corresponding target) in which the interaction vertices will be generated
  
  - vertexGenerator [no default value] -- string, selects one of the generators for vertices, available values -- Mesmer

  At the moment, we only have the Mesmer generator interfaced. Its settings can be set in the generationConfiguration/vertexGeneratorConfiguration subsection.
  These settings have exactly the same names and possible values as described in the Mesmer documentation. The default Mesmer values will also be used if setting is not provided.

  For typical use, the important ones are:
    - ord, born/alpha/alpha2 for LO/NLO/NNLO events
    - mode, weighted/unweighted for weighted and unweighted events


b) digitizationConfiguration

  There are two subsections: tracker and calorimeter. The only settings you need to worry about (the remaining ones should be left with default values, unless you know what you're doing):

  For tracker:
    - saveStripDigis [default = true] -- bool, decides if a container of structures corresponding to fired sensor strips should be saved in the final ntuple
    This is generally used for debugging and if you are not sure if you need it, you can safely set it to false to save space. It's NOT needed for subsequent steps.
    - saveStubs [default = true] -- bool, saves the "hit" structures combining information from both sensors (required for reconstruction if starting from existing ntuple)

  For calorimeter:
    - saveDeposit [default = true] -- bool, saves the calorimeter deposits for each crystal to the ntuple (required for reconstruction if detector layout has a calorimeter)


f) reconstructionConfiguration

  The most important settings are (although in most cases the default settings or ones already provided will be best):

    - isMC [default = true] -- bool, has to be set depending on whether real data or MC samples are processed (the software will crash if you try to process real data with isMC: true and vice versa)
    - verbose [default = false] -- bool, enables/disables printing of additional info, which is mostly more detailed information on the 2D and 3D tracks reconstructed in each event; can be safely set to false for typical use
    - alignmentFile [default = disabled] -- string, name of the file with alignment corrections from common/alignment (note that using alignment for MC is also allowed, although in most cases you shouldn't have this setting present for MC)

    - maxOutlierChi2 [default = disabled] -- double, max chi2 ( (hit-track distance/error)^2 ) that hits can have in a fitted 3D track to not be rejected during the fit
  Note that this is not perfectly well defined, because the chi2 at this point does not include multiple scattering correction, hence the value has to be tuned in simulation.
  Can be set to -1 to disable hit rejection completely.

    - maxNumberOfSharedHits [default = 2] -- integer, max number of hits that two tracks can share
  This is used in clone track removal, where the tracks are first sorted from best to worst by quality and the tracks which share more than this set number of hits with a better one are removed.
  Can be set to -1 for a different approach which instead removes from track each hit it shares with a better one and reject tracks that are no longer valid in the process.

    - restrictHitSharingToFirstNModulesInSector [default = 2] -- integer, can be used to restrict the allowed hit sharing to the first N module of each station, i.e.
  the track is rejected if it shares more than maxNumberOfSharedHits in modules up to restrictHitSharingToFirstNModulesInSector or any hits in modules above restrictHitSharingToFirstNModulesInSector.
  If set to -1, the sharing is allowed in all modules of the station.

    - vertexSplittingAngleThreshold [default = disabled] -- double [radians], if set to a positive value, it will split each vertex with both tracks scattered at a lower angle into two hypothesis -- one where the track with lower scattering angle is considered to be the muon and the other where it's inverted. Both will then be processed as two distinct vertices, with the one where assumption is inverted having the alternativePIDHypothesis flag set to true.

    - addBaselineMSCorrectionToAllTracks [default = true] -- bool, if set to true, all 3D and 2D track fits include multiple scattering uncertainty assuming that each track is the beam muon with average beam momentum, otherwise they only contain module measurement uncertainties
    - MSCorrectionAssumedBeamEnergy [default = 160] -- double, sets the assumed energy for said correction (in GeV)

  The three settings below control whether the full multiple scattering uncertainty will be used during the kinematic fit (calculated under the PID assumption for a given vertex).
  Note that if the baseline correction is enabled, it will be carried over to the kinematic fit. These settings then control whether it should be overriden with a more accurate ony.
    - refitIncomingTrackWithMSCorrection [default = true] -- bool
    - refitMuonTrackWithMSCorrection [default = true] -- bool
    - refitElectronTrackWithMSCorrection [default = true] -- bool

  Each vertex has its position estimated first and then undergoes the kinematic fit. The settings below control whether the position from the initial position estimation will be taken into the kinematic fit, which refits all tracks together while forcing them to go through a common point in the target.
  If they are disabled, the kinematic fit assumes the vertex at the Z midpoint of the target and fits the (x, y) position.
    - useFittedZPositionInKinematicFit [default = true] -- bool, use only Z position and fit (x, y)
    - useFittedPositionInKinematicFit [default = false] -- bool, use full (x, y, z) position and only fit track parameters
    - restrictVertexPositionToTarget [default = true] -- bool, if enabled the fitted position will be restricted to target dimensions before it is taken into the kinematic fit; the position from position fit will be untouched (i.e. can be outside)


  The algorithm can also reconstruct vertices with more than 2 outgoing tracks. However, keep in mind this is done by brute forcing all combinations and can generate many candidates in complex events with a lot of tracks.
  Due to lack of PID at the moment, all tracks are also treated equally in the fitting procedure and no multiple scattering correction other than the baseline one is included.
  It is enabled by setting:
    - maxGenericVertexOutgoingTrackMultiplicity [default = disabled] -- integer, enabled if value greater than 2

  These "generic vertices" also have setting analogues:
    - useFittedZPositionInGenericVertexKinematicFit [default = false]
    - useFittedPositionInGenericVertexKinematicFit [default = false]
    - restrictGenericVertexPositionToTarget [default = false]


  Three settings are also provided to handle very complex events:
    - maxVertexChi2PerDegreeOfFreedom [default = disabled] -- double, the vertex candidates with chi2/ndf after the kinematic fit above this value will not be saved in the output ntuple
    - maxGenericVertexChi2PerDegreeOfFreedom [default = disabled] -- double, same as above but for generic vertices
    - skipEventsWithMoreThanNHits [default = disabled] -- integer, skip processing of events with more than N hits completely; such events will have the eventSkipped flag set to true, but will still be saved into the output ntuple (just without the reconstruction results)



  Less useful settings (specialized or default value is more than fine):
    - xyHitAssignmentWindow [default = 0.2] -- double [cm], controls the maximum hit-track distance for the hit to be assigned to the track candidate 
    - uvHitAssignmentWindow [default = 0.3] -- double [cm], as above but for stereo modules
    - reassignHitsAfterKinematicFit [default = false] -- bool, if enabled and hit sharing between tracks is allowed, it will try to reassign hits after the KF and if anything changed refit the vertex; doesn't seem to have any impact at the moment
    - allowTrivialIncomingTracks [default = false] -- bool, allows track candidates with no stereo hits in the very first station
    - ingoreLastStationInVertexing [default = false] -- bool, excludes the very last station from the reconstruction algorithm, so that it can be used as an improvised muon detector
    - useSeedingSensorOnly [default = false] -- bool, if set to true, only the seeding sensor of each module will be used for reconstruction
    - runKinematicFit [default = true] -- bool, enables/disables the kinematic fit; if disabled, vertices will still be formed and their position estimated, but the kinematic fit will not be performed (i.e. the tracks will have parameters from the initial independent track fit)



  There is also an adaptive fitter that in principle could reconstruct events with variable number of outgoing tracks, selected by a seeding procedure.
  It is not working particularly well, nor tuned at the moment.
    - runAdaptiveVertexFitter [default = false] -- bool, enables/disables adaptive fitter run on all tracks in a station
    - disableAdaptiveFitterSeeding [default = true] -- bool, disables the seeding stage and uses all tracks in a given station
    - adaptiveFitterSeedWindow [default = 0.2] -- double [cm], maximum window size for combining tracks into seeds 
    - adaptiveFitterSeedTrackWindow [default = 0.3] -- double [cm], maximum window size for assigning tracks to seeds
    - adaptiveFitterMaxNumberOfIterations [default = 20] -- integer, maximum number of iterations to achieve convergence
    - adaptiveFitterMaxTrackChi2 [default = 25] -- double [cm], maximum chi2 considered for the Tukey weights
    - adaptiveFitterConvergenceMaxDeltaZ [default = 0.01] -- double [cm], delta Z at which convergence is assumed


