#!/bin/bash

cd $1

rm -rf plots
mkdir plots
mkdir plots/results

declare -a theta_ranges=("0_5" "5_10" "10_15" "15_20" "20_25" "25_32")
theta_ranges_length=${#theta_ranges[@]}

declare -a shared_hits=("0" "1" "2")
shared_hits_length=${#shared_hits[@]}

#validation_resolution
for ((i=0; i<${theta_ranges_length}; i++));
do

    for ((j=0; j<${shared_hits_length}; j++));
    do

        th=${theta_ranges[$i]}
        sh=${shared_hits[$j]}
        recofile=$1/reconstructed_${th}_${sh}.root
        genfile=$1/generated_${th}.root

        root -q -l -b resolutions.cpp'("'${recofile}'","'${genfile}'",'"$sh"',"'${th}'", "'${2}'","'${1}/plots'")'

    done
done


#validation_efficiency
for ((i=0; i<${theta_ranges_length}; i++));
do

    for ((j=0; j<${shared_hits_length}; j++));
    do

        th=${theta_ranges[$i]}
        sh=${shared_hits[$j]}
        recofile=$1/reconstructed_${th}_${sh}.root
        genfile=$1/generated_${th}.root

        root -q -l -b efficiency_MC.cpp'("'${recofile}'","'${genfile}'",'"$sh"',"'${th}'", "'${2}'","'${1}/plots'")'

    done
done

#validation_fake
for ((i=0; i<${theta_ranges_length}; i++));
do

    for ((j=0; j<${shared_hits_length}; j++));
    do

        th=${theta_ranges[$i]}
        sh=${shared_hits[$j]}
        recofile=$1/reconstructed_${th}_${sh}.root
        genfile=$1/generated_${th}.root

        root -q -l -b fake_rates_and_quality_prevrtx.cpp'("'${recofile}'","'${genfile}'",'"$sh"',"'${th}'", "'${2}'","'${1}/plots'")'

    done
done

#check_res
root -q -l -b plots_fit_resolutions.cpp'("'${1}/plots/'", "'${2}'")'

#check_eff
root -q -l -b plots_012hits_eff.cpp'("'${1}/plots/'", "'${2}'")'

#check_res_pre_post
root -q -l -b plots_pre_post_vrtx.cpp'("'${1}/plots/'", "'${2}'")'

#check_fake
root -q -l -b plots_fake.cpp'("'${1}/plots/'", "'${2}'")'
