import argparse

parser = argparse.ArgumentParser()

parser.add_argument("workpath", type=str)

parser.add_argument("-n", "--numberOfEvents", default=1000, type=int)
parser.add_argument("-dc", "--detectorConfiguration", default="TR2023_geometry_3cmTarget_noECAL", type=str)
parser.add_argument("--ord", default="alpha", type=str)

args = parser.parse_args()
print("Generating production file, arguments used:")
print(args)

theta_ranges = [
    (0, 5),
    (5, 10),
    (10, 15),
    (15, 20),
    (20, 25),
    (25, 32)
]

for lower_theta, upper_theta in theta_ranges:

    config = """

outputFile: {0}.root
numberOfEvents: {1}
seed: 42

saveParametersFile: false
saveGeoTracks: false
detectorConfiguration: {2}


runGeneration: true
generationConfiguration:
    geantConfiguration: geant4DefaultConfiguration
    saveTrackerPoints: true
    saveCalorimeterPoints: false
    saveMCTracks: true
    saveSignalTracks: true
    savePileupTracks: false
    pileupMean: 0 

    beamGenerator: ParticleGunBeamProfile
    beamGeneratorConfiguration:
        beamProfile: beamProfile_narrow
        pdg: -13
        mass: 0.1056583715

    forceInteraction: true
    vertexStationIndex: 1
    vertexGenerator: Mesmer
    vertexGeneratorConfiguration:
        mode: weighted
        ord: {3}
        nwarmup: 10000
        Qmu: 1
        Eemin: 0.2
        themin: {4}
        themax: {5}

runDigitization: true
digitizationConfiguration:
    tracker:
        saveStripDigis: true
        saveStubs: true
        maxNumberOfLinkedTracks: -1
    calorimeter:
        saveDeposit: true

runReconstruction: false
runEventFilter: false 
    """.format(
            "{0}/generated_{1}_{2}".format(args.workpath, lower_theta, upper_theta), 
            args.numberOfEvents, 
            args.detectorConfiguration, 
            args.ord, 
            lower_theta, 
            upper_theta
    )


    with open("{0}/generate_{1}_{2}.yaml".format(args.workpath, lower_theta, upper_theta), 'w') as f:
        f.write(config)