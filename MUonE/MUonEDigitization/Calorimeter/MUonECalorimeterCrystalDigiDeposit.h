#ifndef MUONECALORIMETERCRYSTALDIGIDEPOSIT_H
#define MUONECALORIMETERCRYSTALDIGIDEPOSIT_H

#include "TObject.h"
#include "TRandom.h"
#include "TMath.h"

class MUonECalorimeterCrystalDigiDeposit : public TObject {

public:

    MUonECalorimeterCrystalDigiDeposit() = default;

    MUonECalorimeterCrystalDigiDeposit(Int_t crystalColumnID_, Int_t crystalRowID_, Double_t d = 0) : crystalColumnID(crystalColumnID_), crystalRowID(crystalRowID_), EnergyDeposit(d)  {  }

    void resetEnergyDeposit() {EnergyDeposit = 0;}
    void addEnergyDeposit(Double_t d) {EnergyDeposit += d;}

    void smearEnergy(Double_t resolutionPercentOverRootE) { 
      SmearedEnergy = gRandom->Gaus(EnergyDeposit, 
        resolutionPercentOverRootE/100*TMath::Sqrt(EnergyDeposit)); 
   }

    Int_t columnID() const {return crystalColumnID;}
    Int_t rowID() const {return crystalRowID;}
    Double_t energyDeposit() const {return EnergyDeposit;}
    Double_t smearedEnergy() const {return SmearedEnergy;}

private:
    Int_t crystalColumnID{-1};
    Int_t crystalRowID{-1};
    Double_t EnergyDeposit{0};
    Double_t SmearedEnergy{0};

    ClassDef(MUonECalorimeterCrystalDigiDeposit, 1)
};

#endif //MUONECALORIMETERCRYSTALDIGIDEPOSIT_H



