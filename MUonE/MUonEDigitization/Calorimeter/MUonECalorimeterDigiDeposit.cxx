#include "Rtypes.h"

#include "MUonECalorimeterDigiDeposit.h"

MUonECalorimeterCrystalDigiDeposit *MUonECalorimeterDigiDeposit::findCrystal(Int_t crystalColumnID, Int_t crystalRowID) const
{
  for(int i = 0; i < Crystals.GetEntries(); i++)
  {
    MUonECalorimeterCrystalDigiDeposit *crystal = (MUonECalorimeterCrystalDigiDeposit *) Crystals.At(i);
    if(crystal->columnID() == crystalColumnID && crystal->rowID() == crystalRowID)
    {
      return crystal;
    }
  }

  return nullptr;
}

void MUonECalorimeterDigiDeposit::addEnergyDeposit(Int_t crystalColumnID, Int_t crystalRowID, Double_t d)
{
  MUonECalorimeterCrystalDigiDeposit *crystal = findCrystal(crystalColumnID, crystalRowID); 
  if(crystal) 
  {
    crystal->addEnergyDeposit(d);
  } 
  else 
  {
    int newIndex = Crystals.GetEntries();
    new(Crystals[newIndex]) MUonECalorimeterCrystalDigiDeposit(crystalColumnID, crystalRowID, d);
  }

}

Double_t MUonECalorimeterDigiDeposit::energyDeposit(Int_t crystalColumnID, Int_t crystalRowID) const
{
  MUonECalorimeterCrystalDigiDeposit *crystal = findCrystal(crystalColumnID, crystalRowID); 
  if(crystal) 
    return crystal->energyDeposit();
  else
    return 0;
}

void MUonECalorimeterDigiDeposit::smearEnergies(Double_t resolutionPercentOverRootE)
{
  for(int i = 0; i < Crystals.GetEntries(); i++) 
  {
     MUonECalorimeterCrystalDigiDeposit *crystal = (MUonECalorimeterCrystalDigiDeposit *) Crystals.At(i);
     crystal->smearEnergy(resolutionPercentOverRootE);
  } 
}

Double_t MUonECalorimeterDigiDeposit::totalEnergyDeposit() const
{
  double totalEnergy = 0;

  for(int i = 0; i < Crystals.GetEntries(); i++) 
  {
     MUonECalorimeterCrystalDigiDeposit *crystal = (MUonECalorimeterCrystalDigiDeposit *) Crystals.At(i);
     totalEnergy += crystal->energyDeposit();
  } 

  return totalEnergy;
}

Double_t MUonECalorimeterDigiDeposit::totalSmearedEnergy() const
{
  double totalEnergy = 0;

  for(int i = 0; i < Crystals.GetEntries(); i++) 
  {
     MUonECalorimeterCrystalDigiDeposit *crystal = (MUonECalorimeterCrystalDigiDeposit *) Crystals.At(i);
     totalEnergy += crystal->smearedEnergy();
  } 

  return totalEnergy;
}
