#ifndef MUONEDIGITIZATIONCONFIGURATION_H
#define MUONEDIGITIZATIONCONFIGURATION_H

#include "MUonETrackerDigitizationConfiguration.h"
#include "MUonECalorimeterDigitizationConfiguration.h"


#include <yaml-cpp/yaml.h>
#include "Rtypes.h"
#include <string>

class MUonEDigitizationConfiguration {

public:

    MUonEDigitizationConfiguration();

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    MUonETrackerDigitizationConfiguration const& tracker() const {return m_tracker;}
    MUonECalorimeterDigitizationConfiguration const& calorimeter() const {return m_calorimeter;}
    


    void resetDefaultConfiguration();

private:

    Bool_t m_configuredCorrectly{false};

    MUonETrackerDigitizationConfiguration m_tracker;
    MUonECalorimeterDigitizationConfiguration m_calorimeter;

    ClassDef(MUonEDigitizationConfiguration, 2)
};


#endif //MUONEDIGITIZATIONCONFIGURATION_H

