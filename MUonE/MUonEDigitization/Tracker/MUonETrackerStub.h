#ifndef MUONETRACKERSTUB_H
#define MUONETRACKERSTUB_H

#include "Rtypes.h"
#include "TObject.h"
#include "MUonETrackerCluster.h"


#include <set>
#include <array>
#include <unordered_map>




class MUonETrackerStub : public TObject{

public:

    MUonETrackerStub() = default;

    //2 sensors/module constructor
    MUonETrackerStub(Int_t stationID, Int_t moduleID, Int_t sensorHalf, MUonETrackerCluster const& seeding_cluster, MUonETrackerCluster const& correlation_cluster, Double_t window_offset)
        :  TObject(), 
        StationID(stationID), ModuleID(moduleID), SensorHalf(sensorHalf),
        CbcID(seeding_cluster.cbcID()), SeedClusterCenterStrip(seeding_cluster.centerStrip()), SeedReducedChannel(seeding_cluster.centerReducedChannel()), 
        Bend(correlation_cluster.centerStrip() - (seeding_cluster.centerStrip() + window_offset)),
        SeedClusterWidth(seeding_cluster.width()), CorrelationClusterWidth(correlation_cluster.width())
        {
            linkMCTracks(seeding_cluster.hitStrips(), correlation_cluster.hitStrips());
        }

    //1 sensor/module constructor
    MUonETrackerStub(Int_t stationID, Int_t moduleID, Int_t sensorHalf, MUonETrackerCluster const& cluster)
        :  TObject(), 
        StationID(stationID), ModuleID(moduleID), SensorHalf(sensorHalf),
        CbcID(cluster.cbcID()), SeedClusterCenterStrip(cluster.centerStrip()), SeedReducedChannel(cluster.centerReducedChannel()), 
        Bend(0),
        SeedClusterWidth(cluster.width()), CorrelationClusterWidth(0)
        {
            linkMCTracks(cluster.hitStrips(), /*std::set<const MUonETrackerStripDigi*>()*/ {});
        }



	Int_t stationID() const {return StationID;}
	Int_t moduleID() const {return ModuleID;}
	Int_t sensorHalf() const {return SensorHalf;}

    Int_t cbcID() const {return CbcID;}

    Double_t seedClusterCenterStrip() const {return SeedClusterCenterStrip;}
    Double_t seedReducedChannel() const {return SeedReducedChannel;}
    Double_t bend() const {return Bend;}
    void setBend(Double_t bend_) {Bend = bend_;}

    Double_t seedClusterWidth() const {return SeedClusterWidth;}
    Double_t correlationClusterWidth() const {return CorrelationClusterWidth;}

    std::vector<Int_t> const& seedingClusterLinkedTrackIds() const {return SeedingClusterLinkedTrackIds;}
    std::vector<Double_t> const& seedingClusterLinkedTrackDeposits() const {return SeedingClusterLinkedTrackDeposits;}
    std::vector<Int_t> const& correlationClusterLinkedTrackIds() const {return CorrelationClusterLinkedTrackIds;}
    std::vector<Double_t> const& correlationClusterLinkedTrackDeposits() const {return CorrelationClusterLinkedTrackDeposits;}

    Bool_t operator<(const MUonETrackerStub& other) const
    {
        return seedReducedChannel() < other.seedReducedChannel();
    }

private:

	Int_t StationID{-1};
	Int_t ModuleID{-1};
	Int_t SensorHalf{-1};

    Int_t CbcID{-1};

    Double_t SeedClusterCenterStrip{-1};
    Double_t SeedReducedChannel{-1};
    Double_t Bend{0};

    Double_t SeedClusterWidth{0};
    Double_t CorrelationClusterWidth{0};

    std::vector<Int_t> SeedingClusterLinkedTrackIds;
    std::vector<Double_t> SeedingClusterLinkedTrackDeposits;
    std::vector<Int_t> CorrelationClusterLinkedTrackIds;
    std::vector<Double_t> CorrelationClusterLinkedTrackDeposits;
    

    void linkMCTracks(std::set<const MUonETrackerStripDigi*> const& seeding_strips, std::set<const MUonETrackerStripDigi*> const& correlation_strips) {


        //for linking
        auto fillDeposits = [](std::set<const MUonETrackerStripDigi*> const& strips, std::vector<MUonETrackDeposit>& deposits_to_fill) {

            deposits_to_fill.clear();
            deposits_to_fill.reserve(5);

            std::unordered_map<Int_t, Int_t> id_map;//key - TrackID, val - index in the vector

            for(auto const& strip : strips) {
                    
                for(auto const& it : strip->deposits().trackDeposits()) {

                    auto track_id = it.trackID();
                    auto track_dep = it.depositedCharge();

                    //try to map current track id to the end of the deposits vector
                    std::pair<std::unordered_map<Int_t, Int_t>::iterator, bool> tmp_ret = id_map.insert({track_id, deposits_to_fill.size()});

                    if(!tmp_ret.second) //track id already mapped to some position in the vector
                        deposits_to_fill[tmp_ret.first->second].addDeposit(track_dep); //increment that deposit
                    else //newly added track (to the map), add it to the vector
                        deposits_to_fill.emplace_back(track_id, track_dep);
                }
            }
            //sort deposits by deposited charge
            std::sort(deposits_to_fill.begin(), deposits_to_fill.end(), [](const MUonETrackDeposit& lhs, const MUonETrackDeposit& rhs){return lhs.depositedCharge() > rhs.depositedCharge();});
        };

        std::vector<MUonETrackDeposit> seeding;
        std::vector<MUonETrackDeposit> correlation;
        
        fillDeposits(seeding_strips, seeding);
        fillDeposits(correlation_strips, correlation);

        for(auto const& s : seeding) {

            SeedingClusterLinkedTrackIds.push_back(s.trackID());
            SeedingClusterLinkedTrackDeposits.push_back(s.depositedCharge());
        }

        for(auto const& c : correlation) {

            CorrelationClusterLinkedTrackIds.push_back(c.trackID());
            CorrelationClusterLinkedTrackDeposits.push_back(c.depositedCharge());
        }
    }

	ClassDef(MUonETrackerStub,2)

};

#endif //MUONETRACKERSTUB_H

