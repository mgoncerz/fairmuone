#include "MUonECalorimeter.h"

#include "MUonEStack.h"

#include "FairVolume.h"
#include "FairGeoVolume.h"
#include "FairGeoNode.h"
#include "FairRootManager.h"
#include "FairGeoLoader.h"
#include "FairGeoMedia.h"
#include "FairGeoBuilder.h"
#include "FairGeoInterface.h"
#include "FairRun.h"
#include "FairRuntimeDb.h"

#include "TClonesArray.h" 
#include "TGeoManager.h" 


#include "TGeoBBox.h"
#include "TGeoTrd2.h"
#include "TVirtualMC.h"
#include "TParticle.h"


MUonECalorimeter::MUonECalorimeter()
    : FairDetector("MUonECalorimeter", true, DET_CALORIMETER), //name, active, id (see MUonEDetectorConfiguration.h)
    m_calorimeterPoints(new TClonesArray("MUonECalorimeterPoint"))
    {}


MUonECalorimeter::MUonECalorimeter(MUonEDetectorConfiguration const& detectorConfiguration, Bool_t saveCalorimeterPoints)
    : FairDetector("MUonECalorimeter", true, DET_CALORIMETER), //name, active, id (see MUonEDetectorConfiguration.h)
    m_calorimeterPoints(new TClonesArray("MUonECalorimeterPoint"))
    {
        setConfiguration(detectorConfiguration, saveCalorimeterPoints);
    }


MUonECalorimeter::~MUonECalorimeter() {

    if (m_calorimeterPoints) {
        m_calorimeterPoints->Delete();
        delete m_calorimeterPoints;
        m_calorimeterPoints = nullptr;
    }
}

void MUonECalorimeter::setConfiguration(MUonEDetectorConfiguration const& detectorConfiguration, Bool_t saveCalorimeterPoints) {
    
    m_detectorConfiguration = detectorConfiguration;
    m_saveCalorimeterPoints = saveCalorimeterPoints;
}



void MUonECalorimeter::Register() {

	FairRootManager::Instance()->Register("CalorimeterPoints", FairDetector::GetName(), m_calorimeterPoints, m_saveCalorimeterPoints ? kTRUE : kFALSE); //kTRUE = store in ntuple
}


TClonesArray* MUonECalorimeter::GetCollection(Int_t iColl) const {

    if(0 == iColl)
        return m_calorimeterPoints;
    else
        return NULL;
}


void MUonECalorimeter::ConstructGeometry() {

    m_idMap.clear();

    if(!m_detectorConfiguration.hasCalorimeter())
        return;

    auto calorimeter_geometry = m_detectorConfiguration.calorimeterConfiguration().geometry();

    if(0 == calorimeter_geometry.numberOfRows() || 0 == calorimeter_geometry.numberOfColumns())
        return;

    auto crystal_material_name = calorimeter_geometry.crystalMaterial().c_str();
    InitMedium(crystal_material_name);
    auto crystal_material = gGeoManager->GetMedium(crystal_material_name);

    auto crystal_width_upstream = calorimeter_geometry.crystalWidthUpstream();
    auto crystal_width_downstream = calorimeter_geometry.crystalWidthDownstream();
    auto crystal_height_upstream = calorimeter_geometry.crystalHeightUpstream();
    auto crystal_height_downstream = calorimeter_geometry.crystalHeightDownstream();
    auto crystal_thickness = calorimeter_geometry.crystalThickness();
    TGeoVolume* crystal_volume = new TGeoVolume("crystal_volume", new TGeoTrd2(
                                            0.5 * crystal_width_upstream, 0.5 * crystal_width_downstream,
                                            0.5 * crystal_height_upstream, 0.5 * crystal_height_downstream,
                                            0.5 * crystal_thickness
                                        ), crystal_material);

    crystal_volume->SetLineColor(3); //green
    AddSensitiveVolume(crystal_volume);     


    //get pointer to detector cave
    TGeoVolume* top = gGeoManager->GetTopVolume();

    auto calorimeter_position = m_detectorConfiguration.calorimeterPosition();
    auto number_of_columns = calorimeter_geometry.numberOfColumns();
    auto number_of_rows = calorimeter_geometry.numberOfRows();
    auto distance_between_crystal_centers = calorimeter_geometry.distanceBetweenCrystalCenters();

    /*
        Note that if the number of crystals in a row/column is even, the calorimeter midpoint in (x,y) will be in the space between two crystals,
        otherwise it will be in the center of a crystal.

        In the first case (even, |_._|__|_._|_^_|_._|__|_._|), the number of crystals to one side of the midpoint is 0.5 * N_crystals and the first of them has its midpoint
        0.5 * distance_between_crystal_centers to this side. 
        To get to the midpoint of the leftmost crystal, we need to move 0.5*distance_between_crystal_centers to get to the center of the first crystal 
        and then (0.5 * N_crystals - 1) * distance_between_crystal_centers to get to the center of the last crystal.
        The position of the center of the leftmost crystal is thus -1 * [0.5 + 0.5 * N_crystals - 1] * distance_between_crystal_centers.

        In the second case (odd, |_._|__|_._|__|_^_|__|_._|__|_._|), the number of crystals to one of the sides is equal to floor(0.5 * N_crystals)
        and the distance to move is floor(0.5 * N_crystals) * distance_between_crystal_centers. For N_crystals = 1, it's 0.
    */

    Double_t bottom_left_midpoint_x = 0;
    Double_t bottom_left_midpoint_y = 0;

    if(0 == number_of_columns % 2)
        bottom_left_midpoint_x = - (0.5 + 0.5 * number_of_columns - 1) * distance_between_crystal_centers;
    else 
        bottom_left_midpoint_x = - floor(0.5 * number_of_columns) * distance_between_crystal_centers;

    if(0 == number_of_rows % 2)
        bottom_left_midpoint_y = - (0.5 + 0.5 * number_of_rows - 1) * distance_between_crystal_centers;
    else 
        bottom_left_midpoint_y = - floor(0.5 * number_of_rows) * distance_between_crystal_centers;

    /*
        Crystal IDs are assigned assuming (0, 0) to be the bottom left crystal.
        Only crystal copy ids should be positive.
    */

    Int_t crystal_copy_id = 0;
    for(Int_t row_index = 0; row_index < number_of_rows; ++row_index) {
        for(Int_t column_index = 0; column_index < number_of_columns; ++column_index) {

            Double_t current_midpoint_x = bottom_left_midpoint_x + column_index * distance_between_crystal_centers;
            Double_t current_midpoint_y = bottom_left_midpoint_y + row_index * distance_between_crystal_centers;
            
            auto crystal_transformation = new TGeoTranslation(current_midpoint_x, current_midpoint_y, calorimeter_position);

            top->AddNode(crystal_volume, crystal_copy_id, crystal_transformation);
            m_idMap.insert({crystal_copy_id, {column_index, row_index}});
            ++crystal_copy_id;
        }
    }
}

Bool_t MUonECalorimeter::ProcessHits(FairVolume *v) {

    /** This method is called from the MC stepping */

    //all geometry other than crystals should have negative IDs
    Int_t volume_copy_number = v->getCopyNo();
    if(volume_copy_number < 0)
        return kTRUE;

	// track entering volume
	if ( TVirtualMC::GetMC()->IsTrackEntering()) {


		m_trackTotalEnergyLoss  = 0;

        Double_t global_position[3];
        Double_t local_position[3];
        
		TVirtualMC::GetMC()->TrackPosition(global_position[0], global_position[1], global_position[2]);
        m_trackEnteringPositionGlobalCoordinates = TVector3(global_position[0], global_position[1], global_position[2]);

        TVirtualMC::GetMC()->Gmtod(global_position, local_position, 1);
        m_trackEnteringPositionLocalCoordinates = TVector3(local_position[0], local_position[1], local_position[2]);
	}

    // track inside volume, add energy loss from current step
    m_trackTotalEnergyLoss += TVirtualMC::GetMC()->Edep();
    
    // track leaving volume
    if( TVirtualMC::GetMC()->IsTrackExiting()    ||
        TVirtualMC::GetMC()->IsTrackStop()       ||
        TVirtualMC::GetMC()->IsTrackDisappeared()) {


			if(m_trackTotalEnergyLoss == 0. ) {return kFALSE;}

            //get copy number (this is the number we set during geometry construction)
            Int_t crystal_copy_number = volume_copy_number;
            auto const& ids = m_idMap[crystal_copy_number];

            Int_t track_ID = TVirtualMC::GetMC()->GetStack()->GetCurrentTrackNumber();
			Int_t track_pdg_code = TVirtualMC::GetMC()->GetStack()->GetCurrentTrack()->GetPdgCode();


            //calculate track entering position
            Double_t global_position[3];
            Double_t local_position[3];
            
            TVirtualMC::GetMC()->TrackPosition(global_position[0], global_position[1], global_position[2]);
            auto track_exiting_position_global_coordinates = TVector3(global_position[0], global_position[1], global_position[2]);

            TVirtualMC::GetMC()->Gmtod(global_position, local_position, 1);
            auto track_exiting_position_local_coordinates = TVector3(local_position[0], local_position[1], local_position[2]);

            //add point to container
	        TClonesArray& clref = *m_calorimeterPoints;
	        Int_t size = clref.GetEntriesFast();

            new(clref[size]) MUonECalorimeterPoint(track_ID, track_pdg_code, ids, 
                m_trackEnteringPositionGlobalCoordinates, track_exiting_position_global_coordinates,
                m_trackEnteringPositionLocalCoordinates, track_exiting_position_local_coordinates,
                m_trackTotalEnergyLoss);
    }
    
    return kTRUE;
}

void MUonECalorimeter::Reset() {

    m_calorimeterPoints->Clear();
}

void MUonECalorimeter::EndOfEvent() {

    m_calorimeterPoints->Clear();
}

//taken from examples, no need to read into it too much
Int_t MUonECalorimeter::InitMedium(const char* name)
{
	static FairGeoLoader* geoLoad = FairGeoLoader::Instance();
	static FairGeoInterface* geoFace = geoLoad->getGeoInterface();
	static FairGeoMedia* media = geoFace->getMedia();
	static FairGeoBuilder* geoBuild = geoLoad->getGeoBuilder();
    
	FairGeoMedium* ShipMedium = media->getMedium(name);
    
	if (!ShipMedium)
	{
		Fatal("InitMedium","Material %s not defined in media file.", name);
		return -1111;
	}

	TGeoMedium* medium=gGeoManager->GetMedium(name);

	if (medium!=NULL)
		return ShipMedium->getMediumIndex();

    return geoBuild->createMedium(ShipMedium);
}

ClassImp(MUonECalorimeter)


