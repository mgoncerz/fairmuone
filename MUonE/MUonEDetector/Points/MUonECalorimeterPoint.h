#ifndef MUONECALORIMETERPOINT_H
#define MUONECALORIMETERPOINT_H

/*
    Container storing information about the calorimeter's response to a track passing through.
*/

#include "MUonEDetectorPoint.h"
#include <utility>


class MUonECalorimeterPoint : public MUonEDetectorPoint {


public:

    MUonECalorimeterPoint() = default;

    MUonECalorimeterPoint(Int_t trackID, Int_t trackPDGCode, std::pair<Int_t, Int_t> const& ids, 
        TVector3& enteringPositionGlobalCoordinates, TVector3& exitingPositionGlobalCoordinates,
        TVector3& enteringPositionLocalCoordinates, TVector3& exitingPositionLocalCoordinates,
        Double_t totalEnergyDeposit)
        : MUonEDetectorPoint(trackID, trackPDGCode,
        enteringPositionGlobalCoordinates, exitingPositionGlobalCoordinates,
        enteringPositionLocalCoordinates, exitingPositionLocalCoordinates,
        totalEnergyDeposit), 
        CrystalColumnID(ids.first), CrystalRowID(ids.second)
        {}


	Int_t crystalColumnID() const {return CrystalColumnID;}
	Int_t crystalRowID() const {return CrystalRowID;}
	
private:

	Int_t CrystalColumnID{0};
	Int_t CrystalRowID{0};
    

	ClassDef(MUonECalorimeterPoint,2)
};

#endif //MUONECALORIMETERPOINT_H

