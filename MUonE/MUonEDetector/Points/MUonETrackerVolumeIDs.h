#ifndef MUONETRACKERVOLUMEIDS_H
#define MUONETRACKERVOLUMEIDS_H

#include "Rtypes.h"

class MUonETrackerVolumeIDs {

public:

    MUonETrackerVolumeIDs() = default;

    MUonETrackerVolumeIDs(Int_t stationID, Int_t moduleID, Int_t sensorID)
        : m_stationID(stationID), m_moduleID(moduleID), m_sensorID(sensorID)
        {}

    Int_t stationID() const {return m_stationID;}
    Int_t moduleID() const {return m_moduleID;}
    Int_t sensorID() const {return m_sensorID;}


private:

    Int_t m_stationID{-1};
    Int_t m_moduleID{-1};
    Int_t m_sensorID{-1};

	ClassDef(MUonETrackerVolumeIDs, 2)

};

#endif //MUONETRACKERVOLUMEIDS_H



