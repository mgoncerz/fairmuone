#ifndef MUONEDETECTORPOINT_H
#define MUONEDETECTORPOINT_H

#include "TVector3.h"
#include "Rtypes.h"

#include "TObject.h"
#include "TLorentzVector.h"

class MUonEDetectorPoint : public TObject {

public:

    MUonEDetectorPoint() = default;

    MUonEDetectorPoint(Int_t trackID, Int_t trackPDGCode,
        TVector3& enteringPositionGlobalCoordinates, TVector3& exitingPositionGlobalCoordinates,
        TVector3& enteringPositionLocalCoordinates, TVector3& exitingPositionLocalCoordinates,
        Double_t totalEnergyDeposit)
        : TrackID(trackID), TrackPDGCode(trackPDGCode),
        EnteringPositionGlobalCoordinates(enteringPositionGlobalCoordinates), ExitingPositionGlobalCoordinates(exitingPositionGlobalCoordinates),
        EnteringPositionLocalCoordinates(enteringPositionLocalCoordinates), ExitingPositionLocalCoordinates(exitingPositionLocalCoordinates),
        TotalEnergyDeposit(totalEnergyDeposit)
        {}

    Int_t trackID() const {return TrackID;}
    Int_t trackPDGCode() const {return TrackPDGCode;}

    void setTrackID(Int_t id) {TrackID = id;} //for MUonEStack

    TVector3 enteringPositionGlobalCoordinates() const {return EnteringPositionGlobalCoordinates;}
    TVector3 exitingPositionGlobalCoordinates() const {return ExitingPositionGlobalCoordinates;}

    TVector3 enteringPositionLocalCoordinates() const {return EnteringPositionLocalCoordinates;}
    TVector3 exitingPositionLocalCoordinates() const {return ExitingPositionLocalCoordinates;}

    Double_t distanceTraveledInside() const {return (ExitingPositionLocalCoordinates - EnteringPositionLocalCoordinates).Mag();}

    Double32_t totalEnergyDeposit() const {return TotalEnergyDeposit;}


private:

    Int_t TrackID{-2};
	Int_t TrackPDGCode{0};

    TVector3 EnteringPositionGlobalCoordinates;
    TVector3 ExitingPositionGlobalCoordinates;

    TVector3 EnteringPositionLocalCoordinates;
    TVector3 ExitingPositionLocalCoordinates;

    Double32_t TotalEnergyDeposit{0};
    
	ClassDef(MUonEDetectorPoint,2)
};

#endif//MUONEDETECTORPOINT_H

