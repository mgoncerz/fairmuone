#ifndef MUONEDETECTORPROJECTIONCONFIGURATION_H
#define MUONEDETECTORPROJECTIONCONFIGURATION_H

#include "Rtypes.h"

#include <string>
#include <yaml-cpp/yaml.h>


class MUonEDetectorProjectionConfiguration {

public:

    MUonEDetectorProjectionConfiguration();
    MUonEDetectorProjectionConfiguration(char name, Double_t angle);

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    char name() const {return m_name;}

    Double_t angle() const {return m_angle;}
    Double_t tilt() const {return m_tilt;}

    Bool_t negateDataHitPosition() const {return m_negateDataHitPosition;}


    void resetDefaultConfiguration();

private:


    Bool_t m_configuredCorrectly{false};

    //predefined in MUonEDetectorConfiguration.h
    char m_name{'x'};
    Double_t m_angle{0};

    //taken from detector config
    Double_t m_tilt{0};
    Bool_t m_negateDataHitPosition{false};

	ClassDef(MUonEDetectorProjectionConfiguration, 2)
};

#endif //MUONEDETECTORPROJECTIONCONFIGURATION_H 

