#ifndef MUONEDETECTORCALORIMETERGEOMETRYCONFIGURATION_H
#define MUONEDETECTORCALORIMETERGEOMETRYCONFIGURATION_H


#include "Rtypes.h"

#include <yaml-cpp/yaml.h>

#include <string>


class MUonEDetectorCalorimeterGeometryConfiguration {


public:

    MUonEDetectorCalorimeterGeometryConfiguration();
    MUonEDetectorCalorimeterGeometryConfiguration(YAML::Node const& config);

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;


    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    std::string crystalMaterial() const {return m_crystalMaterial;}

    Double_t crystalWidthUpstream() const {return m_crystalWidthUpstream;}
    Double_t crystalHeightUpstream() const {return m_crystalHeightUpstream;}

    Double_t crystalWidthDownstream() const {return m_crystalWidthDownstream;}
    Double_t crystalHeightDownstream() const {return m_crystalHeightDownstream;}

    //used to find center point of each crystal during geometry creation
    Double_t crystalWidthMidpoint() const {return 0.5 * (m_crystalWidthUpstream + m_crystalWidthDownstream);}
    Double_t crystalHeightMidpoint() const {return 0.5 * (m_crystalHeightUpstream + m_crystalHeightDownstream);}

    Double_t crystalThickness() const {return m_crystalThickness;};

    Int_t numberOfColumns() const {return m_numberOfColumns;}
    Int_t numberOfRows() const {return m_numberOfRows;}

    Double_t distanceBetweenCrystalCenters() const {return m_distanceBetweenCrystalCenters;}


    void resetDefaultConfiguration();

private:


    Bool_t m_configuredCorrectly{false};

    std::string m_crystalMaterial;

    Double_t m_crystalWidthUpstream{0};
    Double_t m_crystalHeightUpstream{0};

    Double_t m_crystalWidthDownstream{0};
    Double_t m_crystalHeightDownstream{0};

    Double_t m_crystalThickness{0};

    Int_t m_numberOfColumns{0};
    Int_t m_numberOfRows{0};

    Double_t m_distanceBetweenCrystalCenters{0};


	ClassDef(MUonEDetectorCalorimeterGeometryConfiguration, 2)
};

#endif //MUONEDETECTORCALORIMETERGEOMETRYCONFIGURATION_H

