#include "MUonEDetectorGeometryMisalignmentsConfiguration.h"

#include <fairlogger/Logger.h>

MUonEDetectorGeometryMisalignmentsConfiguration::MUonEDetectorGeometryMisalignmentsConfiguration() {

    resetDefaultConfiguration();
}

MUonEDetectorGeometryMisalignmentsConfiguration::MUonEDetectorGeometryMisalignmentsConfiguration(TString config_name) {

    readConfiguration(config_name);
}

Bool_t MUonEDetectorGeometryMisalignmentsConfiguration::readConfiguration(TString config_name) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    LOG(info) << "";
    LOG(info) << "Reading file with geometry misalignments: " << config_name.Data();
    
    YAML::Node config;
    
    if(!loadConfigFile(config_name, config))
    return false;

    m_xGeometryOffset.reserve(config.size());
    m_yGeometryOffset.reserve(config.size());
    m_zGeometryOffset.reserve(config.size());
    m_angleGeometryOffset.reserve(config.size());
    m_tiltGeometryOffset.reserve(config.size());
    m_gammaGeometryOffset.reserve(config.size());

    for(int station_index = 0; station_index < config.size(); station_index++) {
        auto const& station = config[station_index];
        
        std::vector<Double_t> tmp_xGeometryOffset;
        std::vector<Double_t> tmp_yGeometryOffset;
        std::vector<Double_t> tmp_zGeometryOffset;
        std::vector<Double_t> tmp_angleGeometryOffset;
        std::vector<Double_t> tmp_tiltGeometryOffset;
        std::vector<Double_t> tmp_gammaGeometryOffset;
        
        tmp_xGeometryOffset.reserve(station.size());
        tmp_yGeometryOffset.reserve(station.size());
        tmp_zGeometryOffset.reserve(station.size());
        tmp_angleGeometryOffset.reserve(station.size());
        tmp_tiltGeometryOffset.reserve(station.size());
        tmp_gammaGeometryOffset.reserve(station.size());

        for(int module_index = 0; module_index < station.size(); module_index++) {
            auto const& module = station[module_index];
            
            if(module["xGeometryOffset"]) {
                tmp_xGeometryOffset.push_back(module["xGeometryOffset"].as<Double_t>());
            }
            else {
                LOG(error) << "xGeometryOffset not set for station " << station_index << ", module " << module_index;
                m_configuredCorrectly = false;
            }
            
            if(module["yGeometryOffset"]) {
                tmp_yGeometryOffset.push_back(module["yGeometryOffset"].as<Double_t>());
            }
            else {
                LOG(error) << "yGeometryOffset not set for station " << station_index << ", module " << module_index;
                m_configuredCorrectly = false;
            }
            
            if(module["zGeometryOffset"]) {
                tmp_zGeometryOffset.push_back(module["zGeometryOffset"].as<Double_t>());
            }
            else {
                LOG(error) << "zGeometryOffset not set for station " << station_index << ", module " << module_index;
                m_configuredCorrectly = false;
            }
            
            if(module["angleGeometryOffset"]) {
                tmp_angleGeometryOffset.push_back(module["angleGeometryOffset"].as<Double_t>());
            }
            else {
                LOG(error) << "angleGeometryOffset not set for station " << station_index << ", module " << module_index;
                m_configuredCorrectly = false;
            }

            if(module["tiltGeometryOffset"]) {
                tmp_tiltGeometryOffset.push_back(module["tiltGeometryOffset"].as<Double_t>());
            }
            else {
                LOG(error) << "tiltGeometryOffset not set for station " << station_index << ", module " << module_index;
                m_configuredCorrectly = false;
            }

            if(module["gammaGeometryOffset"]) {
                tmp_gammaGeometryOffset.push_back(module["gammaGeometryOffset"].as<Double_t>());
            }
            else {
                LOG(error) << "gammaGeometryOffset not set for station " << station_index << ", module " << module_index;
                m_configuredCorrectly = false;
            }
        
        }
    
        m_xGeometryOffset.push_back(tmp_xGeometryOffset);
        m_yGeometryOffset.push_back(tmp_yGeometryOffset);
        m_zGeometryOffset.push_back(tmp_zGeometryOffset);
        m_angleGeometryOffset.push_back(tmp_angleGeometryOffset);
        m_tiltGeometryOffset.push_back(tmp_tiltGeometryOffset);
        m_gammaGeometryOffset.push_back(tmp_gammaGeometryOffset);
    
    }

    if(!m_configuredCorrectly)
        LOG(info) << "One of the modules in the geometry misalignments file contains errors. Please fix them and try again.";
    else m_isIdealGeometry = false;

    return m_configuredCorrectly;
}

void MUonEDetectorGeometryMisalignmentsConfiguration::logCurrentConfiguration() const {

    if(!m_isIdealGeometry) {
        for(int station_index = 0; station_index < m_xGeometryOffset.size(); station_index++) {
            LOG(info) << "station " << station_index;

            for(int module_index = 0; module_index < m_xGeometryOffset[station_index].size(); module_index++) {
                LOG(info) << "\tmodule " << module_index;

                LOG(info) << "\txGeometryOffset: " << m_xGeometryOffset[station_index][module_index] << " cm";
                LOG(info) << "\tyGeometryOffset: " << m_yGeometryOffset[station_index][module_index] << " cm";
                LOG(info) << "\tzGeometryOffset: " << m_zGeometryOffset[station_index][module_index] << " cm";
                LOG(info) << "\tangleGeometryOffset: " << m_angleGeometryOffset[station_index][module_index] << " deg";
                LOG(info) << "\ttiltGeometryOffset: "  << m_tiltGeometryOffset[station_index][module_index]  << " deg";
                LOG(info) << "\tgammaGeometryOffset: " << m_gammaGeometryOffset[station_index][module_index] << " deg";
            }
        }
    }
    else {
        LOG(info) << "The ideal geometry is used for the simulation.";
    }

}

void MUonEDetectorGeometryMisalignmentsConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;
    m_isIdealGeometry = true;
    m_xGeometryOffset.clear();
    m_yGeometryOffset.clear();
    m_zGeometryOffset.clear();
    m_angleGeometryOffset.clear();
    m_tiltGeometryOffset.clear();
    m_gammaGeometryOffset.clear();
}


Bool_t MUonEDetectorGeometryMisalignmentsConfiguration::loadConfigFile(TString config_name, YAML::Node& config) {

    if(!config_name.EndsWith(".yaml") && !config_name.EndsWith(".YAML"))
        config_name.Append(".yaml");

    try {

        config = YAML::LoadFile(config_name.Data());
        LOG(info) << "File with geometry misalignments found in the path provided.";

    } catch(...) {

        TString jobs_dir = getenv("GEOMPATH");
        jobs_dir.ReplaceAll("//", "/"); //taken from FR examples, not sure if necessary
        if(!jobs_dir.EndsWith("/"))
            jobs_dir.Append("/");  

        TString full_path = jobs_dir + config_name;

        LOG(info) << "File with geometry misalignments in the given path not found or failed to open. Trying in GEOMPATH directory (" << full_path << ").";

        try {

            config = YAML::LoadFile(full_path.Data());
            LOG(info) << "File found.";

        } catch(...) {

            LOG(fatal) << "Failed to load alignment data.";
            return false;
        }
    }   

    return true;
}


void MUonEDetectorGeometryMisalignmentsConfiguration::setIsIdealGeometry() {
    m_isIdealGeometry = true;
}


ClassImp(MUonEDetectorGeometryMisalignmentsConfiguration)

