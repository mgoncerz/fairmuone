#include "MUonEDetectorModuleGeometryConfiguration.h"
#include <fairlogger/Logger.h>


MUonEDetectorModuleGeometryConfiguration::MUonEDetectorModuleGeometryConfiguration() {

    resetDefaultConfiguration();
}

MUonEDetectorModuleGeometryConfiguration::MUonEDetectorModuleGeometryConfiguration(YAML::Node const& config) {

    readConfiguration(config);
}

Bool_t MUonEDetectorModuleGeometryConfiguration::readConfiguration(YAML::Node const& config) {


    resetDefaultConfiguration();

    m_configuredCorrectly = true;


    if(config["twoSensorsPerModule"])
        m_twoSensorsPerModule = config["twoSensorsPerModule"].as<Bool_t>();
    else {

        LOG(info) << "Variable 'twoSensorsPerModule' not set. Defaults to true.";
    }

    if(config["sensorMaterial"])
        m_sensorMaterial = config["sensorMaterial"].as<std::string>();
    else {

        LOG(error) << "Variable 'sensorMaterial' not set.";
        m_configuredCorrectly = false;
    }

    if(config["sensorSizeMeasurementDirection"])
        m_sensorSizeMeasurementDirection = config["sensorSizeMeasurementDirection"].as<Double_t>();
    else {

        LOG(error) << "Variable 'sensorSizeMeasurementDirection' not set.";
        m_configuredCorrectly = false;
    }

    if(config["sensorSizePerpendicularDirection"])
        m_sensorSizePerpendicularDirection = config["sensorSizePerpendicularDirection"].as<Double_t>();
    else {

        LOG(error) << "Variable 'sensorSizePerpendicularDirection' not set.";
        m_configuredCorrectly = false;
    }

    if(config["sensorThickness"])
        m_sensorThickness = config["sensorThickness"].as<Double_t>();
    else {

        LOG(error) << "Variable 'sensorThickness' not set.";
        m_configuredCorrectly = false;
    }

    if(config["distanceBetweenSensorCenters"])
        m_distanceBetweenSensorCenters = config["distanceBetweenSensorCenters"].as<Double_t>();
    else {

        LOG(error) << "Variable 'distanceBetweenSensorCenters' not set.";
        m_configuredCorrectly = false;
    }    

    if(config["numberOfStrips"])
        m_numberOfStrips = config["numberOfStrips"].as<Int_t>();
    else {

        LOG(error) << "Variable 'numberOfStrips' not set.";
        m_configuredCorrectly = false;
    }

    if(!m_configuredCorrectly)
        LOG(info) << "modulesConfiguration/geometry section contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEDetectorModuleGeometryConfiguration::updateConfiguration(YAML::Node const& config) {

    if(config["twoSensorsPerModule"])
        m_twoSensorsPerModule = config["twoSensorsPerModule"].as<Bool_t>();

    if(config["sensorMaterial"])
        m_sensorMaterial = config["sensorMaterial"].as<std::string>();

    if(config["sensorSizeMeasurementDirection"])
        m_sensorSizeMeasurementDirection = config["sensorSizeMeasurementDirection"].as<Double_t>();

    if(config["sensorSizePerpendicularDirection"])
        m_sensorSizePerpendicularDirection = config["sensorSizePerpendicularDirection"].as<Double_t>();

    if(config["sensorThickness"])
        m_sensorThickness = config["sensorThickness"].as<Double_t>();

    if(config["distanceBetweenSensorCenters"])
        m_distanceBetweenSensorCenters = config["distanceBetweenSensorCenters"].as<Double_t>();

    if(config["numberOfStrips"])
        m_numberOfStrips = config["numberOfStrips"].as<Int_t>();

}

void MUonEDetectorModuleGeometryConfiguration::logCurrentConfiguration() const {

    LOG(info) << "Modules geometry configuration:";
    LOG(info) << "twoSensorsPerModule:" << m_twoSensorsPerModule;
    LOG(info) << "sensorMaterial: " << m_sensorMaterial;
    LOG(info) << "sensorSizeMeasurementDirection: " << m_sensorSizeMeasurementDirection << " cm";
    LOG(info) << "sensorSizePerpendicularDirection: " << m_sensorSizePerpendicularDirection << " cm";
    LOG(info) << "sensorThickness: " << m_sensorThickness << " cm";
    LOG(info) << "distanceBetweenSensorCenters: " << m_distanceBetweenSensorCenters << " cm";
    LOG(info) << "numberOfStrips: " << m_numberOfStrips;

}

void MUonEDetectorModuleGeometryConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_twoSensorsPerModule = true;
    m_sensorMaterial = "";
    m_sensorSizeMeasurementDirection = 0;
    m_sensorSizePerpendicularDirection = 0;
    m_sensorThickness = 0;
    m_distanceBetweenSensorCenters = 0;
    m_numberOfStrips = 0;
}


ClassImp(MUonEDetectorModuleGeometryConfiguration)
