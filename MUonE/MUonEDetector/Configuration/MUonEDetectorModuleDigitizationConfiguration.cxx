#include "MUonEDetectorModuleDigitizationConfiguration.h"

#include <fairlogger/Logger.h>
#include <iomanip>

MUonEDetectorModuleDigitizationConfiguration::MUonEDetectorModuleDigitizationConfiguration() {

    resetDefaultConfiguration();
}

MUonEDetectorModuleDigitizationConfiguration::MUonEDetectorModuleDigitizationConfiguration(YAML::Node const& config) {

    readConfiguration(config);
}

Bool_t MUonEDetectorModuleDigitizationConfiguration::readConfiguration(YAML::Node const& config) {


    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    if(config["driftDirectionUpstream"])
        m_driftDirectionUpstream = config["driftDirectionUpstream"].as<Int_t>();
    else {

        LOG(error) << "Variable 'driftDirectionUpstream' not set.";
        m_configuredCorrectly = false;
    }
    
    if(config["driftDirectionDownstream"])
        m_driftDirectionDownstream = config["driftDirectionDownstream"].as<Int_t>();
    else {

        LOG(error) << "Variable 'driftDirectionDownstream' not set.";
        m_configuredCorrectly = false;
    }


    if(config["sigma0"])
        m_sigma0 = config["sigma0"].as<Double_t>();
    else {

        LOG(error) << "Variable 'sigma0' not set.";
        m_configuredCorrectly = false;
    }

    if(config["sigmaCoefficient"])
        m_sigmaCoefficient = config["sigmaCoefficient"].as<Double_t>();
    else {

        LOG(error) << "Variable 'sigmaCoefficient' not set.";
        m_configuredCorrectly = false;
    }

    if(config["readoutNoise"])
        m_readoutNoise = config["readoutNoise"].as<Double_t>();
    else {

        LOG(error) << "Variable 'readoutNoise' not set.";
        m_configuredCorrectly = false;
    }

    if(config["stripThreshold"])
        m_stripThreshold = config["stripThreshold"].as<Double_t>();
    else {

        LOG(error) << "Variable 'stripThreshold' not set.";
        m_configuredCorrectly = false;
    }

    if(config["halfChannelStripOrder"])
        m_halfChannelStripOrder = config["halfChannelStripOrder"].as<Int_t>();
    else {

        LOG(error) << "Variable 'halfChannelStripOrder' not set.";
        m_configuredCorrectly = false;
    }

    if(config["maxClusterWidth"])
        m_maxClusterWidth = config["maxClusterWidth"].as<Int_t>();
    else {

        LOG(error) << "Variable 'maxClusterWidth' not set.";
        m_configuredCorrectly = false;
    }

    if(config["ptWidth"])
        m_ptWidth = config["ptWidth"].as<Double_t>();
    else {

        LOG(error) << "Variable 'ptWidth' not set.";
        m_configuredCorrectly = false;
    }

    if(config["windowOffset"])
        m_windowOffset = config["windowOffset"].as<Double_t>();
    else {

        LOG(error) << "Variable 'windowOffset' not set.";
        m_configuredCorrectly = false;
    }

    if(config["seedSensor"])
        m_seedSensor = config["seedSensor"].as<Int_t>();
    else {

        LOG(error) << "Variable 'seedSensor' not set.";
        m_configuredCorrectly = false;
    }

    if(config["enableLUT"])
        m_enableLUT = config["enableLUT"].as<Bool_t>();
    else {

        LOG(error) << "Variable 'enableLUT' not set.";
        m_configuredCorrectly = false;
    }

    if(config["LUTmap"]) {

        m_LUTmap.reserve(29);

        for(auto const& LUTelement : config["LUTmap"]) {

            std::pair<uint8_t, Double_t> LUTpair;

            if(LUTelement["binaryBendCode"]) {
                LUTpair.first = static_cast<uint8_t>(strtoul(LUTelement["binaryBendCode"].as<std::string>().c_str(), NULL, 16));
            }
            else {
                LOG(error) << "Variable 'binaryBendCode' not set";
                m_configuredCorrectly = false;
            }

            if(LUTelement["bendInStripUnits"]) {
                LUTpair.second = LUTelement["bendInStripUnits"].as<Double_t>();
            }
            else {
                LOG(error) << "Variable 'bendInStripUnits' not set";
                m_configuredCorrectly = false;
            }

            m_LUTmap.insert(LUTpair);

        }
    }
    else {

        LOG(error) << "Variable 'LUTmap' not set.";
        m_configuredCorrectly = false;
    }

    if(!m_configuredCorrectly)
        LOG(info) << "modulesConfiguration/digitization section contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}


void MUonEDetectorModuleDigitizationConfiguration::updateConfiguration(YAML::Node const& config) {

    if(config["driftDirectionUpstream"])
        m_driftDirectionUpstream = config["driftDirectionUpstream"].as<Int_t>();
    
    if(config["driftDirectionDownstream"])
        m_driftDirectionDownstream = config["driftDirectionDownstream"].as<Int_t>();

    if(config["sigma0"])
        m_sigma0 = config["sigma0"].as<Double_t>();

    if(config["sigmaCoefficient"])
        m_sigmaCoefficient = config["sigmaCoefficient"].as<Double_t>();

    if(config["readoutNoise"])
        m_readoutNoise = config["readoutNoise"].as<Double_t>();

    if(config["stripThreshold"])
        m_stripThreshold = config["stripThreshold"].as<Double_t>();

    if(config["halfChannelStripOrder"])
        m_halfChannelStripOrder = config["halfChannelStripOrder"].as<Int_t>();

    if(config["maxClusterWidth"])
        m_maxClusterWidth = config["maxClusterWidth"].as<Int_t>();

    if(config["ptWidth"])
        m_ptWidth = config["ptWidth"].as<Double_t>();

    if(config["windowOffset"])
        m_windowOffset = config["windowOffset"].as<Double_t>();

    if(config["seedSensor"])
        m_seedSensor = config["seedSensor"].as<Int_t>();

    if(config["enableLUT"])
        m_enableLUT = config["enableLUT"].as<Bool_t>();

    if(config["LUTmap"]) {
        m_LUTmap.clear();
    	m_LUTmap.reserve(29);
        for(auto const& LUTelement : config["LUTmap"]) {
            std::pair<uint8_t, Double_t> LUTpair;
            if(LUTelement["binaryBendCode"]) {
                LUTpair.first = static_cast<uint8_t>(strtoul(LUTelement["binaryBendCode"].as<std::string>().c_str(), NULL, 16));
            }
            else {
                LOG(error) << "Variable 'binaryBendCode' not set";
                m_configuredCorrectly = false;
            }
            if(LUTelement["bendInStripUnits"]) {
                LUTpair.second = LUTelement["bendInStripUnits"].as<Double_t>();
            }
            else {
                LOG(error) << "Variable 'bendInStripUnits' not set";
                m_configuredCorrectly = false;
            }
            m_LUTmap.insert(LUTpair);
        }
    }

}

void MUonEDetectorModuleDigitizationConfiguration::logCurrentConfiguration() const {

    LOG(info) << "Modules digitization configuration:";

    LOG(info) << "driftDirectionUpstream: " << m_driftDirectionUpstream;
    LOG(info) << "driftDirectionDownstream: " << m_driftDirectionDownstream;

    LOG(info) << "sigma0: " << m_sigma0;
    LOG(info) << "sigmaCoefficient: " << m_sigmaCoefficient;
    LOG(info) << "readoutNoise: " << m_readoutNoise;
    LOG(info) << "stripThreshold: " << m_stripThreshold;
    LOG(info) << "halfChannelStripOrder: " << m_halfChannelStripOrder;
    LOG(info) << "maxClusterWidth: " << m_maxClusterWidth;
    LOG(info) << "ptWidth: " << m_ptWidth;
    LOG(info) << "windowOffset: " << m_windowOffset;
    LOG(info) << "seedSensor: " << m_seedSensor;
    LOG(info) << "enableLUT: " << m_enableLUT;
    if(m_enableLUT) {
        LOG(info) << "LUTmap: ";
        for(auto const& LUTelement : m_LUTmap) {
            LOG(info) << "\tbinaryBendCode: 0x" << std::hex << std::setw(2) << std::setfill('0') << +(LUTelement.first) << ", "
                      << "bendInStripUnits: " << std::dec << LUTelement.second;
        }
    }
}

void MUonEDetectorModuleDigitizationConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_driftDirectionUpstream = 0;
    m_driftDirectionDownstream = 0;

    m_sigma0 = 0;
    m_sigmaCoefficient = 0;
    m_readoutNoise = 0;
    m_stripThreshold = 0;
    m_halfChannelStripOrder = 0;
    m_maxClusterWidth = 0;
    m_ptWidth = 0;
    m_windowOffset = 0;    
    m_seedSensor = 0;
    m_enableLUT = 0;
    m_LUTmap.clear();
}

ClassImp(MUonEDetectorModuleDigitizationConfiguration)
