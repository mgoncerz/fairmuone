#include "MUonETargets.h"

#include "MUonEStack.h"

#include "FairVolume.h"
#include "FairGeoVolume.h"
#include "FairGeoNode.h"
#include "FairRootManager.h"
#include "FairGeoLoader.h"
#include "FairGeoMedia.h"
#include "FairGeoBuilder.h"
#include "FairGeoInterface.h"
#include "FairRun.h"
#include "FairRuntimeDb.h"

#include "TClonesArray.h" 
#include "TGeoManager.h" 


#include "TGeoBBox.h"
#include "TGeoTrd2.h"
#include "TVirtualMC.h"
#include "TParticle.h"


MUonETargets::MUonETargets()
    : FairDetector("MUonETargets", true, DET_TARGETS) //name, active, id (see MUonEDetectorConfiguration.h)
    {}


MUonETargets::MUonETargets(MUonEDetectorConfiguration const& detectorConfiguration, Int_t targetToBias)
    : FairDetector("MUonETargets", true, DET_TARGETS) //name, active, id (see MUonEDetectorConfiguration.h)
    {
        setConfiguration(detectorConfiguration, targetToBias);
    }


MUonETargets::~MUonETargets() {}

void MUonETargets::setConfiguration(MUonEDetectorConfiguration const& detectorConfiguration, Int_t targetToBias) {
    
    m_detectorConfiguration = detectorConfiguration;
    m_targetToBias = targetToBias;
}


void MUonETargets::setTargetToBias(Int_t targetToBias) {

    m_targetToBias = targetToBias;
}


void MUonETargets::Register() {}


TClonesArray* MUonETargets::GetCollection(Int_t iColl) const {

    return NULL;
}


void MUonETargets::ConstructGeometry() {

    if(!m_detectorConfiguration.hasTargets())
        return;

    /*auto target_geometry = m_detectorConfiguration.targetsConfiguration();

    auto target_material_name = target_geometry.targetMaterial().c_str();
    InitMedium(target_material_name);
    auto target_material = gGeoManager->GetMedium(target_material_name);

    auto target_width = target_geometry.width();
    auto target_height = target_geometry.height();
    auto target_thickness = target_geometry.thickness();
    auto target_volume = new TGeoVolume("target_volume", 
                                            new TGeoBBox(
                                                    0.5 * target_width,
                                                    0.5 * target_height, 
                                                    0.5 * target_thickness
                                                    ),
                                                target_material);

    auto target_volume_bias = new TGeoVolume("target_volume_bias", 
                                            new TGeoBBox(
                                                    0.5 * target_width,
                                                    0.5 * target_height, 
                                                    0.5 * target_thickness
                                                    ),
                                                target_material);

    target_volume->SetLineColor(2); 
    target_volume_bias->SetLineColor(2); 

    AddSensitiveVolume(target_volume);     
    AddSensitiveVolume(target_volume_bias);     

*/


    //get pointer to detector cave
    TGeoVolume* top = gGeoManager->GetTopVolume();

    auto const& stations = m_detectorConfiguration.stations();

    std::vector<TGeoVolume*> volumes;
    volumes.reserve(5);

    std::vector<Int_t> station_target_volume_map;
    station_target_volume_map.resize(stations.size());


    for(Int_t station_index = 0; station_index < stations.size(); ++station_index) {

        auto const& station = stations[station_index];
        
        if(!station.hasTarget())
            continue;

        auto target_config = station.targetConfiguration();
        auto target_material_name = target_config.targetMaterial().c_str();
        auto target_width = target_config.width();
        auto target_height = target_config.height();
        auto target_thickness = target_config.thickness();  

        //InitMedium has check to not initialize medium multiple times
        InitMedium(target_material_name);
        auto target_material = gGeoManager->GetMedium(target_material_name);


        Int_t biased_volume_id = -1;
        if(m_targetToBias == station_index) { //can happen only once per detector

            volumes.emplace_back(new TGeoVolume(("target_volume_bias" + std::to_string(volumes.size())).c_str(), 
                                            new TGeoBBox(
                                                    0.5 * target_width,
                                                    0.5 * target_height, 
                                                    0.5 * target_thickness
                                                    ),
                                            target_material));

            AddSensitiveVolume(volumes.back());
            volumes.back()->SetLineColor(4); //blue

            station_target_volume_map[station_index] = volumes.size() - 1;  
            biased_volume_id = volumes.size() - 1;     

        } else {

            //check if volume would be the same as one of the existing ones
            //the modules will be mostly uniform, so we can brute force it
            bool sameAsExisting = false;
            for(Int_t test_volume_id = 0; test_volume_id < volumes.size(); ++test_volume_id) {

                if(test_volume_id == biased_volume_id) continue;

                auto const& test_volume = volumes[test_volume_id];

                auto const& shape = static_cast<TGeoBBox*>(test_volume->GetShape());
                auto const& test_dx = shape->GetDX();
                auto const& test_dy = shape->GetDY();
                auto const& test_dz = shape->GetDZ();
                auto const& test_medium = test_volume->GetMedium();

                if(fabs(0.5 * target_width - test_dx) < 1e-5
                && fabs(0.5 * target_height - test_dy) < 1e-5
                && fabs(0.5 * target_thickness - test_dz) < 1e-5
                && strcmp(test_medium->GetName(), target_material->GetName()) == 0) {

                    sameAsExisting = true;
                    station_target_volume_map[station_index] = test_volume_id;
                    break;
                }
            }

            if(!sameAsExisting) {

                volumes.emplace_back(new TGeoVolume(("target_volume" + std::to_string(volumes.size())).c_str(), 
                                                new TGeoBBox(
                                                        0.5 * target_width,
                                                        0.5 * target_height, 
                                                        0.5 * target_thickness
                                                        ),
                                                    target_material));

                AddSensitiveVolume(volumes.back());
                volumes.back()->SetLineColor(4); //blue

                station_target_volume_map[station_index] = volumes.size() - 1;
            }
        }

        auto target_position = station.targetPosition();
        auto transformation = new TGeoTranslation(0, 0, target_position);

        //copy id = station_index
        //all geometry other than targets should have negative IDs
        top->AddNode(volumes[station_target_volume_map[station_index]], station_index, transformation);                  
    }
}

Bool_t MUonETargets::ProcessHits(FairVolume *v) {

    /** This method is called from the MC stepping */

    //all geometry other than targets should have negative IDs
    Int_t volume_copy_number = v->getCopyNo();
    if(volume_copy_number < 0)
        return kTRUE;

    //get number of station from volume ID
    Int_t station = volume_copy_number;

	// for signal, stop incoming track as it enters target
	if (m_targetToBias < 0 && TVirtualMC::GetMC()->IsTrackEntering()) {

        //access track container (see MUonEStack class)
        auto trackStack = dynamic_cast<MUonEStack*>(TVirtualMC::GetMC()->GetStack());

        //this track should be stopped at one of the targets
        if(trackStack->getCurrentTrackStopAtTargetDecision()) {
                
            if(station == trackStack->getCurrentTrackStationIndex()) 
                TVirtualMC::GetMC()->StopTrack();
        }        
	}

    //for biasing stop all involved tracks as they leave the volume
    if (station == m_targetToBias && TVirtualMC::GetMC()->IsTrackExiting()) {

        auto trackStack = dynamic_cast<MUonEStack*>(TVirtualMC::GetMC()->GetStack());
        auto track = trackStack->GetCurrentTrack();

        if(track->IsPrimary()/* || 44 == track->GetUniqueID()*/) {
                
            TVirtualMC::GetMC()->StopTrack();
        }        
    }    
    
    return kTRUE;
}

void MUonETargets::Reset() {}
void MUonETargets::EndOfEvent() {}

//taken from examples, no need to read into it too much
Int_t MUonETargets::InitMedium(const char* name)
{
	static FairGeoLoader* geoLoad = FairGeoLoader::Instance();
	static FairGeoInterface* geoFace = geoLoad->getGeoInterface();
	static FairGeoMedia* media = geoFace->getMedia();
	static FairGeoBuilder* geoBuild = geoLoad->getGeoBuilder();
    
	FairGeoMedium* ShipMedium = media->getMedium(name);
    
	if (!ShipMedium)
	{
		Fatal("InitMedium","Material %s not defined in media file.", name);
		return -1111;
	}

	TGeoMedium* medium=gGeoManager->GetMedium(name);

	if (medium!=NULL)
		return ShipMedium->getMediumIndex();

    return geoBuild->createMedium(ShipMedium);
}


ClassImp(MUonETargets)