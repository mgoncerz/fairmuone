#include "MUonEDigitizedEventsFilter.h"


#include "FairRootManager.h"
#include <fairlogger/Logger.h>

MUonEDigitizedEventsFilter::MUonEDigitizedEventsFilter() 
{
    resetDefaultConfiguration();
}

Bool_t MUonEDigitizedEventsFilter::setConfiguration(MUonEDigitizedEventsFilterConfiguration const& config) {

	resetDefaultConfiguration();
	m_configuredCorrectly = true;

	m_configuration = config;
    m_configuredCorrectly = m_configuration.configuredCorrectly();
    m_isActive = config.isActive();

    if(!m_configuredCorrectly)
        LOG(info) << "eventFilterConfiguration/digitizedEvents section contains errors. Please fix them and try again.";
	
	return m_configuredCorrectly;
}

void MUonEDigitizedEventsFilter::logCurrentConfiguration() {

	m_configuration.logCurrentConfiguration();
}

void MUonEDigitizedEventsFilter::resetDefaultConfiguration() {

	m_configuredCorrectly = false;
    m_isActive = false;

	m_configuration.resetDefaultConfiguration();
}


Bool_t MUonEDigitizedEventsFilter::init(FairRootManager* ioman)
{

    if(!m_isActive)
        return false;

	m_stubs = static_cast<TClonesArray*>(ioman->GetObject("TrackerStubs"));

    if(nullptr != m_stubs) {
        m_stubsFound = true;
    }
    else {
        m_stubsFound = false;
        LOG(info) << "Stubs not found in the ntuple. Digitized events filters based on them will be ommited.";
    }

	m_calorimeterDeposit = ioman->InitObjectAs<const MUonECalorimeterDigiDeposit*>("CalorimeterDigiDeposit");

    if(nullptr != m_stubs) {
        m_calorimeterDepositFound = true;
    }
    else {
        m_calorimeterDepositFound = false;
        LOG(info) << "CalorimeterDigiDeposit not found in the ntuple. Digitized events filters based on it will be ommited.";
    }

    if(!m_stubsFound && !m_calorimeterDepositFound) {

        LOG(warning) << "Digitized events filter defined in job configuration, but no required structures present in the input. Filter will be deactivated.";
        m_isActive = false;
        return false;
    }


    return true;    
}

Bool_t MUonEDigitizedEventsFilter::saveEvent() 
{

    ++m_allEventsCounter;

    if(!m_isActive)
        return true;


    ++m_passedEventsCounter;
    return true;
}

void MUonEDigitizedEventsFilter::finish() 
{
    if(m_isActive)
        LOG(info) << "Digitized events filter: " << m_passedEventsCounter << " events out of " << m_allEventsCounter << " passed. Filter efficiency: " << efficiency();
}

ClassImp(MUonEDigitizedEventsFilter)