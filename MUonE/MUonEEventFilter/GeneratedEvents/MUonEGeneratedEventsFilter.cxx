#include "MUonEGeneratedEventsFilter.h"


#include "FairRootManager.h"
#include <fairlogger/Logger.h>

MUonEGeneratedEventsFilter::MUonEGeneratedEventsFilter() 
{
    resetDefaultConfiguration();
}

Bool_t MUonEGeneratedEventsFilter::setConfiguration(MUonEGeneratedEventsFilterConfiguration const& config) {

	resetDefaultConfiguration();
	m_configuredCorrectly = true;

	m_configuration = config;
    m_configuredCorrectly = m_configuration.configuredCorrectly();
    m_isActive = config.isActive();

    if(!m_configuredCorrectly)
        LOG(info) << "eventFilterConfiguration/generatedEvents section contains errors. Please fix them and try again.";
	
	return m_configuredCorrectly;
}

void MUonEGeneratedEventsFilter::logCurrentConfiguration() {

	m_configuration.logCurrentConfiguration();
}

void MUonEGeneratedEventsFilter::resetDefaultConfiguration() {

	m_configuredCorrectly = false;
    m_isActive = false;

	m_configuration.resetDefaultConfiguration();
}

Bool_t MUonEGeneratedEventsFilter::init(FairRootManager* ioman)
{
    
    if(!m_isActive)
        return false;

    m_mcTracks = static_cast<TClonesArray*>(ioman->GetObject("MCTrack"));

    if(nullptr != m_mcTracks) {
        m_mcTracksFound = true;
    }
    else {
        m_mcTracksFound = false;
        LOG(info) << "MCTracks not found in the ntuple. Generated events filters based on them will be ommited.";
    }

	m_trackerPoints = static_cast<TClonesArray*>(ioman->GetObject("TrackerPoints"));

    if(nullptr != m_trackerPoints) {
        m_trackerPointsFound = true;
    }
    else {
        m_trackerPointsFound = false;
        LOG(info) << "TrackerPoints not found in the ntuple. Generated events filters based on them will be ommited.";
    }

	m_calorimeterPoints = static_cast<TClonesArray*>(ioman->GetObject("CalorimeterPoints"));

    if(nullptr != m_trackerPoints) {
        m_calorimeterPointsFound = true;
    }
    else {
        m_calorimeterPointsFound = false;
        LOG(info) << "CalorimeterPoints not found in the ntuple. Generated events filters based on them will be ommited.";
    }


    if(!m_mcTracksFound && !m_trackerPointsFound && !m_calorimeterPointsFound) {

        LOG(warning) << "Generated events filter defined in job configuration, but no required structures present in the input. Filter will be deactivated.";
        m_isActive = false;
        return false;
    }

    return true;    
}

Bool_t MUonEGeneratedEventsFilter::saveEvent() 
{

    ++m_allEventsCounter;

    if(!m_isActive)
        return true;

    if(m_mcTracksFound) {

        if(m_configuration.minNumberOfMonteCarloTracksActive() && m_mcTracks->GetEntries() < m_configuration.minNumberOfMonteCarloTracks())
            return false;
    }


    ++m_passedEventsCounter;
    return true;
}

void MUonEGeneratedEventsFilter::finish() 
{
    if(m_isActive)
        LOG(info) << "Generated events filter: " << m_passedEventsCounter << " events out of " << m_allEventsCounter << " passed. Filter efficiency: " << efficiency();
}

ClassImp(MUonEGeneratedEventsFilter)