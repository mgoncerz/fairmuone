#include "MUonEDigitizedEventsFilterConfiguration.h"

#include <fairlogger/Logger.h>


MUonEDigitizedEventsFilterConfiguration::MUonEDigitizedEventsFilterConfiguration() {

    resetDefaultConfiguration();
}

Bool_t MUonEDigitizedEventsFilterConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;



    if(!m_configuredCorrectly)
        LOG(info) << "Reconstructed events filter configuration contains errors. Please fix them and try again.";


    return m_configuredCorrectly;
}

void MUonEDigitizedEventsFilterConfiguration::logCurrentConfiguration() const {
    
    if(m_isActive) {
        LOG(info) << "Digitized events filter is active.";

    }
}

void MUonEDigitizedEventsFilterConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_isActive = false;
 
}

ClassImp(MUonEDigitizedEventsFilterConfiguration)