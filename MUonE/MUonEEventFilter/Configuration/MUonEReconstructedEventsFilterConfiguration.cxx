#include "MUonEReconstructedEventsFilterConfiguration.h"

#include <fairlogger/Logger.h>


MUonEReconstructedEventsFilterConfiguration::MUonEReconstructedEventsFilterConfiguration() {

    resetDefaultConfiguration();
}

Bool_t MUonEReconstructedEventsFilterConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;

    if(config["saveNotReconstructedEvents"]) {

        m_isActive = true;
        m_saveNotReconstructedEventsActive = true;
        m_saveNotReconstructedEvents = config["saveNotReconstructedEvents"].as<Bool_t>();
    }


    if(config["maxBestVertexChi2PerNdf"]) {

        m_isActive = true;
        m_maxBestVertexChi2PerNdfActive = true;
        m_maxBestVertexChi2PerNdf = config["maxBestVertexChi2PerNdf"].as<Double_t>();
    }

    if(!m_configuredCorrectly)
        LOG(info) << "Reconstructed events filter configuration contains errors. Please fix them and try again.";


    return m_configuredCorrectly;
}

void MUonEReconstructedEventsFilterConfiguration::logCurrentConfiguration() const {
    
    if(m_isActive) {
        LOG(info) << "Reconstructed events filter is active.";

        if(m_saveNotReconstructedEventsActive) {

            if(m_saveNotReconstructedEvents)
                LOG(info) << "Only reconstructed events will be saved.";
            else 
                LOG(info) << "Events that failed reconstruction will also be saved.";
        }

        if(m_maxBestVertexChi2PerNdfActive) {

            LOG(info) << "Only events witch Chi2/ndf of best vertex < " << m_maxBestVertexChi2PerNdf << " will be saved.";
        }

    }
}

void MUonEReconstructedEventsFilterConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_isActive = false;

    m_saveNotReconstructedEventsActive = false;
    m_maxBestVertexChi2PerNdfActive = false;
 
}

ClassImp(MUonEReconstructedEventsFilterConfiguration)