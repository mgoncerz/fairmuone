#ifndef MUONEEVENTFILTERCONFIGURATION_H
#define MUONEEVENTFILTERCONFIGURATION_H

#include <yaml-cpp/yaml.h>
#include "Rtypes.h"
#include <string>

#include "MUonEGeneratedEventsFilterConfiguration.h"
#include "MUonEDigitizedEventsFilterConfiguration.h"
#include "MUonEReconstructedEventsFilterConfiguration.h"

class MUonEEventFilterConfiguration {

public:

    MUonEEventFilterConfiguration();

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Bool_t hasGeneratedEventsFilter() const {return m_hasGeneratedEventsFilter;}
    MUonEGeneratedEventsFilterConfiguration const& generated() const {
        if(m_hasGeneratedEventsFilter)
            return m_generated;
        else
            throw std::logic_error("Trying to access generatedEventsFilterConfiguration, but it's not defined in the job configuration file.");
    }

    Bool_t hasDigitizedEventsFilter() const {return m_hasDigitizedEventsFilter;}
    MUonEDigitizedEventsFilterConfiguration const& digitized() const {
        if(m_hasDigitizedEventsFilter)
            return m_digitized;
        else
            throw std::logic_error("Trying to access digitizedEventsFilterConfiguration, but it's not defined in the job configuration file.");
    }

    Bool_t hasReconstructedEventsFilter() const {return m_hasReconstructedEventsFilter;}
    MUonEReconstructedEventsFilterConfiguration const& reconstructed() const {
        if(m_hasReconstructedEventsFilter)
            return m_reconstructed;
        else
            throw std::logic_error("Trying to access reconstructedEventsFilterConfiguration, but it's not defined in the job configuration file.");
    }

    void resetDefaultConfiguration();

private:

    Bool_t m_configuredCorrectly{false};

    Bool_t m_hasGeneratedEventsFilter{false};
    MUonEGeneratedEventsFilterConfiguration m_generated;

    Bool_t m_hasDigitizedEventsFilter{false};
    MUonEDigitizedEventsFilterConfiguration m_digitized;

    Bool_t m_hasReconstructedEventsFilter{false};
    MUonEReconstructedEventsFilterConfiguration m_reconstructed;


    ClassDef(MUonEEventFilterConfiguration, 2)
};

#endif //MUONEEVENTFILTERCONFIGURATION_H

