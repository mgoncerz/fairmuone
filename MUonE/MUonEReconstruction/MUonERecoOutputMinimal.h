#ifndef MUONERECOOUTPUTMINIMAL_H
#define MUONERECOOUTPUTMINIMAL_H

#include "TClonesArray.h"
#include "TVector3.h"
#include "TMath.h"
#include "Rtypes.h"
#include "TMatrixD.h"
#include "Math/SMatrix.h"

#include <cmath>
#include <vector>
#include <algorithm>
#include <unordered_map>

#include "MUonETrack.h"
#include "MUonERecoHit.h"
#include "MUonERecoTrack3D.h"
#include "MUonERecoVertex.h"
#include "MUonERecoAdaptiveFitterVertex.h"

#include "MUonEReconstructionConfiguration.h"


#include "MUonERecoOutputBase.h"

/*output classes for "minimal" type*/


class MUonERecoOutputHitMinimal {

public:

    MUonERecoOutputHitMinimal() = default;

    MUonERecoOutputHitMinimal(MUonERecoHit const& hit)
        : Index(hit.index()),
        StationID(hit.module().stationID()),
        ModuleID(hit.module().moduleID()),        
        Projection(hit.module().projection()),
        Z(hit.z()), 
        Position(hit.position()) 
        {}

    char projection() const {return Projection;}
    Double_t z() const {return Z;}
	Double_t position() const {return Position;}

    Int_t index() const {return Index;}
    Int_t stationID() const {return StationID;}
    Int_t moduleID() const {return ModuleID;}



private:

    Int_t Index{-1};
    Int_t StationID{-1};
    Int_t ModuleID{-1};

    char Projection{'n'};
    Double_t Z{0}; 
	Double_t Position{0};    
    
    ClassDef(MUonERecoOutputHitMinimal,1)
};

class MUonERecoOutputTrackMinimal {

public:

    MUonERecoOutputTrackMinimal() = default;

    MUonERecoOutputTrackMinimal(MUonERecoTrack3D const& track)
    : Sector(track.sector()), Index(track.index()),
    Z0(track.xTrack().z0()),
    XSlope(track.xTrack().slope()),
    X0(track.xTrack().x0()), 
    YSlope(track.yTrack().slope()),
    Y0(track.yTrack().y0()),
    DegreesOfFreedom(track.degreesOfFreedom()), 
    Chi2perDegreeOfFreedom(track.chi2PerDegreeOfFreedom())
    {}

    Int_t sector() const {return Sector;}
    Int_t index() const {return Index;}

    Double_t z0() const {return Z0;}

    Double_t xSlope() const {return XSlope;}
    Double_t x0() const {return X0;}

    Double_t ySlope() const {return YSlope;}
    Double_t y0() const {return Y0;}

    Int_t degreesOfFreedom() const {return DegreesOfFreedom;}
    Double_t chi2perDegreeOfFreedom() const {return Chi2perDegreeOfFreedom;}

private:

    Int_t Sector{-1};
    Int_t Index{-1};

    Double_t Z0{0};

    Double_t XSlope{0};
    Double_t X0{0};

    Double_t YSlope{0};
    Double_t Y0{0};

    Int_t DegreesOfFreedom{0};
    Double_t Chi2perDegreeOfFreedom{0};

public:

    MUonERecoOutputTrackMinimal(MUonERecoOutputTrackMinimal const& track)
    : Sector(track.sector()), Index(track.index()),
    Z0(track.z0()),
    XSlope(track.xSlope()),
    X0(track.x0()), 
    YSlope(track.ySlope()),
    Y0(track.y0()), 
    DegreesOfFreedom(track.degreesOfFreedom()), Chi2perDegreeOfFreedom(track.chi2perDegreeOfFreedom())
    {}    

    MUonERecoOutputTrackMinimal& operator=(MUonERecoOutputTrackMinimal const& track)
    {
        Sector = track.sector(); 
        Index = track.index();
        Z0 = track.z0();
        XSlope = track.xSlope(); 
        X0 = track.x0(); 
        YSlope = track.ySlope(); 
        Y0 = track.y0(); 
        DegreesOfFreedom = track.degreesOfFreedom(); 
        Chi2perDegreeOfFreedom = track.chi2perDegreeOfFreedom();

        return *this;
    }    


    ClassDef(MUonERecoOutputTrackMinimal,1)
};



class MUonERecoOutputVertexMinimal {

public:

    MUonERecoOutputVertexMinimal() = default;

    MUonERecoOutputVertexMinimal(MUonERecoVertex const& vertex)
    : StationIndex(vertex.stationIndex()),
    AlternativePIDHypothesis(vertex.alternativePIDHypothesis()),
    MuonTheta(vertex.assumedMuonTrackTheta()),
    ElectronTheta(vertex.assumedElectronTrackTheta()),
    XPositionFit(vertex.xPositionFit()), 
    YPositionFit(vertex.yPositionFit()), 
    ZPositionFit(vertex.zPositionFit()),
    PositionFitSuccessful(vertex.positionFitSuccessful()),
    Chi2perDegreeOfFreedom(vertex.chi2PerDegreeOfFreedom())
    {

        auto i = vertex.incomingTrack().directionVector().Unit();
        auto o1 = vertex.assumedMuonTrack().directionVector().Unit();
        auto o2 = vertex.assumedElectronTrack().directionVector().Unit();

        TripleProduct = i.Dot(o1.Cross(o2));
        Acoplanarity = M_PI_2 - i.Angle(o1.Cross(o2));
        ModifiedAcoplanarity = M_PI - (i.Cross(o1)).Angle(i.Cross(o2)); 
        if(TripleProduct < 0) ModifiedAcoplanarity *= -1;


    }

    Int_t stationIndex() const {return StationIndex;}

    Bool_t alternativePIDHypothesis() const {return AlternativePIDHypothesis;}

    Double_t muonTheta() const {return MuonTheta;}
    Double_t electronTheta() const {return ElectronTheta;}

    Double_t tripleProduct() const {return TripleProduct;}
    Double_t acoplanarity() const {return Acoplanarity;}
    Double_t modifiedAcoplanarity() const {return ModifiedAcoplanarity;}

    Double_t xPositionFit() const {return XPositionFit;}
    Double_t yPositionFit() const {return YPositionFit;}
    Double_t zPositionFit() const {return ZPositionFit;}

    Bool_t positionFitSuccessful() const {return PositionFitSuccessful;}
    Double_t chi2perDegreeOfFreedom() const {return Chi2perDegreeOfFreedom;}

private:

    Int_t StationIndex{-1};

    Bool_t AlternativePIDHypothesis{false};

    Double_t MuonTheta{0};
    Double_t ElectronTheta{0};

    Double_t TripleProduct{0};
    Double_t Acoplanarity{0};
    Double_t ModifiedAcoplanarity{0};


    Double_t XPositionFit{0};
    Double_t YPositionFit{0};
    Double_t ZPositionFit{0};

    Bool_t PositionFitSuccessful{false};

    Double_t Chi2perDegreeOfFreedom{0};


public:

    MUonERecoOutputVertexMinimal(MUonERecoOutputVertexMinimal const& vertex)
    : StationIndex(vertex.stationIndex()), 
    AlternativePIDHypothesis(vertex.alternativePIDHypothesis()),
    MuonTheta(vertex.muonTheta()), ElectronTheta(vertex.electronTheta()), 
    TripleProduct(vertex.tripleProduct()), Acoplanarity(vertex.acoplanarity()), ModifiedAcoplanarity(vertex.modifiedAcoplanarity()),
    XPositionFit(vertex.xPositionFit()), 
    YPositionFit(vertex.yPositionFit()), 
    ZPositionFit(vertex.zPositionFit()),
    PositionFitSuccessful(vertex.positionFitSuccessful()),
    Chi2perDegreeOfFreedom(vertex.chi2perDegreeOfFreedom())
    {}

    MUonERecoOutputVertexMinimal& operator=(MUonERecoOutputVertexMinimal const& vertex)
    {

        ElectronTheta = vertex.electronTheta(); 
        MuonTheta = vertex.muonTheta(); 
        TripleProduct = vertex.tripleProduct(); 
        Acoplanarity = vertex.acoplanarity(); 
        ModifiedAcoplanarity = vertex.modifiedAcoplanarity();
        StationIndex = vertex.stationIndex();
        AlternativePIDHypothesis = vertex.alternativePIDHypothesis();
        XPositionFit = vertex.xPositionFit(); 
        YPositionFit = vertex.yPositionFit(); 
        ZPositionFit = vertex.zPositionFit(); 
        PositionFitSuccessful = vertex.positionFitSuccessful(); 
        Chi2perDegreeOfFreedom = vertex.chi2perDegreeOfFreedom();

        return *this;
    }


    ClassDef(MUonERecoOutputVertexMinimal,1)
};


class MUonERecoOutputGenericVertexMinimal {

public:

    MUonERecoOutputGenericVertexMinimal() = default;

    MUonERecoOutputGenericVertexMinimal(MUonERecoGenericVertex const& vertex)
    : Multiplicity(vertex.multiplicity()),
    StationIndex(vertex.stationIndex()),
    XPositionFit(vertex.xPositionFit()), 
    YPositionFit(vertex.yPositionFit()), 
    ZPositionFit(vertex.zPositionFit()),
    PositionFitSuccessful(vertex.positionFitSuccessful()),
    Chi2perDegreeOfFreedom(vertex.chi2PerDegreeOfFreedom())
    {}

    Int_t multiplicity() const {return Multiplicity;}
    Int_t stationIndex() const {return StationIndex;}

    Double_t xPositionFit() const {return XPositionFit;}
    Double_t yPositionFit() const {return YPositionFit;}
    Double_t zPositionFit() const {return ZPositionFit;}
    Bool_t positionFitSuccessful() const {return PositionFitSuccessful;}

    Double_t chi2perDegreeOfFreedom() const {return Chi2perDegreeOfFreedom;}



private:

    Int_t Multiplicity{0};

    Int_t StationIndex{-1};

    Double_t XPositionFit{0};
    Double_t YPositionFit{0};
    Double_t ZPositionFit{0};

    Bool_t PositionFitSuccessful{false};
    Double_t Chi2perDegreeOfFreedom{0};


public:

    MUonERecoOutputGenericVertexMinimal(MUonERecoOutputGenericVertexMinimal const& vertex)
    : Multiplicity(vertex.multiplicity()),
    StationIndex(vertex.stationIndex()),
    XPositionFit(vertex.xPositionFit()), 
    YPositionFit(vertex.yPositionFit()), 
    ZPositionFit(vertex.zPositionFit()),
    PositionFitSuccessful(vertex.positionFitSuccessful()),
    Chi2perDegreeOfFreedom(vertex.chi2perDegreeOfFreedom())
    {}

    MUonERecoOutputGenericVertexMinimal& operator=(MUonERecoOutputGenericVertexMinimal const& vertex)
    {

        Multiplicity = vertex.multiplicity();
        StationIndex = vertex.stationIndex();
        XPositionFit = vertex.xPositionFit(); 
        YPositionFit = vertex.yPositionFit(); 
        ZPositionFit = vertex.zPositionFit(); 
        PositionFitSuccessful = vertex.positionFitSuccessful(); 
        Chi2perDegreeOfFreedom = vertex.chi2perDegreeOfFreedom();

        return *this;
    }    


    ClassDef(MUonERecoOutputGenericVertexMinimal,1)
};


class MUonERecoOutputAdaptiveFitterVertexMinimal {

public:

    MUonERecoOutputAdaptiveFitterVertexMinimal() = default;

    MUonERecoOutputAdaptiveFitterVertexMinimal(MUonERecoAdaptiveFitterVertex const& vertex) 
        : StationIndex(vertex.stationIndex()),
        X(vertex.position().x()),
        Y(vertex.position().y()),
        Z(vertex.position().z()),
        Chi2(vertex.chi2())
    {}

    Int_t stationIndex() const {return StationIndex;}

    Double_t x() const {return X;}
    Double_t y() const {return Y;}
    Double_t z() const {return Z;}

    Double_t chi2() const {return Chi2;}

private:

    Int_t StationIndex{-1};

    Double_t X{0};
    Double_t Y{0};
    Double_t Z{0};

    Double_t Chi2{0};
    
    ClassDef(MUonERecoOutputAdaptiveFitterVertexMinimal,1)
};

class MUonERecoOutputMinimal : public MUonERecoOutputBase {

public:

    MUonERecoOutputMinimal() = default;

    //used for events that failed to reconstruct
    MUonERecoOutputMinimal(Int_t eventNumber, Double_t eventEnergy, std::vector<std::vector<std::vector<MUonERecoHit>>> const& hitsPerModulePerSector, std::vector<std::vector<MUonERecoTrack3D>> const& reconstructedTracksPerSector, std::vector<MUonERecoAdaptiveFitterVertex> afVertices, MUonEReconstructionConfiguration const& recoConfig, Bool_t event_skipped)
        : MUonERecoOutputBase(MUonEReconstructionConfiguration::OutputFormat::minimal),
        IsReconstructed(false), SourceEventNumber(eventNumber), IsMC(recoConfig.isMC()), TotalEventEnergy(eventEnergy), EventSkipped(event_skipped), AdaptiveFitterVerticesMultiplicity(afVertices.size())
        {

            ReconstructedHits.reserve(50);
            ReconstructedHitsMultiplicity = 0;

            for(auto const& sectors : hitsPerModulePerSector) {
                for(auto const& modules : sectors) {
                    for(auto const& hit : modules) {

                        ReconstructedHits.emplace_back(hit);
                        ++ReconstructedHitsMultiplicity;
                    }
                }
            }
                

            ReconstructedTracks.reserve(50);
            ReconstructedTracksMultiplicity = 0;

            for(auto const& sectors : reconstructedTracksPerSector) {
                for(auto const& track : sectors) {

                    ReconstructedTracks.emplace_back(track);
                    ++ReconstructedTracksMultiplicity;
                }
            }

            
            if(!afVertices.empty()) {

                BestAdaptiveFitterVertex = afVertices[0];
            }            

        }


    //used for correctly reconstructed event
    MUonERecoOutputMinimal(Int_t eventNumber, Double_t eventEnergy, std::vector<std::vector<std::vector<MUonERecoHit>>> const& hitsPerModulePerSector, std::vector<std::vector<MUonERecoTrack3D>> const& reconstructedTracksPerSector, std::vector<MUonERecoVertex> const& vertices, std::vector<MUonERecoAdaptiveFitterVertex> afVertices, std::vector<MUonERecoGenericVertex> genericVertices, MUonEReconstructionConfiguration const& recoConfig)
        : MUonERecoOutputBase(MUonEReconstructionConfiguration::OutputFormat::minimal),
        IsReconstructed(true), SourceEventNumber(eventNumber), IsMC(recoConfig.isMC()), TotalEventEnergy(eventEnergy), BestVertex(vertices[0]), ReconstructedVerticesMultiplicity(vertices.size()), AdaptiveFitterVerticesMultiplicity(afVertices.size()), ReconstructedGenericVerticesMultiplicity(genericVertices.size())
        {

            ReconstructedHits.reserve(50);
            ReconstructedHitsMultiplicity = 0;

            for(auto const& sectors : hitsPerModulePerSector) {
                for(auto const& modules : sectors) {
                    for(auto const& hit : modules) {

                        ReconstructedHits.emplace_back(hit);
                        ++ReconstructedHitsMultiplicity;
                    }
                }
            }


            ReconstructedTracks.reserve(50);
            ReconstructedTracksMultiplicity = 0;


            for(auto const& sectors : reconstructedTracksPerSector) {
                for(auto const& track : sectors) {

                    ReconstructedTracks.emplace_back(track);
                    ++ReconstructedTracksMultiplicity;
                }
            }
                            

            if(!afVertices.empty()) {

                BestAdaptiveFitterVertex = afVertices[0];
            }

            if(!genericVertices.empty()) {

                BestGenericVertex = genericVertices[0];
            }
            
        }




    Bool_t isReconstructed() const {return IsReconstructed;}

    Int_t sourceEventNumber() const {return SourceEventNumber;}

    Bool_t isMC() const {return IsMC;}
    Bool_t eventSkipped() const {return EventSkipped;}

    Double_t totalEventEnergy() const {return TotalEventEnergy;}

    MUonERecoOutputVertexMinimal const& bestVertex() const {return BestVertex;}

    Int_t reconstructedHitsMultiplicity() const {return ReconstructedHitsMultiplicity;}
    std::vector<MUonERecoOutputHitMinimal> const& reconstructedHits() const {return ReconstructedHits;}

    Int_t reconstructedTracksMultiplicity() const {return ReconstructedTracksMultiplicity;}
    std::vector<MUonERecoOutputTrackMinimal> const& reconstructedTracks() const {return ReconstructedTracks;}

    Int_t adaptiveFitterVerticesMultiplicity() const {return AdaptiveFitterVerticesMultiplicity;}
    MUonERecoOutputAdaptiveFitterVertexMinimal const& bestAdaptiveFitterVertex() const {return BestAdaptiveFitterVertex;}

    Int_t reconstructedGenericVerticesMultiplicity() const {return ReconstructedGenericVerticesMultiplicity;}
    MUonERecoOutputGenericVertexMinimal const& bestGenericVertex() const {return BestGenericVertex;}


private:

    Bool_t IsReconstructed{false};

    Int_t SourceEventNumber{-1};

    Bool_t IsMC{true};
    Bool_t EventSkipped{false};

    Double_t TotalEventEnergy{0};


    Int_t ReconstructedVerticesMultiplicity{0};
    MUonERecoOutputVertexMinimal BestVertex;

    Int_t ReconstructedHitsMultiplicity{0};
    std::vector<MUonERecoOutputHitMinimal> ReconstructedHits;

    Int_t ReconstructedTracksMultiplicity{0};
    std::vector<MUonERecoOutputTrackMinimal> ReconstructedTracks;

    Int_t AdaptiveFitterVerticesMultiplicity{0};
    MUonERecoOutputAdaptiveFitterVertexMinimal BestAdaptiveFitterVertex;

    Int_t ReconstructedGenericVerticesMultiplicity{0};
    MUonERecoOutputGenericVertexMinimal BestGenericVertex;


    ClassDef(MUonERecoOutputMinimal,1)
};

#endif //MUONERECOOUTPUTMINIMAL_H

