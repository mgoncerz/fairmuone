#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class MUonERecoFitter+;
#pragma link C++ class AFTrack+;
#pragma link C++ class AFSeed+;
#pragma link C++ class AFTrackInVertex+;
#pragma link C++ class MUonERecoHit+;
#pragma link C++ class MUonERecoModule+;
#pragma link C++ class MUonEReconstruction+;
#pragma link C++ class MUonERecoTarget+;
#pragma link C++ class MUonERecoTrack2D+;
#pragma link C++ class MUonERecoTrack3D+;
#pragma link C++ class MUonERecoVertex+;
#pragma link C++ class MUonERecoGenericVertex+;
#pragma link C++ class MUonERecoAdaptiveFitterVertex+;

#pragma link C++ class MUonERecoOutputBase+;

#pragma link C++ class MUonERecoOutputMinimal+;
#pragma link C++ class MUonERecoOutputHitMinimal+;
#pragma link C++ class MUonERecoOutputTrackMinimal+;
#pragma link C++ class MUonERecoOutputVertexMinimal+;
#pragma link C++ class MUonERecoOutputGenericVertexMinimal+;
#pragma link C++ class MUonERecoOutputAdaptiveFitterVertexMinimal+;

#pragma link C++ class MUonERecoOutputAnalysis+;
#pragma link C++ class MUonERecoOutputHitAnalysis+;
#pragma link C++ class MUonERecoOutputTrackAnalysis+;
#pragma link C++ class MUonERecoOutputVertexAnalysis+;
#pragma link C++ class MUonERecoOutputGenericVertexAnalysis+;
#pragma link C++ class MUonERecoOutputAdaptiveFitterVertexAnalysis+;

#pragma link C++ class MUonERecoOutputFull+;
#pragma link C++ class MUonERecoOutputHitFull+;
#pragma link C++ class MUonERecoOutputTrackFull+;
#pragma link C++ class MUonERecoOutputVertexFull+;
#pragma link C++ class MUonERecoOutputGenericVertexFull+;
#pragma link C++ class MUonERecoOutputAdaptiveFitterVertexFull+;

#endif