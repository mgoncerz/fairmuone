#ifndef MUONERECOVERTEX_H
#define MUONERECOVERTEX_H

#include "MUonERecoTarget.h"
#include "MUonERecoTrack3D.h"

#include "TVector3.h"
#include "TMatrixD.h"
#include "Math/SMatrix.h"

#include <cmath>

/*
	Used to store a vertex of a scattering event.
*/

class MUonERecoVertex {

public:

	MUonERecoVertex() = default;

	//constructs vertex from a single incoming and two outgoing tracks
	//initial x and y coordinates are calculated as arithmetic means of the incoming tracks position and the arithmetic mean of outgoing tracks positions
	//initial z is set to target's z
	MUonERecoVertex(MUonERecoTrack3D const& incomingTrack, MUonERecoTrack3D const& firstOutgoingTrack, MUonERecoTrack3D const& secondOutgoingTrack, MUonERecoTarget const& target, Bool_t alternativePIDHypothesis = false)
		: m_incomingTrack(incomingTrack), m_firstOutgoingTrack(firstOutgoingTrack), m_secondOutgoingTrack(secondOutgoingTrack),
		m_stationIndex(target.station()),
		m_chi2(incomingTrack.chi2() + firstOutgoingTrack.chi2() + secondOutgoingTrack.chi2()),
		m_alternativePIDHypothesis(alternativePIDHypothesis)
		{

			//calculate theta angles
			recalculateAngles();
	
			m_firstThetaLowerBeforeVertexFit = m_firstTheta < m_secondTheta;
		}	


	MUonERecoTrack3D const& incomingTrack() const {return m_incomingTrack;}
	MUonERecoTrack3D const& firstOutgoingTrack() const {return m_firstOutgoingTrack;}
	MUonERecoTrack3D const& secondOutgoingTrack() const {return m_secondOutgoingTrack;}

	MUonERecoTrack3D const& assumedMuonTrack() const {

		if(!m_alternativePIDHypothesis) {

			return m_firstThetaLowerBeforeVertexFit ? m_firstOutgoingTrack : m_secondOutgoingTrack;

		} else {

			return m_firstThetaLowerBeforeVertexFit ? m_secondOutgoingTrack : m_firstOutgoingTrack;
		}
	}

	MUonERecoTrack3D const& assumedElectronTrack() const {

		if(!m_alternativePIDHypothesis) {

			return m_firstThetaLowerBeforeVertexFit ? m_secondOutgoingTrack : m_firstOutgoingTrack;

		} else {

			return m_firstThetaLowerBeforeVertexFit ? m_firstOutgoingTrack : m_secondOutgoingTrack;
		}		
	}

	//used to add MS and refit
	//methods above should be prefered if modifications are not needed
	MUonERecoTrack3D& incomingTrackRef() {return m_incomingTrack;}

	MUonERecoTrack3D& assumedMuonTrackRef() {

		if(!m_alternativePIDHypothesis) {

			return m_firstThetaLowerBeforeVertexFit ? m_firstOutgoingTrack : m_secondOutgoingTrack;

		} else {

			return m_firstThetaLowerBeforeVertexFit ? m_secondOutgoingTrack : m_firstOutgoingTrack;
		}
	}

	MUonERecoTrack3D& assumedElectronTrackRef() {

		if(!m_alternativePIDHypothesis) {

			return m_firstThetaLowerBeforeVertexFit ? m_secondOutgoingTrack : m_firstOutgoingTrack;

		} else {

			return m_firstThetaLowerBeforeVertexFit ? m_firstOutgoingTrack : m_secondOutgoingTrack;
		}		
	}	

	Double_t assumedMuonTrackTheta() const {

		if(!m_alternativePIDHypothesis) {

			return m_firstThetaLowerBeforeVertexFit ? m_firstTheta : m_secondTheta;

		} else {

			return m_firstThetaLowerBeforeVertexFit ? m_secondTheta : m_firstTheta;
		}
	}
	Double_t assumedMuonTrackThetaError() const {

		if(!m_alternativePIDHypothesis) {

			return m_firstThetaLowerBeforeVertexFit ? m_firstThetaError : m_secondThetaError;

		} else {

			return m_firstThetaLowerBeforeVertexFit ? m_secondThetaError : m_firstThetaError;
		}
	}

	Double_t assumedElectronTrackTheta() const {

		if(!m_alternativePIDHypothesis) {

			return m_firstThetaLowerBeforeVertexFit ? m_secondTheta : m_firstTheta;

		} else {

			return m_firstThetaLowerBeforeVertexFit ? m_firstTheta : m_secondTheta;
		}
	}

	Double_t assumedElectronTrackThetaError() const {

		if(!m_alternativePIDHypothesis) {

			return m_firstThetaLowerBeforeVertexFit ? m_secondThetaError : m_firstThetaError;

		} else {

			return m_firstThetaLowerBeforeVertexFit ? m_firstThetaError : m_secondThetaError;
		}
	}


	Int_t stationIndex() const {return m_stationIndex;}

	Bool_t alternativePIDHypothesis() const {return m_alternativePIDHypothesis;}

	Double_t xKinematicFit() const {return m_xKinematicFit;}
	Double_t xKinematicFitError() const {return m_xKinematicFitError;}

	Double_t yKinematicFit() const {return m_yKinematicFit;}
	Double_t yKinematicFitError() const {return m_yKinematicFitError;}

	Double_t zKinematicFit() const {return m_zKinematicFit;}
	Double_t zKinematicFitError() const {return m_zKinematicFitError;}

	Double_t xPositionFit() const {return m_xPositionFit;}
	Double_t xPositionFitError() const {return m_xPositionFitError;}

	Double_t yPositionFit() const {return m_yPositionFit;}
	Double_t yPositionFitError() const {return m_yPositionFitError;}

	Double_t zPositionFit() const {return m_zPositionFit;}
	Double_t zPositionFitError() const {return m_zPositionFitError;}

	Bool_t positionFitSuccessful() const {return m_positionFitSuccessful;}

	ROOT::Math::SMatrix<Double_t, 3> const& positionFitCovarianceMatrix() const {return m_positionFitCovarianceMatrix;}
	TMatrixD const& kinematicFitCovarianceMatrix() const {return m_kinematicFitCovarianceMatrix;}


	Double_t chi2() const {return m_chi2;}

	//only tracks DoF are used for a fit quality variable
	Int_t degreesOfFreedom() const {return m_incomingTrack.degreesOfFreedom() + m_firstOutgoingTrack.degreesOfFreedom() + m_secondOutgoingTrack.degreesOfFreedom();}

    Double_t chi2PerDegreeOfFreedom() const {return m_chi2/degreesOfFreedom();}

	Bool_t hitsReassigned() const {return m_hitsReassigned;}

	//methods for fitter
	Bool_t reassignHits(std::vector<std::vector<MUonERecoHit>> const& hitsPerModule, Int_t maxNumberOfSharedHits, Double_t xyThreshold, Double_t uvThreshsold, Double_t usePerpendicularDistance2D, Double_t usePerpendicularDistance3D) {

		m_hitsReassigned = false;

		if(maxNumberOfSharedHits < 1) return false;

		for(int i = 0; i < maxNumberOfSharedHits && i < hitsPerModule.size(); ++i) {

			auto hits_in_module = hitsPerModule[i];
			if(hits_in_module.empty()) continue;

			auto mod = hits_in_module.front().module();

			auto hits_out1 = mod.isStereo() ? m_firstOutgoingTrack.stereoHits() : ('x' == mod.projection() ? m_firstOutgoingTrack.xTrack().hits() : m_firstOutgoingTrack.yTrack().hits());
			auto hits_out2 = mod.isStereo() ? m_secondOutgoingTrack.stereoHits() : ('x' == mod.projection() ? m_secondOutgoingTrack.xTrack().hits() : m_secondOutgoingTrack.yTrack().hits());

			//find hits in this module from both tracks
			auto first_track_hit = std::find_if(hits_out1.begin(), hits_out1.end(), [&mod](MUonERecoHit const& hit){return hit.module().sameModuleAs(mod);});
			auto second_track_hit = std::find_if(hits_out2.begin(), hits_out2.end(), [&mod](MUonERecoHit const& hit){return hit.module().sameModuleAs(mod);});

			if(first_track_hit == hits_out1.end() || second_track_hit == hits_out2.end()) continue;

			//they are shared between tracks
			if((*first_track_hit == *second_track_hit)) {

				//if electron track got pulled away by KF, the hit should be closer to the muon track
				auto hit_tack1_distance = usePerpendicularDistance3D ? m_firstOutgoingTrack.perpendicularDistanceToHit(*first_track_hit) : m_firstOutgoingTrack.distanceToHitInModulePlane(*first_track_hit);
				auto hit_tack2_distance = usePerpendicularDistance3D ? m_secondOutgoingTrack.perpendicularDistanceToHit(*first_track_hit) : m_secondOutgoingTrack.distanceToHitInModulePlane(*first_track_hit);
				
				if(hit_tack1_distance < hit_tack2_distance) {
					//hit is closer to track1
					//try to assign a better hit to track 2
					//if not found, allow sharing
					m_hitsReassigned = m_hitsReassigned | m_secondOutgoingTrack.reassignHit(hits_in_module, mod, second_track_hit - hits_out2.begin(), xyThreshold, uvThreshsold, usePerpendicularDistance2D, usePerpendicularDistance3D);
				} else {

					m_hitsReassigned = m_hitsReassigned | m_firstOutgoingTrack.reassignHit(hits_in_module, mod, first_track_hit - hits_out1.begin(), xyThreshold, uvThreshsold, usePerpendicularDistance2D, usePerpendicularDistance3D);
				}
			}
		}	

		return m_hitsReassigned;	
	}



	//call this after hit vectors have been modified or track slopes changed
	void recalculateAngles() {

		auto calculateTheta = [](MUonERecoTrack3D const& track1, MUonERecoTrack3D const& track2, Double_t& theta, Double_t& error) {

			TVector3 v1 = track1.directionVector().Unit();
			TVector3 v2 = track2.directionVector().Unit();

			theta = v1.Angle(v2);

			Double_t v1Mag = v1.Mag();
			Double_t v2Mag = v2.Mag();

			Double_t C1 = v1Mag * v2Mag;
			Double_t C1_inv = 1/C1;
			Double_t C2 = v1.Dot(v2);
			Double_t C3 = C2 * C1_inv * C1_inv * C1_inv;
			Double_t C4 = 1 - C2 * C2 * C1_inv * C1_inv;

			Double_t v1AxErrSq = (track2.xTrack().slope() * C1_inv - track1.xTrack().slope() * v2Mag * C3) * track1.xTrack().slopeError();
			v1AxErrSq *= v1AxErrSq;

			Double_t v1AyErrSq = (track2.yTrack().slope() * C1_inv - track1.yTrack().slope() * v2Mag * C3) * track1.yTrack().slopeError();
			v1AyErrSq *= v1AyErrSq;			

			Double_t v2AxErrSq = (track1.xTrack().slope() * C1_inv - track2.xTrack().slope() * v1Mag * C3) * track2.xTrack().slopeError();
			v2AxErrSq *= v2AxErrSq;

			Double_t v2AyErrSq = (track1.yTrack().slope() * C1_inv - track2.yTrack().slope() * v1Mag * C3) * track2.yTrack().slopeError();
			v2AyErrSq *= v2AyErrSq;

			error = 1/sqrt(C4) * sqrt(v1AxErrSq + v1AyErrSq + v2AxErrSq + v2AyErrSq);		
		};

		calculateTheta(m_incomingTrack, m_firstOutgoingTrack, m_firstTheta, m_firstThetaError);
		calculateTheta(m_incomingTrack, m_secondOutgoingTrack, m_secondTheta, m_secondThetaError);
	}

    void setPositionKinematicFit(Double_t x, Double_t xError, Double_t y, Double_t yError, Double_t z, Double_t zError = 0) {

	    m_xKinematicFit = x;
	    m_xKinematicFitError = xError;

	    m_yKinematicFit = y;
	    m_yKinematicFitError = yError;

		m_zKinematicFit = z;
		m_zKinematicFitError = zError;
    }

    void setPositionPositionFit(Double_t x, Double_t xError, Double_t y, Double_t yError, Double_t z, Double_t zError = 0) {

	    m_xPositionFit = x;
	    m_xPositionFitError = xError;

	    m_yPositionFit = y;
	    m_yPositionFitError = yError;

		m_zPositionFit = z;
		m_zPositionFitError = zError;
    }

	void setPositionFitCovarianceMatrix(ROOT::Math::SMatrix<Double_t, 3> const& m) {

		m_positionFitCovarianceMatrix = m;
	}

	void setPositionFitStatus(Bool_t s) {

		m_positionFitSuccessful = s;
	}


	void setLinearFitCovarianceMatrix(TMatrixD const& m) {

		m_kinematicFitCovarianceMatrix.ResizeTo(m.GetNrows(), m.GetNcols());
		m_kinematicFitCovarianceMatrix = m;
	}

	void setChi2(Double_t chi2) {

		m_chi2 = chi2;
	}

private:

	MUonERecoTrack3D m_incomingTrack;
	MUonERecoTrack3D m_firstOutgoingTrack;
	MUonERecoTrack3D m_secondOutgoingTrack;

	Bool_t m_firstThetaLowerBeforeVertexFit{true};


	Int_t m_stationIndex{-1};

	Bool_t m_alternativePIDHypothesis{false};

	Double_t m_firstTheta{0};
	Double_t m_firstThetaError{0};

	Double_t m_secondTheta{0};
	Double_t m_secondThetaError{0};

	//vertex position from KF
	Double_t m_xKinematicFit{0};
	Double_t m_xKinematicFitError{0};

	Double_t m_yKinematicFit{0};
	Double_t m_yKinematicFitError{0};

	Double_t m_zKinematicFit{0}; 
	Double_t m_zKinematicFitError{0};

	//vertex position from position fitter
	Double_t m_xPositionFit{0};
	Double_t m_xPositionFitError{0};

	Double_t m_yPositionFit{0};
	Double_t m_yPositionFitError{0};

	Double_t m_zPositionFit{0}; 
	Double_t m_zPositionFitError{0};

	Bool_t m_positionFitSuccessful{false};

	ROOT::Math::SMatrix<Double_t, 3> m_positionFitCovarianceMatrix;
	TMatrixD m_kinematicFitCovarianceMatrix;

	Double_t m_chi2{0};

	Bool_t m_hitsReassigned{false};


	//if sorting is required, prefer vertices with lower chi2 per degree of freedom
	//worse vertex is considered to be of lower value, that is worse < better is true	
    friend inline bool operator< (MUonERecoVertex const& lhs, MUonERecoVertex const& rhs) {
        
        return lhs.chi2PerDegreeOfFreedom() > rhs.chi2PerDegreeOfFreedom();
    }
    friend inline bool operator> (MUonERecoVertex const& lhs, MUonERecoVertex const& rhs) {return  operator< (rhs,lhs);}
    friend inline bool operator<=(MUonERecoVertex const& lhs, MUonERecoVertex const& rhs) {return !operator> (lhs,rhs);}
    friend inline bool operator>=(MUonERecoVertex const& lhs, MUonERecoVertex const& rhs) {return !operator< (lhs,rhs);}

	friend inline std::ostream& operator<<(std::ostream& os, const MUonERecoVertex& v) {

		os << "########################################################################################################" << std::endl
			<< ">>> MUonERecoVertex <<<  stationIndex = " << v.m_stationIndex << ", hitsReassigned? " << v.m_hitsReassigned << std::endl
			<< "firstThetaLowerBeforeVertexFit = " << v.m_firstThetaLowerBeforeVertexFit << ", alternativePIDHypothesis = " << v.m_alternativePIDHypothesis << std::endl
			<< "incomingTrack: " << v.m_incomingTrack << std::endl
			<< "firstOutgoingTrack: " << v.m_firstOutgoingTrack << std::endl
			<< "secondOutgoingTrack: " << v.m_secondOutgoingTrack << std::endl
			<< "firstTheta = " << v.m_firstTheta << " +- " << v.m_firstThetaError << std::endl
			<< "secondTheta = " << v.m_secondTheta << " +- " << v.m_secondThetaError << std::endl
			<< "DoCA fit successful? " << v.m_positionFitSuccessful << std::endl
			<< "vertex (x,y,z) from DoCA fit: (" << v.m_xPositionFit << " +- " << v.m_xPositionFitError << ", " << v.m_yPositionFit << " +- " << v.m_yPositionFitError << ", " << v.m_zPositionFit << " +- " << v.m_zPositionFitError << ")" << std::endl
			<< "vertex (x,y,z) from KF      : (" << v.m_xKinematicFit << " +- " << v.m_xKinematicFitError << ", " << v.m_yKinematicFit << " +- " << v.m_yKinematicFitError << ", " << v.m_zKinematicFit << " +- " << v.m_zKinematicFitError << ")" << std::endl
			<< "chi2 = " << v.m_chi2 << std::endl;

		return os;
	}

    ClassDef(MUonERecoVertex,3)
};


/*
Generic N-vertex class.
*/

class MUonERecoGenericVertex {

public:

	MUonERecoGenericVertex() = default;

	MUonERecoGenericVertex(MUonERecoTrack3D const& incoming, std::vector<MUonERecoTrack3D> const& outgoing, MUonERecoTarget const& target)
		: m_incomingTrack(incoming), m_outgoingTracks(outgoing),
		m_stationIndex(target.station())
		{

			Double_t chi2 = incoming.chi2();
			for(auto const& t : outgoing) {

				chi2 += t.chi2();
			}

			m_chi2 = chi2;		
		}

	MUonERecoTrack3D const& incomingTrack() const {return m_incomingTrack;}
	std::vector<MUonERecoTrack3D> const& outgoingTracks() const {return m_outgoingTracks;}

	MUonERecoTrack3D& incomingTrackRef() {return m_incomingTrack;}
	std::vector<MUonERecoTrack3D>& outgoingTracksRef() {return m_outgoingTracks;}

	
	Int_t multiplicity() const {return m_outgoingTracks.size();}
	Int_t stationIndex() const {return m_stationIndex;}
	
	Double_t xKinematicFit() const {return m_xKinematicFit;}
	Double_t xKinematicFitError() const {return m_xKinematicFitError;}

	Double_t yKinematicFit() const {return m_yKinematicFit;}
	Double_t yKinematicFitError() const {return m_yKinematicFitError;}

	Double_t zKinematicFit() const {return m_zKinematicFit;}
	Double_t zKinematicFitError() const {return m_zKinematicFitError;}

	Double_t xPositionFit() const {return m_xPositionFit;}
	Double_t xPositionFitError() const {return m_xPositionFitError;}

	Double_t yPositionFit() const {return m_yPositionFit;}
	Double_t yPositionFitError() const {return m_yPositionFitError;}

	Double_t zPositionFit() const {return m_zPositionFit;}
	Double_t zPositionFitError() const {return m_zPositionFitError;}

	Bool_t positionFitSuccessful() const {return m_positionFitSuccessful;}

	ROOT::Math::SMatrix<Double_t, 3> const& positionFitCovarianceMatrix() const {return m_positionFitCovarianceMatrix;}


	Double_t chi2() const {return m_chi2;}

	TMatrixD const& kinematicFitCovarianceMatrix() const {return m_kinematicFitCovarianceMatrix;}

	//only tracks DoF are used for a fit quality variable
	Int_t degreesOfFreedom() const {

		Int_t ndf = m_incomingTrack.degreesOfFreedom();

		for(auto& track : m_outgoingTracks) 
			ndf += track.degreesOfFreedom();

		return ndf;
	}


    Double_t chi2PerDegreeOfFreedom() const {return m_chi2/degreesOfFreedom();}
	Bool_t hitsReassigned() const {return m_hitsReassigned;}



    void setPositionKinematicFit(Double_t x, Double_t xError, Double_t y, Double_t yError, Double_t z, Double_t zError = 0) {

	    m_xKinematicFit = x;
	    m_xKinematicFitError = xError;

	    m_yKinematicFit = y;
	    m_yKinematicFitError = yError;

		m_zKinematicFit = z;
		m_zKinematicFitError = zError;
    }


    void setPositionPositionFit(Double_t x, Double_t xError, Double_t y, Double_t yError, Double_t z, Double_t zError = 0) {

	    m_xPositionFit = x;
	    m_xPositionFitError = xError;

	    m_yPositionFit = y;
	    m_yPositionFitError = yError;

		m_zPositionFit = z;
		m_zPositionFitError = zError;
    }

	void setPositionFitCovarianceMatrix(ROOT::Math::SMatrix<Double_t, 3> const& m) {

		m_positionFitCovarianceMatrix = m;
	}

	void setPositionFitStatus(Bool_t s) {

		m_positionFitSuccessful = s;
	}

	void setLinearFitCovarianceMatrix(TMatrixD const& m) {

		m_kinematicFitCovarianceMatrix.ResizeTo(m.GetNrows(), m.GetNcols());
		m_kinematicFitCovarianceMatrix = m;
	}

	void setChi2(Double_t chi2) {

		m_chi2 = chi2;
	}

private:

	MUonERecoTrack3D m_incomingTrack;
	std::vector<MUonERecoTrack3D> m_outgoingTracks;

	Int_t m_stationIndex{-1};

	//vertex position from KF
	Double_t m_xKinematicFit{0};
	Double_t m_xKinematicFitError{0};

	Double_t m_yKinematicFit{0};
	Double_t m_yKinematicFitError{0};

	Double_t m_zKinematicFit{0}; 
	Double_t m_zKinematicFitError{0};

	//vertex position from position fitter
	Double_t m_xPositionFit{0};
	Double_t m_xPositionFitError{0};

	Double_t m_yPositionFit{0};
	Double_t m_yPositionFitError{0};

	Double_t m_zPositionFit{0}; 
	Double_t m_zPositionFitError{0};

	Bool_t m_positionFitSuccessful{false};

	ROOT::Math::SMatrix<Double_t, 3> m_positionFitCovarianceMatrix;
	TMatrixD m_kinematicFitCovarianceMatrix;


	Double_t m_chi2{0};

	Bool_t m_hitsReassigned{false};


	//if sorting is required, prefer vertices with lower chi2 per degree of freedom
	//worse vertex is considered to be of lower value, that is worse < better is true	
    friend inline bool operator< (MUonERecoGenericVertex const& lhs, MUonERecoGenericVertex const& rhs) {
        
        return lhs.chi2PerDegreeOfFreedom() > rhs.chi2PerDegreeOfFreedom();
    }
    friend inline bool operator> (MUonERecoGenericVertex const& lhs, MUonERecoGenericVertex const& rhs) {return  operator< (rhs,lhs);}
    friend inline bool operator<=(MUonERecoGenericVertex const& lhs, MUonERecoGenericVertex const& rhs) {return !operator> (lhs,rhs);}
    friend inline bool operator>=(MUonERecoGenericVertex const& lhs, MUonERecoGenericVertex const& rhs) {return !operator< (lhs,rhs);}


    ClassDef(MUonERecoGenericVertex,2)
};

#endif//MUONERECOVERTEX_H

