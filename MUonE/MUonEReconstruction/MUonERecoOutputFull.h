#ifndef MUONERECOOUTPUTFULL_H
#define MUONERECOOUTPUTFULL_H

#include "TClonesArray.h"
#include "TVector3.h"
#include "TMath.h"
#include "Rtypes.h"
#include "TMatrixD.h"
#include "Math/SMatrix.h"

#include <cmath>
#include <vector>
#include <algorithm>
#include <unordered_map>

#include "MUonETrack.h"
#include "MUonERecoHit.h"
#include "MUonERecoTrack3D.h"
#include "MUonERecoVertex.h"
#include "MUonERecoAdaptiveFitterVertex.h"

#include "MUonEReconstructionConfiguration.h"

#include "MUonERecoOutputBase.h"

/*output classes for "full" type*/


class MUonERecoOutputHitFull {

public:

    MUonERecoOutputHitFull() = default;

    MUonERecoOutputHitFull(MUonERecoHit const& hit, Double_t res, Double_t resModPlane)
        : Projection(hit.module().projection()),
        Z(hit.z()), PositionPerpendicular(hit.positionPerpendicular()),
        PositionError(hit.positionError()), 
        PerpendicularResiduum(res),
        ResiduumInModulePlane(resModPlane),
        Position(hit.position()), StationID(hit.module().stationID()),
        ModuleID(hit.module().moduleID()),
        SeedingClusterLinkedTrackIds(hit.seedingClusterLinkedTrackIds()),
        SeedingClusterLinkedTrackDeposits(hit.seedingClusterLinkedTrackDeposits()),
        CorrelationClusterLinkedTrackIds(hit.correlationClusterLinkedTrackIds()),
        CorrelationClusterLinkedTrackDeposits(hit.correlationClusterLinkedTrackDeposits()),
        SeedClusterWidth(hit.seedClusterWidth()),
        CorrelationClusterWidth(hit.correlationClusterWidth()),
        Index(hit.index()),
        Bx(hit.bx()),
        SuperID(hit.superID())
        {}

    char projection() const {return Projection;}

    Double_t z() const {return Z;}

    Double_t positionPerpendicular() const {return PositionPerpendicular;}
    Double_t positionError() const {return PositionError;}
    Double_t perpendicularResiduum() const {return PerpendicularResiduum;}
    Double_t residuumInModulePlane() const {return ResiduumInModulePlane;}

	Double_t position() const {return Position;}

    Int_t stationID() const {return StationID;}
    Int_t moduleID() const {return ModuleID;}

	std::vector<Int_t> const& seedingClusterLinkedTrackIds() const {return SeedingClusterLinkedTrackIds;}
	std::vector<Double_t> const& seedingClusterLinkedTrackDeposits() const {return SeedingClusterLinkedTrackDeposits;}
    std::vector<Int_t> const& correlationClusterLinkedTrackIds() const {return CorrelationClusterLinkedTrackIds;}
    std::vector<Double_t> const& correlationClusterLinkedTrackDeposits() const {return CorrelationClusterLinkedTrackDeposits;}

    Double_t seedClusterWidth() const {return SeedClusterWidth;}
    Double_t correlationClusterWidth() const {return CorrelationClusterWidth;}

    Int_t index() const {return Index;}
    UShort_t bx() const {return Bx;}
    UInt_t superID() const {return SuperID;}

private:

    char Projection{'n'};

    Double_t Z{0}; //module().z() + position() * module().tiltSine();
	//position in plane perpendicular to beam axis (if the module is not tilted, it's equivalent to m_position)
	//note that it doesn't depend on the angle around beam axis, only on the amount of tilt
    //position() * module().tiltCosine();
    Double_t PositionPerpendicular{0};
	Double_t PositionError{0}; 	//equivalent to modules's hit resolution
    Double_t PerpendicularResiduum{0};
    Double_t ResiduumInModulePlane{0};

	Double_t Position{0};			//position in module's coordinates

    Int_t StationID{-1};
    Int_t ModuleID{-1};

	std::vector<Int_t> SeedingClusterLinkedTrackIds;
	std::vector<Double_t> SeedingClusterLinkedTrackDeposits;
    std::vector<Int_t> CorrelationClusterLinkedTrackIds;
    std::vector<Double_t> CorrelationClusterLinkedTrackDeposits;

    Double_t SeedClusterWidth{0};
    Double_t CorrelationClusterWidth{0};

    Int_t Index{-1}; //in the input from digi/data
    UShort_t Bx{0};
    UInt_t SuperID{0};

    ClassDef(MUonERecoOutputHitFull,1)
};

class MUonERecoOutputTrackFull {

public:

    MUonERecoOutputTrackFull() = default;

    MUonERecoOutputTrackFull(MUonERecoTrack3D const& track)
    : Sector(track.sector()), Index(track.index()),
    Z0(track.xTrack().z0()),
    XSlope(track.xTrack().slope()), XSlopeError(track.xTrack().slopeError()),
    X0(track.xTrack().x0()), X0Error(track.xTrack().x0Error()), 
    YSlope(track.yTrack().slope()), YSlopeError(track.yTrack().slopeError()),
    Y0(track.yTrack().y0()), Y0Error(track.yTrack().y0Error()), 
    NumberOfXProjectionHits(track.xTrack().numberOfHits()), NumberOfYProjectionHits(track.yTrack().numberOfHits()), NumberOfStereoHits(track.numberOfStereoHits()),
    Chi2(track.chi2()),DegreesOfFreedom(track.degreesOfFreedom()), Chi2perDegreeOfFreedom(track.chi2PerDegreeOfFreedom()),
    KalmanFitSuccessful(track.kalmanFitSuccessful()),
	FirstStateZ(track.firstStateZ()), FirstStateX(track.firstStateX()), FirstStateY(track.firstStateY()),
	FirstStateXSlope(track.firstStateXSlope()), FirstStateYSlope(track.firstStateYSlope()),
    LinkedTrackID(track.linkedTrackID()), 
    RecoSimLinks(track.recoSimLinks()),
    FractionOfHitsSharedWithLinkedTrack(track.fractionOfHitsSharedWithLinkedTrack()),
    ProcessIDofLinkedTrack(track.processIDofLinkedTrack()),
    HitsReassignedDuringKinematicFit(track.hitsReassigned())
    {

        LinearFitCovarianceMatrix.ResizeTo(track.linearFitCovarianceMatrix().GetNrows(), track.linearFitCovarianceMatrix().GetNcols());
        LinearFitCovarianceMatrix = track.linearFitCovarianceMatrix();

        if(track.kalmanFitSuccessful()) {

            auto const& cov = track.firstStateCovarianceMatrix();

            FirstStateXError = TMath::Sqrt(cov(0,0));
            FirstStateYError = TMath::Sqrt(cov(1,1));
            FirstStateXSlopeError = TMath::Sqrt(cov(2,2));
            FirstStateYSlopeError = TMath::Sqrt(cov(3,3)); 
            FirstStateCovarianceMatrix = cov;

        }

        for(auto& h : track.hitsCopy()) {

            Hits.emplace_back(h, track.perpendicularDistanceToHitSigned(h), track.distanceToHitInModulePlaneSigned(h));
        }
    }    

    Int_t sector() const {return Sector;}
    Int_t index() const {return Index;}

    Double_t z0() const {return Z0;}

    Double_t xSlope() const {return XSlope;}
    Double_t xSlopeError() const {return XSlopeError;}

    Double_t x0() const {return X0;}
    Double_t x0Error() const {return X0Error;}

    Double_t ySlope() const {return YSlope;}
    Double_t ySlopeError() const {return YSlopeError;}

    Double_t y0() const {return Y0;}
    Double_t y0Error() const {return Y0Error;}

    std::vector<MUonERecoOutputHitFull> const& hits() const {return Hits;}
    TMatrixD const& linearFitCovarianceMatrix() const {return LinearFitCovarianceMatrix;}
    

    Int_t numberOfXProjectionHits() const {return NumberOfXProjectionHits;}
    Int_t numberOfYProjectionHits() const {return NumberOfYProjectionHits;}
    Int_t numberOfStereoHits() const {return NumberOfStereoHits;}

    Double_t chi2() const {return Chi2;}
    Int_t degreesOfFreedom() const {return DegreesOfFreedom;}
    Double_t chi2perDegreeOfFreedom() const {return Chi2perDegreeOfFreedom;}

    Bool_t kalmanFitSuccessful() const {return KalmanFitSuccessful;}

	Double_t firstStateZ() const {return FirstStateZ;}
	Double_t firstStateX() const {return FirstStateX;}
	Double_t firstStateXError() const {return FirstStateXError;}
	Double_t firstStateY() const {return FirstStateY;}
	Double_t firstStateYError() const {return FirstStateYError;}

	Double_t firstStateXSlope() const {return FirstStateXSlope;}
	Double_t firstStateXSlopeError() const {return FirstStateXSlopeError;}
	Double_t firstStateYSlope() const {return FirstStateYSlope;}
	Double_t firstStateYSlopeError() const {return FirstStateYSlopeError;}

    ROOT::Math::SMatrix<Double_t, 4> const& firstStateCovarianceMatrix() const {return FirstStateCovarianceMatrix;}   

    std::vector<std::tuple<Int_t, Double_t, Int_t>> const& recoSimLinks() const {return RecoSimLinks;}
    Int_t linkedTrackID() const {return LinkedTrackID;}
    Double_t fractionOfHitsSharedWithLinkedTrack() const {return FractionOfHitsSharedWithLinkedTrack;}
    Int_t processIDofLinkedTrack() const {return ProcessIDofLinkedTrack;}

    Bool_t hitsReassignedDuringKinematicFit() const {return HitsReassignedDuringKinematicFit;}

private:

    Int_t Sector{-1};
    Int_t Index{-1};

    Double_t Z0{0};

    Double_t XSlope{0};
    Double_t XSlopeError{0};

    Double_t X0{0};
    Double_t X0Error{0};

    Double_t YSlope{0};
    Double_t YSlopeError{0};

    Double_t Y0{0};
    Double_t Y0Error{0};

    std::vector<MUonERecoOutputHitFull> Hits;

    TMatrixD LinearFitCovarianceMatrix;

    Int_t NumberOfXProjectionHits{0};
    Int_t NumberOfYProjectionHits{0};
    Int_t NumberOfStereoHits{0};

    Double_t Chi2{0};
    Int_t DegreesOfFreedom{0};
    Double_t Chi2perDegreeOfFreedom{0};

    Bool_t KalmanFitSuccessful{false};

	Double_t FirstStateZ{0};
	Double_t FirstStateX{0};
    Double_t FirstStateXError{0};
	Double_t FirstStateY{0};
    Double_t FirstStateYError{0};

	Double_t FirstStateXSlope{0};
	Double_t FirstStateXSlopeError{0};
    
	Double_t FirstStateYSlope{0};
	Double_t FirstStateYSlopeError{0};

    ROOT::Math::SMatrix<Double_t, 4> FirstStateCovarianceMatrix;

    std::vector<std::tuple<Int_t, Double_t, Int_t>> RecoSimLinks;
    Int_t LinkedTrackID{-1};
    Double_t FractionOfHitsSharedWithLinkedTrack{0};
    Int_t ProcessIDofLinkedTrack{-1};

    Bool_t HitsReassignedDuringKinematicFit{false};

    public:

        MUonERecoOutputTrackFull(MUonERecoOutputTrackFull const& track)
        : Sector(track.sector()), Index(track.index()),
        Z0(track.z0()),
        XSlope(track.xSlope()), XSlopeError(track.xSlopeError()),
        X0(track.x0()), X0Error(track.x0Error()), 
        YSlope(track.ySlope()), YSlopeError(track.ySlopeError()),
        Y0(track.y0()), Y0Error(track.y0Error()), 
        NumberOfXProjectionHits(track.numberOfXProjectionHits()), NumberOfYProjectionHits(track.numberOfYProjectionHits()), NumberOfStereoHits(track.numberOfStereoHits()),
        Chi2(track.chi2()),DegreesOfFreedom(track.degreesOfFreedom()), Chi2perDegreeOfFreedom(track.chi2perDegreeOfFreedom()),
        KalmanFitSuccessful(track.kalmanFitSuccessful()),
        FirstStateZ(track.firstStateZ()), FirstStateX(track.firstStateX()), FirstStateY(track.firstStateY()),
        FirstStateXSlope(track.firstStateXSlope()), FirstStateYSlope(track.firstStateYSlope()),
        LinkedTrackID(track.linkedTrackID()), 
        FractionOfHitsSharedWithLinkedTrack(track.fractionOfHitsSharedWithLinkedTrack()),
        ProcessIDofLinkedTrack(track.processIDofLinkedTrack()),
        HitsReassignedDuringKinematicFit(track.hitsReassignedDuringKinematicFit()),
        FirstStateXError(track.firstStateXError()),
        FirstStateYError(track.firstStateYError()),
        FirstStateXSlopeError(track.firstStateXSlopeError()),
        FirstStateYSlopeError(track.firstStateYSlopeError()),
        FirstStateCovarianceMatrix(track.firstStateCovarianceMatrix()),
        Hits(track.hits()),
	    RecoSimLinks(track.recoSimLinks())
        {

            LinearFitCovarianceMatrix.ResizeTo(track.linearFitCovarianceMatrix().GetNrows(), track.linearFitCovarianceMatrix().GetNcols());
            LinearFitCovarianceMatrix = track.linearFitCovarianceMatrix();

        }    

        MUonERecoOutputTrackFull& operator=(MUonERecoOutputTrackFull const& track)
        {
            Sector = track.sector(); 
            Index = track.index();
            Z0 = track.z0();
            XSlope = track.xSlope(); 
            XSlopeError = track.xSlopeError();
            X0 = track.x0(); 
            X0Error = track.x0Error(); 
            YSlope = track.ySlope(); 
            YSlopeError = track.ySlopeError();
            Y0 = track.y0(); 
            Y0Error = track.y0Error(); 
            NumberOfXProjectionHits = track.numberOfXProjectionHits(); 
            NumberOfYProjectionHits = track.numberOfYProjectionHits(); 
            NumberOfStereoHits = track.numberOfStereoHits();
            Chi2 = track.chi2();
            DegreesOfFreedom = track.degreesOfFreedom(); 
            Chi2perDegreeOfFreedom = track.chi2perDegreeOfFreedom();
            KalmanFitSuccessful = track.kalmanFitSuccessful();
            FirstStateZ = track.firstStateZ(); 
            FirstStateX = track.firstStateX(); 
            FirstStateY = track.firstStateY();
            FirstStateXSlope = track.firstStateXSlope(); 
            FirstStateYSlope = track.firstStateYSlope();
            LinkedTrackID = track.linkedTrackID(); 
            FractionOfHitsSharedWithLinkedTrack = track.fractionOfHitsSharedWithLinkedTrack();
            ProcessIDofLinkedTrack = track.processIDofLinkedTrack();
            HitsReassignedDuringKinematicFit = track.hitsReassignedDuringKinematicFit();
            FirstStateXError = track.firstStateXError();
            FirstStateYError = track.firstStateYError();
            FirstStateXSlopeError = track.firstStateXSlopeError();
            FirstStateYSlopeError = track.firstStateYSlopeError();
            FirstStateCovarianceMatrix = track.firstStateCovarianceMatrix(),
            Hits = track.hits();
	        RecoSimLinks = track.recoSimLinks();


            LinearFitCovarianceMatrix.ResizeTo(track.linearFitCovarianceMatrix().GetNrows(), track.linearFitCovarianceMatrix().GetNcols());
            LinearFitCovarianceMatrix = track.linearFitCovarianceMatrix();

            return *this;
        }    


    ClassDef(MUonERecoOutputTrackFull,1)
};

class MUonERecoOutputVertexFull {

public:

    MUonERecoOutputVertexFull() = default;

    MUonERecoOutputVertexFull(MUonERecoVertex const& vertex)
    : IncomingMuon(vertex.incomingTrack()),
    StationIndex(vertex.stationIndex()),
    AlternativePIDHypothesis(vertex.alternativePIDHypothesis()),
    XKinematicFit(vertex.xKinematicFit()), XKinematicFitError(vertex.xKinematicFitError()), 
    YKinematicFit(vertex.yKinematicFit()), YKinematicFitError(vertex.yKinematicFitError()), 
    ZKinematicFit(vertex.zKinematicFit()), ZKinematicFitError(vertex.zKinematicFitError()),
    XPositionFit(vertex.xPositionFit()), XPositionFitError(vertex.xPositionFitError()), 
    YPositionFit(vertex.yPositionFit()), YPositionFitError(vertex.yPositionFitError()), 
    ZPositionFit(vertex.zPositionFit()), ZPositionFitError(vertex.zPositionFitError()),
    PositionFitSuccessful(vertex.positionFitSuccessful()), PositionFitCovarianceMatrix(vertex.positionFitCovarianceMatrix()),
    Chi2perDegreeOfFreedom(vertex.chi2PerDegreeOfFreedom()),
    HitsReassignedDuringKinematicFit(vertex.hitsReassigned()),
    OutgoingElectron(vertex.assumedElectronTrack()),
    ElectronTheta(vertex.assumedElectronTrackTheta()),
    ElectronThetaError(vertex.assumedElectronTrackThetaError()),
    OutgoingMuon(vertex.assumedMuonTrack()),
    MuonTheta(vertex.assumedMuonTrackTheta()),
    MuonThetaError(vertex.assumedMuonTrackThetaError())
    {

        KinematicFitCovarianceMatrix.ResizeTo(vertex.kinematicFitCovarianceMatrix().GetNrows(), vertex.kinematicFitCovarianceMatrix().GetNcols());
        KinematicFitCovarianceMatrix = vertex.kinematicFitCovarianceMatrix();


        auto i = vertex.incomingTrack().directionVector().Unit();
        auto o1 = vertex.assumedMuonTrack().directionVector().Unit();
        auto o2 = vertex.assumedElectronTrack().directionVector().Unit();

        TripleProduct = i.Dot(o1.Cross(o2));
        Acoplanarity = M_PI_2 - i.Angle(o1.Cross(o2));
        ModifiedAcoplanarity = M_PI - (i.Cross(o1)).Angle(i.Cross(o2)); 
        if(TripleProduct < 0) ModifiedAcoplanarity *= -1;


    }

    MUonERecoOutputTrackFull incomingMuon() const {return IncomingMuon;}
    MUonERecoOutputTrackFull outgoingMuon() const {return OutgoingMuon;}
    MUonERecoOutputTrackFull outgoingElectron() const {return OutgoingElectron;}

    Int_t stationIndex() const {return StationIndex;}

    Bool_t alternativePIDHypothesis() const {return AlternativePIDHypothesis;}

    Double_t muonTheta() const {return MuonTheta;}
    Double_t muonThetaError() const {return MuonThetaError;}

    Double_t electronTheta() const {return ElectronTheta;}
    Double_t electronThetaError() const {return ElectronThetaError;}

    Double_t tripleProduct() const {return TripleProduct;}
    Double_t acoplanarity() const {return Acoplanarity;}
    Double_t modifiedAcoplanarity() const {return ModifiedAcoplanarity;}

    Double_t xKinematicFit() const {return XKinematicFit;}
    Double_t xKinematicFitError() const {return XKinematicFitError;}

    Double_t yKinematicFit() const {return YKinematicFit;}
    Double_t yKinematicFitError() const {return YKinematicFitError;}

    Double_t zKinematicFit() const {return ZKinematicFit;}
    Double_t zKinematicFitError() const {return ZKinematicFitError;}

    Double_t xPositionFit() const {return XPositionFit;}
    Double_t xPositionFitError() const {return XPositionFitError;}

    Double_t yPositionFit() const {return YPositionFit;}
    Double_t yPositionFitError() const {return YPositionFitError;}

    Double_t zPositionFit() const {return ZPositionFit;}
    Double_t zPositionFitError() const {return ZPositionFitError;}

    Bool_t positionFitSuccessful() const {return PositionFitSuccessful;}
    ROOT::Math::SMatrix<Double_t, 3> const& positionFitCovarianceMatrix() const {return PositionFitCovarianceMatrix;}    
    TMatrixD const& kinematicFitCovarianceMatrix() const {return KinematicFitCovarianceMatrix;}

    Double_t chi2perDegreeOfFreedom() const {return Chi2perDegreeOfFreedom;}

    Bool_t hitsReassignedDuringKinematicFit() const {return HitsReassignedDuringKinematicFit;}

private:

    MUonERecoOutputTrackFull IncomingMuon;
    MUonERecoOutputTrackFull OutgoingMuon;
    MUonERecoOutputTrackFull OutgoingElectron;

    Int_t StationIndex{-1};

    Bool_t AlternativePIDHypothesis{false};

    Double_t MuonTheta{0};
    Double_t MuonThetaError{0};

    Double_t ElectronTheta{0};
    Double_t ElectronThetaError{0};

    Double_t TripleProduct{0};
    Double_t Acoplanarity{0};
    Double_t ModifiedAcoplanarity{0};

    Double_t XKinematicFit{0};
    Double_t XKinematicFitError{0};

    Double_t YKinematicFit{0};
    Double_t YKinematicFitError{0};

    Double_t ZKinematicFit{0};
    Double_t ZKinematicFitError{0};

    Double_t XPositionFit{0};
    Double_t XPositionFitError{0};

    Double_t YPositionFit{0};
    Double_t YPositionFitError{0};

    Double_t ZPositionFit{0};
    Double_t ZPositionFitError{0};

    Bool_t PositionFitSuccessful{false};
    ROOT::Math::SMatrix<Double_t, 3> PositionFitCovarianceMatrix;
    TMatrixD KinematicFitCovarianceMatrix;

    Double_t Chi2perDegreeOfFreedom{0};

    Bool_t HitsReassignedDuringKinematicFit{false};


public:

    MUonERecoOutputVertexFull(MUonERecoOutputVertexFull const& vertex)
    : IncomingMuon(vertex.incomingMuon()),
    OutgoingElectron(vertex.outgoingElectron()),
    OutgoingMuon(vertex.outgoingMuon()),
    ElectronTheta(vertex.electronTheta()), ElectronThetaError(vertex.electronThetaError()),
    MuonTheta(vertex.muonTheta()), MuonThetaError(vertex.muonThetaError()),
    TripleProduct(vertex.tripleProduct()), Acoplanarity(vertex.acoplanarity()), ModifiedAcoplanarity(vertex.modifiedAcoplanarity()),
    StationIndex(vertex.stationIndex()),
    AlternativePIDHypothesis(vertex.alternativePIDHypothesis()),
    XKinematicFit(vertex.xKinematicFit()), XKinematicFitError(vertex.xKinematicFitError()), 
    YKinematicFit(vertex.yKinematicFit()), YKinematicFitError(vertex.yKinematicFitError()), 
    ZKinematicFit(vertex.zKinematicFit()), ZKinematicFitError(vertex.zKinematicFitError()),
    XPositionFit(vertex.xPositionFit()), XPositionFitError(vertex.xPositionFitError()), 
    YPositionFit(vertex.yPositionFit()), YPositionFitError(vertex.yPositionFitError()), 
    ZPositionFit(vertex.zPositionFit()), ZPositionFitError(vertex.zPositionFitError()),
    PositionFitSuccessful(vertex.positionFitSuccessful()), PositionFitCovarianceMatrix(vertex.positionFitCovarianceMatrix()),
    Chi2perDegreeOfFreedom(vertex.chi2perDegreeOfFreedom()),
    HitsReassignedDuringKinematicFit(vertex.hitsReassignedDuringKinematicFit())
    {

        KinematicFitCovarianceMatrix.ResizeTo(vertex.kinematicFitCovarianceMatrix().GetNrows(), vertex.kinematicFitCovarianceMatrix().GetNcols());
        KinematicFitCovarianceMatrix = vertex.kinematicFitCovarianceMatrix();

    }

    MUonERecoOutputVertexFull& operator=(MUonERecoOutputVertexFull const& vertex)
    {

        IncomingMuon = vertex.incomingMuon();
        OutgoingElectron = vertex.outgoingElectron();
        OutgoingMuon = vertex.outgoingMuon();
        ElectronTheta = vertex.electronTheta(); 
        ElectronThetaError = vertex.electronThetaError();
        MuonTheta = vertex.muonTheta(); 
        MuonThetaError = vertex.muonThetaError();
        TripleProduct = vertex.tripleProduct(); 
        Acoplanarity = vertex.acoplanarity(); 
        ModifiedAcoplanarity = vertex.modifiedAcoplanarity();
        StationIndex = vertex.stationIndex();
        AlternativePIDHypothesis = vertex.alternativePIDHypothesis();
        XKinematicFit = vertex.xKinematicFit(); 
        XKinematicFitError = vertex.xKinematicFitError(); 
        YKinematicFit = vertex.yKinematicFit(); 
        YKinematicFitError = vertex.yKinematicFitError(); 
        ZKinematicFit = vertex.zKinematicFit(); 
        ZKinematicFitError = vertex.zKinematicFitError();
        XPositionFit = vertex.xPositionFit(); 
        XPositionFitError = vertex.xPositionFitError(); 
        YPositionFit = vertex.yPositionFit(); 
        YPositionFitError = vertex.yPositionFitError(); 
        ZPositionFit = vertex.zPositionFit(); 
        ZPositionFitError = vertex.zPositionFitError();
        PositionFitSuccessful = vertex.positionFitSuccessful(); 
        PositionFitCovarianceMatrix = vertex.positionFitCovarianceMatrix();
        Chi2perDegreeOfFreedom = vertex.chi2perDegreeOfFreedom();
        HitsReassignedDuringKinematicFit = vertex.hitsReassignedDuringKinematicFit();


        KinematicFitCovarianceMatrix.ResizeTo(vertex.kinematicFitCovarianceMatrix().GetNrows(), vertex.kinematicFitCovarianceMatrix().GetNcols());
        KinematicFitCovarianceMatrix = vertex.kinematicFitCovarianceMatrix();

        return *this;
    }


    ClassDef(MUonERecoOutputVertexFull,1)
};


class MUonERecoOutputGenericVertexFull {

public:

    MUonERecoOutputGenericVertexFull() = default;

    MUonERecoOutputGenericVertexFull(MUonERecoGenericVertex const& vertex)
    : IncomingTrack(vertex.incomingTrack()),
    Multiplicity(vertex.multiplicity()),
    StationIndex(vertex.stationIndex()),
    XKinematicFit(vertex.xKinematicFit()), XKinematicFitError(vertex.xKinematicFitError()), 
    YKinematicFit(vertex.yKinematicFit()), YKinematicFitError(vertex.yKinematicFitError()), 
    ZKinematicFit(vertex.zKinematicFit()), ZKinematicFitError(vertex.zKinematicFitError()),
    XPositionFit(vertex.xPositionFit()), XPositionFitError(vertex.xPositionFitError()), 
    YPositionFit(vertex.yPositionFit()), YPositionFitError(vertex.yPositionFitError()), 
    ZPositionFit(vertex.zPositionFit()), ZPositionFitError(vertex.zPositionFitError()),
    PositionFitSuccessful(vertex.positionFitSuccessful()), PositionFitCovarianceMatrix(vertex.positionFitCovarianceMatrix()),
    Chi2perDegreeOfFreedom(vertex.chi2PerDegreeOfFreedom()),
    HitsReassignedDuringKinematicFit(vertex.hitsReassigned())
    {

        KinematicFitCovarianceMatrix.ResizeTo(vertex.kinematicFitCovarianceMatrix().GetNrows(), vertex.kinematicFitCovarianceMatrix().GetNcols());
        KinematicFitCovarianceMatrix = vertex.kinematicFitCovarianceMatrix();

        OutgoingTracks.reserve(vertex.multiplicity());
        for(auto const& track : vertex.outgoingTracks())
            OutgoingTracks.emplace_back(track);
    }

    MUonERecoOutputTrackFull incomingTrack() const {return IncomingTrack;}
    std::vector<MUonERecoOutputTrackFull> outgoingTracks() const {return OutgoingTracks;}


    Int_t multiplicity() const {return Multiplicity;}
    Int_t stationIndex() const {return StationIndex;}

    Double_t xKinematicFit() const {return XKinematicFit;}
    Double_t xKinematicFitError() const {return XKinematicFitError;}

    Double_t yKinematicFit() const {return YKinematicFit;}
    Double_t yKinematicFitError() const {return YKinematicFitError;}

    Double_t zKinematicFit() const {return ZKinematicFit;}
    Double_t zKinematicFitError() const {return ZKinematicFitError;}

    Double_t xPositionFit() const {return XPositionFit;}
    Double_t xPositionFitError() const {return XPositionFitError;}

    Double_t yPositionFit() const {return YPositionFit;}
    Double_t yPositionFitError() const {return YPositionFitError;}

    Double_t zPositionFit() const {return ZPositionFit;}
    Double_t zPositionFitError() const {return ZPositionFitError;}

    Bool_t positionFitSuccessful() const {return PositionFitSuccessful;}
    ROOT::Math::SMatrix<Double_t, 3> const& positionFitCovarianceMatrix() const {return PositionFitCovarianceMatrix;}    
    TMatrixD const& kinematicFitCovarianceMatrix() const {return KinematicFitCovarianceMatrix;}


    Double_t chi2perDegreeOfFreedom() const {return Chi2perDegreeOfFreedom;}

    Bool_t hitsReassignedDuringKinematicFit() const {return HitsReassignedDuringKinematicFit;}


private:

    MUonERecoOutputTrackFull IncomingTrack;
    std::vector<MUonERecoOutputTrackFull> OutgoingTracks;

    Int_t Multiplicity{0};

    Int_t StationIndex{-1};

    Double_t XKinematicFit{0};
    Double_t XKinematicFitError{0};

    Double_t YKinematicFit{0};
    Double_t YKinematicFitError{0};

    Double_t ZKinematicFit{0};
    Double_t ZKinematicFitError{0};

    Double_t XPositionFit{0};
    Double_t XPositionFitError{0};

    Double_t YPositionFit{0};
    Double_t YPositionFitError{0};

    Double_t ZPositionFit{0};
    Double_t ZPositionFitError{0};

    Bool_t PositionFitSuccessful{false};
    ROOT::Math::SMatrix<Double_t, 3> PositionFitCovarianceMatrix;
    TMatrixD KinematicFitCovarianceMatrix;


    Double_t Chi2perDegreeOfFreedom{0};

    Bool_t HitsReassignedDuringKinematicFit{false};

public:

    MUonERecoOutputGenericVertexFull(MUonERecoOutputGenericVertexFull const& vertex)
    : IncomingTrack(vertex.incomingTrack()),
    OutgoingTracks(vertex.outgoingTracks()),
    Multiplicity(vertex.multiplicity()),
    StationIndex(vertex.stationIndex()),
    XKinematicFit(vertex.xKinematicFit()), XKinematicFitError(vertex.xKinematicFitError()), 
    YKinematicFit(vertex.yKinematicFit()), YKinematicFitError(vertex.yKinematicFitError()), 
    ZKinematicFit(vertex.zKinematicFit()), ZKinematicFitError(vertex.zKinematicFitError()),
    XPositionFit(vertex.xPositionFit()), XPositionFitError(vertex.xPositionFitError()), 
    YPositionFit(vertex.yPositionFit()), YPositionFitError(vertex.yPositionFitError()), 
    ZPositionFit(vertex.zPositionFit()), ZPositionFitError(vertex.zPositionFitError()),
    PositionFitSuccessful(vertex.positionFitSuccessful()), PositionFitCovarianceMatrix(vertex.positionFitCovarianceMatrix()),
    Chi2perDegreeOfFreedom(vertex.chi2perDegreeOfFreedom()),
    HitsReassignedDuringKinematicFit(vertex.hitsReassignedDuringKinematicFit())
    {

        KinematicFitCovarianceMatrix.ResizeTo(vertex.kinematicFitCovarianceMatrix().GetNrows(), vertex.kinematicFitCovarianceMatrix().GetNcols());
        KinematicFitCovarianceMatrix = vertex.kinematicFitCovarianceMatrix();

    }

    MUonERecoOutputGenericVertexFull& operator=(MUonERecoOutputGenericVertexFull const& vertex)
    {

        IncomingTrack = vertex.incomingTrack();
        OutgoingTracks = vertex.outgoingTracks();
        Multiplicity = vertex.multiplicity();
        StationIndex = vertex.stationIndex();
        XKinematicFit = vertex.xKinematicFit(); 
        XKinematicFitError = vertex.xKinematicFitError(); 
        YKinematicFit = vertex.yKinematicFit(); 
        YKinematicFitError = vertex.yKinematicFitError(); 
        ZKinematicFit = vertex.zKinematicFit(); 
        ZKinematicFitError = vertex.zKinematicFitError();
        XPositionFit = vertex.xPositionFit(); 
        XPositionFitError = vertex.xPositionFitError(); 
        YPositionFit = vertex.yPositionFit(); 
        YPositionFitError = vertex.yPositionFitError(); 
        ZPositionFit = vertex.zPositionFit(); 
        ZPositionFitError = vertex.zPositionFitError();
        PositionFitSuccessful = vertex.positionFitSuccessful(); 
        PositionFitCovarianceMatrix = vertex.positionFitCovarianceMatrix();
        Chi2perDegreeOfFreedom = vertex.chi2perDegreeOfFreedom();
        HitsReassignedDuringKinematicFit = vertex.hitsReassignedDuringKinematicFit();


        KinematicFitCovarianceMatrix.ResizeTo(vertex.kinematicFitCovarianceMatrix().GetNrows(), vertex.kinematicFitCovarianceMatrix().GetNcols());
        KinematicFitCovarianceMatrix = vertex.kinematicFitCovarianceMatrix();

        return *this;
    }    


    ClassDef(MUonERecoOutputGenericVertexFull,1)
};


class MUonERecoOutputAdaptiveFitterVertexFull {

public:

    MUonERecoOutputAdaptiveFitterVertexFull() = default;

    MUonERecoOutputAdaptiveFitterVertexFull(MUonERecoAdaptiveFitterVertex const& vertex, std::vector<std::vector<MUonERecoTrack3D>> const& reconstructedTracksPerSector) 
        : StationIndex(vertex.stationIndex()), TrackWeights(vertex.trackWeights()),
        X(vertex.position().x()), XError(TMath::Sqrt(vertex.positionCovariance()(0,0))),
        Y(vertex.position().y()), YError(TMath::Sqrt(vertex.positionCovariance()(1,1))),
        Z(vertex.position().z()), ZError(TMath::Sqrt(vertex.positionCovariance()(2,2))),
        Chi2(vertex.chi2()), PositionCovariance(vertex.positionCovariance())
    {
        auto const& tracks = reconstructedTracksPerSector[vertex.targetIndex() + 1]; //tracks after the target in which the vertex was reconstructed
        auto const& track_id = vertex.trackID();

        for(auto const& id : track_id) {

            Tracks.emplace_back(tracks[id]);
        }
    }

    Int_t stationIndex() const {return StationIndex;}

    std::vector<MUonERecoOutputTrackFull> const& tracks() const {return Tracks;}
    std::vector<Double_t> const& trackWeights() const {return TrackWeights;}    

    Double_t x() const {return X;}
    Double_t xError() const {return XError;}

    Double_t y() const {return Y;}
    Double_t yError() const {return YError;}

    Double_t z() const {return Z;}
    Double_t zError() const {return ZError;}

    Double_t chi2() const {return Chi2;}

    ROOT::Math::SMatrix<Double_t, 3> const& positionCovariance() const {return PositionCovariance;}    

private:

    Int_t StationIndex{-1};

    std::vector<MUonERecoOutputTrackFull> Tracks;
    std::vector<Double_t> TrackWeights;    

    Double_t X{0};
    Double_t XError{0};

    Double_t Y{0};
    Double_t YError{0};

    Double_t Z{0};
    Double_t ZError{0};

    Double_t Chi2{0};

    ROOT::Math::SMatrix<Double_t, 3> PositionCovariance;
    
    ClassDef(MUonERecoOutputAdaptiveFitterVertexFull,1)
};


/*
    Class used to store reconstruction output.
*/

class MUonERecoOutputFull : public MUonERecoOutputBase {

public:

    MUonERecoOutputFull() = default;

    //used for events that failed to reconstruct
    MUonERecoOutputFull(Int_t eventNumber, Double_t eventEnergy, std::vector<std::vector<std::vector<MUonERecoHit>>> const& hitsPerModulePerSector, std::vector<std::vector<MUonERecoTrack3D>> const& reconstructedTracksPerSector, std::vector<MUonERecoAdaptiveFitterVertex> afVertices, MUonEReconstructionConfiguration const& recoConfig, Bool_t event_skipped)
        : MUonERecoOutputBase(MUonEReconstructionConfiguration::OutputFormat::full),
        IsReconstructed(false), SourceEventNumber(eventNumber), IsMC(recoConfig.isMC()), TotalEventEnergy(eventEnergy), EventSkipped(event_skipped)
        {

            ReconstructedHits.reserve(50);
            ReconstructedHitsMultiplicity = 0;

            for(auto const& sectors : hitsPerModulePerSector) {
                for(auto const& modules : sectors) {
                    for(auto const& hit : modules) {

                        ReconstructedHits.emplace_back(hit, 0, 0);
                        ++ReconstructedHitsMultiplicity;
                    }
                }
            }
                

            ReconstructedTracks.reserve(50);
            ReconstructedTracksMultiplicity = 0;


            for(auto const& sectors : reconstructedTracksPerSector) {
                for(auto const& track : sectors) {

                    ReconstructedTracks.emplace_back(track);
                    ++ReconstructedTracksMultiplicity;
                }
            }
            

            if(!afVertices.empty()) {

                AdaptiveFitterVerticesMultiplicity = afVertices.size();
                AdaptiveFitterVertices.reserve(afVertices.size());

                for(auto& afv : afVertices) {

                    AdaptiveFitterVertices.emplace_back(afv, reconstructedTracksPerSector);
                }
            }            

        }


    //used for correctly reconstructed event
    MUonERecoOutputFull(Int_t eventNumber, Double_t eventEnergy, std::vector<std::vector<std::vector<MUonERecoHit>>> const& hitsPerModulePerSector, std::vector<std::vector<MUonERecoTrack3D>> const& reconstructedTracksPerSector, std::vector<MUonERecoVertex> const& vertices, std::vector<MUonERecoAdaptiveFitterVertex> afVertices, std::vector<MUonERecoGenericVertex> genericVertices, MUonEReconstructionConfiguration const& recoConfig)
        : MUonERecoOutputBase(MUonEReconstructionConfiguration::OutputFormat::full),
        IsReconstructed(true), SourceEventNumber(eventNumber), IsMC(recoConfig.isMC()), TotalEventEnergy(eventEnergy), BestVertex(vertices[0])
        {

            ReconstructedHits.reserve(50);
            ReconstructedHitsMultiplicity = 0;

            for(auto const& sectors : hitsPerModulePerSector) {
                for(auto const& modules : sectors) {
                    for(auto const& hit : modules) {

                        ReconstructedHits.emplace_back(hit, 0, 0);
                        ++ReconstructedHitsMultiplicity;
                    }
                }
            }

            ReconstructedTracks.reserve(50);
            ReconstructedTracksMultiplicity = 0;


            for(auto const& sectors : reconstructedTracksPerSector) {
                for(auto const& track : sectors) {

                    ReconstructedTracks.emplace_back(track);
                    ++ReconstructedTracksMultiplicity;
                }
            }
            

            ReconstructedVertices.reserve(vertices.size());


            ReconstructedVerticesMultiplicity = vertices.size();
            for(auto const& vertex : vertices)
                ReconstructedVertices.emplace_back(vertex);                    
            

            if(!afVertices.empty()) {

                AdaptiveFitterVerticesMultiplicity = afVertices.size();
                AdaptiveFitterVertices.reserve(afVertices.size());

                for(auto& afv : afVertices) {

                    AdaptiveFitterVertices.emplace_back(afv, reconstructedTracksPerSector);
                }
            }

            ReconstructedGenericVerticesMultiplicity = genericVertices.size();
            ReconstructedGenericVertices.reserve(genericVertices.size());
            for(auto const& vertex : genericVertices)
                ReconstructedGenericVertices.emplace_back(vertex);
        }




    Bool_t isReconstructed() const {return IsReconstructed;}

    Int_t sourceEventNumber() const {return SourceEventNumber;}

    Bool_t isMC() const {return IsMC;}
    Bool_t eventSkipped() const {return EventSkipped;}

    Double_t totalEventEnergy() const {return TotalEventEnergy;}

    MUonERecoOutputVertexFull const& bestVertex() const {return BestVertex;}

    Int_t reconstructedHitsMultiplicity() const {return ReconstructedHitsMultiplicity;}
    std::vector<MUonERecoOutputHitFull> const& reconstructedHits() const {return ReconstructedHits;}

    Int_t reconstructedTracksMultiplicity() const {return ReconstructedTracksMultiplicity;}
    std::vector<MUonERecoOutputTrackFull> const& reconstructedTracks() const {return ReconstructedTracks;}

    Int_t reconstructedVerticesMultiplicity() const {return ReconstructedVerticesMultiplicity;}
    std::vector<MUonERecoOutputVertexFull> const& reconstructedVertices() const {return ReconstructedVertices;}

    Int_t adaptiveFitterVerticesMultiplicity() const {return AdaptiveFitterVerticesMultiplicity;}
    std::vector<MUonERecoOutputAdaptiveFitterVertexFull> const& adaptiveFitterVertices() const {return AdaptiveFitterVertices;}

    Int_t reconstructedGenericVerticesMultiplicity() const {return ReconstructedGenericVerticesMultiplicity;}
    std::vector<MUonERecoOutputGenericVertexFull> const& reconstructedGenericVertices() const {return ReconstructedGenericVertices;}

private:

    Bool_t IsReconstructed{false};

    Int_t SourceEventNumber{-1};

    Bool_t IsMC{true};
    Bool_t EventSkipped{false};

    Double_t TotalEventEnergy{0};


    MUonERecoOutputVertexFull BestVertex;

    Int_t ReconstructedHitsMultiplicity{0};
    std::vector<MUonERecoOutputHitFull> ReconstructedHits;

    Int_t ReconstructedTracksMultiplicity{0};
    std::vector<MUonERecoOutputTrackFull> ReconstructedTracks;

    Int_t ReconstructedVerticesMultiplicity{0};
    std::vector<MUonERecoOutputVertexFull> ReconstructedVertices;

    Int_t AdaptiveFitterVerticesMultiplicity{0};
    std::vector<MUonERecoOutputAdaptiveFitterVertexFull> AdaptiveFitterVertices;

    Int_t ReconstructedGenericVerticesMultiplicity{0};
    std::vector<MUonERecoOutputGenericVertexFull> ReconstructedGenericVertices;


    ClassDef(MUonERecoOutputFull,1)
};

#endif//MUONERECOOUTPUTFULL_H 

