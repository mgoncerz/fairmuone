#ifndef MUONEPARTICLEGUNBOXCONFIGURATION_H
#define MUONEPARTICLEGUNBOXCONFIGURATION_H

#include "MUonEBeamGeneratorConfiguration.h"
#include <yaml-cpp/yaml.h>

#include "Rtypes.h"

class MUonEParticleGunBoxConfiguration : public MUonEBeamGeneratorConfiguration {

public:

    MUonEParticleGunBoxConfiguration();

    Int_t pdg() const {return m_pdg;}

    Bool_t massSet() const {return m_massSet;}
    Double_t mass() const {return m_mass;}

    Bool_t energySet() const {return m_energySet;}
    Double_t energy() const {return m_energy;}

    Bool_t momentumSet() const {return m_momentumSet;}
    Double_t momentum() const {return m_momentum;}

    Double_t z() const {return m_z;}
    Double_t xMin() const {return m_xMin;}
    Double_t xMax() const {return m_xMax;}
    Double_t yMin() const {return m_yMin;}
    Double_t yMax() const {return m_yMax;}


    Double_t thetaMin() const {return m_thetaMin;}
    Double_t thetaMax() const {return m_thetaMax;}


    virtual Bool_t isEnergyValid() const override {
        return m_energySet;
    }

    virtual Bool_t readConfiguration(YAML::Node const& config, std::string opt) override;
    virtual void logCurrentConfiguration() const override;
    virtual void resetDefaultConfiguration() override;


protected:

    Int_t m_pdg{0};

    Bool_t m_massSet{false};
    Double_t m_mass{0};

    Bool_t m_energySet{false};
    Double_t m_energy{0};

    Bool_t m_momentumSet{false};
    Double_t m_momentum{0};

    Double_t m_z{0};
    Double_t m_xMin{0};
    Double_t m_xMax{0};
    Double_t m_yMin{0};
    Double_t m_yMax{0};    

    Double_t m_thetaMin{0};
    Double_t m_thetaMax{0};


    ClassDef(MUonEParticleGunBoxConfiguration, 2)

};

#endif //MUONEPARTICLEGUNBOXCONFIGURATION_H

