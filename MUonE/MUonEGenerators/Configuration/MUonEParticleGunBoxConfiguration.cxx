#include "MUonEParticleGunBoxConfiguration.h"

#include <fairlogger/Logger.h>
#include "TMath.h"

MUonEParticleGunBoxConfiguration::MUonEParticleGunBoxConfiguration()
    : MUonEBeamGeneratorConfiguration("Box")
{
    resetDefaultConfiguration();
}

Bool_t MUonEParticleGunBoxConfiguration::readConfiguration(YAML::Node const& config, std::string opt) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    if(0 == opt.compare("muon")) {

        m_pdg = 13;
        m_massSet = true;
        m_mass = 105.6583715 * 1.e-3;        

    } else if(0 == opt.compare("pion")) {

        m_pdg = 211;
        m_massSet = true;
        m_mass = 134.9766 * 1.e-3;
    }
    else
    {
        if(config["pdg"])
            m_pdg = config["pdg"].as<Int_t>();
        else {

            LOG(error) << "Variable 'pdg' not set.";
            m_configuredCorrectly = false;
        }

        if(config["mass"]) {
            m_mass = config["mass"].as<Double_t>();
            m_massSet = true;
        }
        else {
            m_massSet = false;
        }    
    }


    if(config["energy"]) {
        m_energy = config["energy"].as<Double_t>();
        m_energySet = true;
    }
    else {
        m_energySet = false;
    }    

    if(config["momentum"]) {
        m_momentum = config["momentum"].as<Double_t>();
        m_momentumSet = true;
    }
    else {
        m_momentumSet = false;
    }      

    if(config["z"])
        m_z = config["z"].as<Double_t>();
    else {

        LOG(error) << "Variable 'z' not set.";
        m_configuredCorrectly = false;
    }

    if(config["xMin"])
        m_xMin = config["xMin"].as<Double_t>();
    else {

        LOG(error) << "Variable 'xMin' not set.";
        m_configuredCorrectly = false;
    }

    if(config["xMax"])
        m_xMax = config["xMax"].as<Double_t>();
    else {

        LOG(error) << "Variable 'xMax' not set.";
        m_configuredCorrectly = false;
    }

    if(config["yMin"])
        m_yMin = config["yMin"].as<Double_t>();
    else {

        LOG(error) << "Variable 'yMin' not set.";
        m_configuredCorrectly = false;
    }

    if(config["yMax"])
        m_yMax = config["yMax"].as<Double_t>();
    else {

        LOG(error) << "Variable 'yMax' not set.";
        m_configuredCorrectly = false;
    }

    if(config["thetaMin"])
        m_thetaMin = config["thetaMin"].as<Double_t>();
    else {

        LOG(error) << "Variable 'thetaMin' not set.";
        m_configuredCorrectly = false;
    }

    if(config["thetaMax"])
        m_thetaMax = config["thetaMax"].as<Double_t>();
    else {

        LOG(error) << "Variable 'thetaMax' not set.";
        m_configuredCorrectly = false;
    }

    
    if(m_energySet && m_momentumSet) {

        LOG(error) << "Momentum and energy set at the same time.";
        m_configuredCorrectly = false;

    } else if(m_energySet && !m_massSet) {

        LOG(error) << "Energy set, but mass not set. Can't calculate momentum!";
        m_configuredCorrectly = false;

    } else if(m_momentumSet && !m_massSet) {

        LOG(info) << "Momentum set, but mass not set. Can't calculate energy. Using vertex generator impossible.";
    }

    if(!m_energySet && m_momentumSet && m_massSet) {

        m_energySet = true;
        m_energy = TMath::Sqrt(m_momentum*m_momentum + m_mass*m_mass);
    }

    if(!m_momentumSet && m_energySet && m_massSet) {

        m_momentumSet = true;
        m_momentum = TMath::Sqrt(m_energy*m_energy - m_mass*m_mass);
    }

    if(!m_configuredCorrectly)
        LOG(info) << "ParticleGunBox configuration contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEParticleGunBoxConfiguration::logCurrentConfiguration() const {

    LOG(info) << "pdg: " << m_pdg;
    if(m_massSet)
        LOG(info) << "mass: " << m_mass << " GeV";
    if(m_energySet)
        LOG(info) << "energy: " << m_energy << " GeV";
    if(m_momentumSet)
        LOG(info) << "momentum: " << m_momentum << " GeV";
    LOG(info) << "z: " << m_z << " cm";
    LOG(info) << "xMin: " << m_xMin << " cm";
    LOG(info) << "xMax: " << m_xMax << " cm";
    LOG(info) << "yMin: " << m_yMin << " cm";
    LOG(info) << "yMax: " << m_yMax << " cm";        
    LOG(info) << "thetaMin: " << m_thetaMin << " cm";
    LOG(info) << "thetaMax: " << m_thetaMax << " cm";
}


void MUonEParticleGunBoxConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;
    m_pdg = 0;

    m_massSet = false;
    m_mass = 0;

    m_energySet = false;
    m_energy = 0;

    m_momentumSet = false;
    m_momentum = 0;

    m_z = 0;
    m_xMin = 0;
    m_xMax = 0;
    m_yMin = 0;
    m_yMax = 0;

    m_thetaMin = 0;
    m_thetaMax = 0; 
}

ClassImp(MUonEParticleGunBoxConfiguration)