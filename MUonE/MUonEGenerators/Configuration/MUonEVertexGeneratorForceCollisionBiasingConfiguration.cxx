#include "MUonEVertexGeneratorForceCollisionBiasingConfiguration.h"

#include <fairlogger/Logger.h>

MUonEVertexGeneratorForceCollisionBiasingConfiguration::MUonEVertexGeneratorForceCollisionBiasingConfiguration()
    : MUonEGeneratorConfiguration("ForceCollisionBiasing")
{
    resetDefaultConfiguration();
}

Bool_t MUonEVertexGeneratorForceCollisionBiasingConfiguration::readConfiguration(YAML::Node const& config, std::string opt) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    if(config["particlesAndProcessesToBias"]) {

        auto const& list_node = config["particlesAndProcessesToBias"];

        m_processesToBias.resize(list_node.size());

        for(Int_t i = 0; i < list_node.size(); ++i) {

            auto const& node = list_node[i];

            if(node.size() < 2) {
                LOG(fatal) << "One of particle-processes pairs (index " + std::to_string(i) + ") in ForceCollisionBiasing is missing either particle name or processes.";
                m_configuredCorrectly = false;
                break;
            }

            if(node[1].size() <= 0) {
                LOG(fatal) << "The processes list in ForceCollisionBiasing configuration (index " + std::to_string(i) + ") is empty";
                m_configuredCorrectly = false;
                break;                
            }

            m_particlesToBias.push_back(node[0].as<std::string>());
            for(auto const& ptb : node[1])
                m_processesToBias[i].push_back(ptb.as<std::string>());
        }
    }

    if(m_particlesToBias.empty()) {
        LOG(fatal) << "ForceCollisionBiasing selected, but 'particlesToBias' empty";
        m_configuredCorrectly = false;
    }




    if(!m_configuredCorrectly)
        LOG(info) << "ForceCollisionBiasing configuration contains errors. Please fix them and try again.";


    return m_configuredCorrectly;
}

void MUonEVertexGeneratorForceCollisionBiasingConfiguration::logCurrentConfiguration() const {

	LOG(info) << "particles and processes to bias: ";
    for(Int_t i = 0; i < m_particlesToBias.size(); ++i) {

        LOG(info) << "particle: " << m_particlesToBias[i];
        LOG(info) << "processes: ";
        for(auto const& ptb : m_processesToBias[i])
            LOG(info) << ptb;
    }

}


void MUonEVertexGeneratorForceCollisionBiasingConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

	m_particlesToBias.clear();
    m_processesToBias.clear();
 
}

ClassImp(MUonEVertexGeneratorForceCollisionBiasingConfiguration)

