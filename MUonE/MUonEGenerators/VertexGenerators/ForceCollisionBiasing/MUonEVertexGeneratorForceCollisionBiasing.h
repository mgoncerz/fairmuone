#ifndef MUONEVERTEXGENERATORFORCECOLLISIONBIASING_H
#define MUONEVERTEXGENERATORFORCECOLLISIONBIASING_H

#include "MUonEVertexGenerator.h"
#include "MUonEVertexGeneratorForceCollisionBiasingConfiguration.h"


class MUonEVertexGeneratorForceCollisionBiasing : public MUonEVertexGenerator {

public:

    MUonEVertexGeneratorForceCollisionBiasing();
    ~MUonEVertexGeneratorForceCollisionBiasing();

    void attachOperatorToVolume(std::string name);

    virtual Bool_t setConfiguration(const MUonEGeneratorConfiguration* config) override;
    virtual void logCurrentConfiguration() const override;

    virtual void resetDefaultConfiguration() override;

    virtual Bool_t Init() {return true;}
    virtual Bool_t Finalize(Int_t numberOfEvents = 0, Int_t numberOfVertices = 0) {return true;}

private:

    virtual Int_t generateOutgoingParticlesKinematics(Double_t beamEnergy, TVector3 const& beamMomentum, std::vector<Int_t>& outgoingPDG, std::vector<TVector3>& outgoingMomenta) override;


    MUonEVertexGeneratorForceCollisionBiasingConfiguration m_configuration;

    ClassDef(MUonEVertexGeneratorForceCollisionBiasing, 2)
};


#endif//MUONEVERTEXGENERATORFORCECOLLISIONBIASING_H

