#include "MUonEForceCollisionBiasingOperator.h"

#include "G4ParticleTable.hh"
#include <fairlogger/Logger.h>

#include "G4VProcess.hh"

MUonEForceCollisionBiasingOperator::MUonEForceCollisionBiasingOperator(std::vector<std::string> const& particlesToBias) 
    : G4VBiasingOperator("MUonEForceCollisionBiasingOperator")
{

    for(auto const& p : particlesToBias) {

        auto const part = G4ParticleTable::GetParticleTable()->FindParticle(p);

        if(!part) {

            LOG(fatal) << "Biasing operator: particle " << p << " not found.";
        
        } else {

            m_particleOperatorMap.emplace(std::make_pair(part, new G4BOptrForceCollision(part, "ForceCollisionBiasing_" + p)));
        }
    }
}

G4VBiasingOperation* MUonEForceCollisionBiasingOperator::ProposeNonPhysicsBiasingOperation(const G4Track* track, const G4BiasingProcessInterface* callingProcess)
{
    if(m_currentOperator) 
        return m_currentOperator->GetProposedNonPhysicsBiasingOperation(track, callingProcess);
    else
        return nullptr;
}

G4VBiasingOperation* MUonEForceCollisionBiasingOperator::ProposeOccurenceBiasingOperation(const G4Track* track, const G4BiasingProcessInterface* callingProcess)
{
    if(m_currentOperator) 
        return m_currentOperator->GetProposedOccurenceBiasingOperation(track, callingProcess);
    else
        return nullptr;
}


G4VBiasingOperation* MUonEForceCollisionBiasingOperator::ProposeFinalStateBiasingOperation(const G4Track* track, const G4BiasingProcessInterface* callingProcess)
{
    if(m_currentOperator) 
        return m_currentOperator->GetProposedFinalStateBiasingOperation(track, callingProcess);
    else
        return nullptr;
}

void MUonEForceCollisionBiasingOperator::StartTracking(const G4Track* track)
{

    m_currentOperator = nullptr;


    auto process = track->GetCreatorProcess();

    if(1 == track->GetTrackID() || (/*1 == track->GetParentID() && */process && 0 == process->GetProcessType())) {

        const G4ParticleDefinition* definition = track->GetParticleDefinition();
        auto it = m_particleOperatorMap.find(definition);
        if (it != m_particleOperatorMap.end()) {

            m_currentOperator = (*it).second;
            m_currentOperator->StartTracking(track);
        }
    }


}


void MUonEForceCollisionBiasingOperator::OperationApplied(const G4BiasingProcessInterface* callingProcess,
                  G4BiasingAppliedCase biasingCase,
                  G4VBiasingOperation* operationApplied,
                  const G4VParticleChange* particleChangeProduced)
{
  if(m_currentOperator) m_currentOperator->ReportOperationApplied(callingProcess, biasingCase, operationApplied, particleChangeProduced);
}

void MUonEForceCollisionBiasingOperator::OperationApplied(const G4BiasingProcessInterface* callingProcess,
                  G4BiasingAppliedCase biasingCase,
                  G4VBiasingOperation* occurenceOperationApplied,
                  G4double weightForOccurenceInteraction,
                  G4VBiasingOperation* finalStateOperationApplied, 
                  const G4VParticleChange* particleChangeProduced)
{
  if(m_currentOperator) m_currentOperator->ReportOperationApplied(callingProcess, biasingCase, occurenceOperationApplied, weightForOccurenceInteraction, finalStateOperationApplied, particleChangeProduced);
}

void MUonEForceCollisionBiasingOperator::ExitBiasing( const G4Track* track, const G4BiasingProcessInterface* callingProcess)
{
  if(m_currentOperator) m_currentOperator->ExitingBiasing(track, callingProcess);
}

ClassImp(MUonEForceCollisionBiasingOperator)



