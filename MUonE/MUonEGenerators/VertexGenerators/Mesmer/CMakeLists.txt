include(ExternalProject)
include(FetchContent)

find_program(MAKE_EXE NAMES make)

option(MESMER_PATH "Path to Mesmer source for docker builds, if not set the version set in CMakeLists will be fetched automatically")

if(MESMER_PATH)

message("Mesmer source path set, using repository from:")
message(${MESMER_PATH})

ExternalProject_Add(mesmer_build
  SOURCE_DIR ${MESMER_PATH}
  CONFIGURE_COMMAND ""
  BUILD_IN_SOURCE TRUE
  BUILD_COMMAND     ${MAKE_EXE}
  INSTALL_COMMAND ""
)

else()

message("Mesmer source path not set, downloading from repository.")

ExternalProject_Add(mesmer_build
  GIT_REPOSITORY    https://gitlab.cern.ch/muesli/nlo-mc/mesmer-dev.git
  GIT_TAG           v1.2.0-dev
  CONFIGURE_COMMAND ""
  BUILD_IN_SOURCE TRUE
  BUILD_COMMAND     ${MAKE_EXE}
  INSTALL_COMMAND ""
)

endif()
unset(MESMER_PATH CACHE)

ExternalProject_Get_Property(mesmer_build SOURCE_DIR)
add_library(mesmer_library STATIC IMPORTED)
set_target_properties(mesmer_library PROPERTIES IMPORTED_LOCATION ${SOURCE_DIR}/libmesmerfull.a)


FetchContent_Declare(
  mesmer_interface
  GIT_REPOSITORY https://gitlab.cern.ch/muesli/nlo-mc/mue.git
  GIT_TAG        v2.1.2
)

FetchContent_MakeAvailable(mesmer_interface)

file(RELATIVE_PATH intrel ${CMAKE_CURRENT_SOURCE_DIR} ${mesmer_interface_SOURCE_DIR}/code/)

set(target Mesmer)

set(sources 
${intrel}/MuEtree.cc
${intrel}/Utils.cc
${intrel}/ElasticState.cc
)

list(APPEND headers
${intrel}/Mesmer.h
${intrel}/MuEtree.h
${intrel}/Utils.h
${intrel}/ElasticState.h
)

add_library(${target} SHARED ${sources} ${headers})
add_library(MUonE::${target} ALIAS ${target})
set_target_properties(${target} PROPERTIES ${PROJECT_LIBRARY_PROPERTIES})

target_include_directories(${target} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    ${intrel}
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>

    # TODO: DELETE ME ONCE USING root targets
    ${ROOT_INCLUDE_DIR}
)

target_link_directories(${target} PUBLIC
    ${ROOT_LIBRARY_DIR}
)

target_link_libraries(${target} PUBLIC

    Core
    Physics # TLorentzVector, TVector3
    MathCore # TMath
    mesmer_library
    m
    gfortran
    quadmath
)

add_dependencies(${target} mesmer_build)


fairroot_target_root_dictionary(${target}
    HEADERS ${headers}
    LINKDEF ${intrel}/MuEtreeLinkDef.h
)

install(TARGETS ${target} LIBRARY DESTINATION ${PROJECT_INSTALL_LIBDIR})
install(FILES ${headers} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
