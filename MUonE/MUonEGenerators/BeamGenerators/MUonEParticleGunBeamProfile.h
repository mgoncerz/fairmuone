#ifndef MUONEPARTICLEGUNBEAMPROFILE_H
#define MUONEPARTICLEGUNBEAMPROFILE_H

#include "MUonEBeamGenerator.h"
#include "MUonEBeamGeneratorConfiguration.h"
#include "MUonEParticleGunBeamProfileConfiguration.h"

#include "MUonEBeamProfile.h"

class MUonEParticleGunBeamProfile : public MUonEBeamGenerator {

public:

	MUonEParticleGunBeamProfile();

    virtual Bool_t setConfiguration(const MUonEBeamGeneratorConfiguration* config) override;
    virtual void logCurrentConfiguration() const override;

    virtual Bool_t generateBeam(Int_t& pdg, Double_t& energy, TVector3& beamPosition, TVector3& beamMomentum) override;
    virtual Double_t getNominalBeamEnergy() const override;

    virtual void resetDefaultConfiguration() override;


private:

    MUonEParticleGunBeamProfileConfiguration m_configuration;

    MUonEBeamProfile m_beamProfile;

	ClassDef(MUonEParticleGunBeamProfile,2)

};



#endif //MUONEPARTICLEGUNBEAMPROFILE_H 

