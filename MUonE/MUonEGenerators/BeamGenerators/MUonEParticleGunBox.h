#ifndef MUONEPARTICLEGUNBOX_H
#define MUONEPARTICLEGUNBOX_H

#include "MUonEBeamGenerator.h"
#include "MUonEParticleGunBoxConfiguration.h"
#include "MUonEBeamGeneratorConfiguration.h"

class MUonEParticleGunBox : public MUonEBeamGenerator {

public:

    MUonEParticleGunBox();

    virtual Bool_t setConfiguration(const MUonEBeamGeneratorConfiguration* config) override;
    virtual void logCurrentConfiguration() const override;

    virtual Bool_t generateBeam(Int_t& pdg, Double_t& energy, TVector3& beamPosition, TVector3& beamMomentum) override;
    virtual Double_t getNominalBeamEnergy() const override;

    virtual void resetDefaultConfiguration() override;


private:

    MUonEParticleGunBoxConfiguration m_configuration;

    ClassDef(MUonEParticleGunBox, 2)

};

#endif //MUONEPARTICLEGUNBOX_H

