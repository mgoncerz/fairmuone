#ifndef MUONEGEANTCONFIG_H
#define MUONEGEANTCONFIG_H

/*
  Setup Geant4 configuration based on Yaml files.
  Based on FairYamlVMCConfig.
*/

#include "TString.h"

#include "FairGenericVMCConfig.h"
#include <fairlogger/Logger.h>

#include <yaml-cpp/yaml.h>

#include <vector>
#include <string>

class MUonEGeantConfiguration : public FairGenericVMCConfig {


public:

  MUonEGeantConfiguration(TString config_name = "", Bool_t saveMCTracks = true, Bool_t saveSignalTracks = true, Bool_t savePileupTracks = true, std::vector<std::string> particlesToBias = {}, std::vector<std::vector<std::string>> processesToBias = {});
  virtual ~MUonEGeantConfiguration() = default;

  virtual void Setup(const char* mcEngine);
  virtual void SetupPostInit(const char* mcEngine);

private:

  Bool_t m_saveMCTracks{true};
  Bool_t m_saveSignalTracks{true};
  Bool_t m_savePileupTracks{true};

  std::string fMCEngine;
  TString m_configName;

  void SetupGeant4();
  virtual void SetupStack();
  void SetCuts();

  YAML::Node m_yamlConfig;

  std::vector<std::string> m_particlesToBias;
  std::vector<std::vector<std::string>> m_processesToBias;

  ClassDef(MUonEGeantConfiguration, 2)
};

#endif //MUONEGEANTCONFIG_H 


