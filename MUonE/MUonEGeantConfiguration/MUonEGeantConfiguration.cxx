
#include "MUonEGeantConfiguration.h"
#include "MUonEGeantRunConfiguration.h"

#include "MUonEStack.h"

#include "FairFastSimRunConfiguration.h"
#include "TG4RunConfiguration.h"
#include <fairlogger/Logger.h>
#include "FairRunSim.h"
#include "FairSink.h"   // for FairSink

#include <TString.h>
#include <Rtypes.h>
#include <TGeant3.h>
#include <TGeant3TGeo.h>
#include <TGeant4.h>
#include <TObjString.h>   // for TObjString
#include <TObject.h>      // for TObject, TObject::kSingleKey
#include <TString.h>
#include <TSystem.h>      // for TSystem, gSystem
#include <TVirtualMC.h>   // for TVirtualMC
#include <cstdlib>        // for getenv
#include <cstring>        // for strcmp, strncmp
#include <ostream>        // for operator<<, ostringstream
#include <string>         // for string, basic_string, cha...
#include <vector>         // for vector


MUonEGeantConfiguration::MUonEGeantConfiguration(TString config_name, Bool_t saveMCTracks, Bool_t saveSignalTracks, Bool_t savePileupTracks, std::vector<std::string> particlesToBias, std::vector<std::vector<std::string>> processesToBias)
    : FairGenericVMCConfig(), fMCEngine(""), m_configName(config_name), m_saveMCTracks(saveMCTracks), m_saveSignalTracks(saveSignalTracks), m_savePileupTracks(savePileupTracks), m_particlesToBias(particlesToBias), m_processesToBias(processesToBias)
    {}


void MUonEGeantConfiguration::Setup(const char* mcEngine)
{

    //we're only be working with Geant4 anyway
    if (0 != strcmp(mcEngine, "TGeant4")) {
        LOG(fatal) << "FairYamlVMCConfig::Setup() Engine \"" << mcEngine << "\" unknown!";
        return;
    }

    TString config_dir = getenv("GEANTCONFIGPATH");
    config_dir.ReplaceAll("//", "/"); //taken from FR examples, not sure if necessary
    if(!config_dir.EndsWith("/"))
        config_dir.Append("/");  


    if(0 != m_configName.Length()) {

        if(!m_configName.EndsWith(".yaml") && !m_configName.EndsWith(".YAML"))
            m_configName.Append(".yaml");      

        try {

            m_yamlConfig = YAML::LoadFile(m_configName.Data());
            LOG(info) << "Geant configuration file found in the path provided.";

        } catch(...) {

            TString full_path = config_dir + m_configName;

            LOG(info) << "Geant configuration file in the given path not found or failed to open. Trying in GEANTCONFIGPATH directory (" << full_path << ").";

            try {

                m_yamlConfig = YAML::LoadFile(full_path.Data());
                LOG(info) << "File found.";

            } catch(...) {

                LOG(fatal) << "Failed to load geant configuration.";
                return;
            }
        }    

    } else {

        m_configName = config_dir + "geant4DefaultConfiguration.yaml";
        LOG(info) << "Geant configuration not provided, using default configuration: " << m_configName;


        try {

            m_yamlConfig = YAML::LoadFile(m_configName.Data());
            LOG(info) << "Default configuration found.";

        } catch(...) {

            LOG(fatal) << "Failed to load default geant configuration. Is variable GEANTCONFIGPATH set correctly?";
            return;

        }   
    }

    LOG(info) << "Using geant4 configuration: " << m_configName.Data();

    //we're reading post init configuration from the same file
    if(m_yamlConfig["Geant4_PostInit_Commands"]) {

        fPostInitName = m_configName;
        fPostInitFlag = true;
    }

    SetupGeant4();
    SetupStack();
    SetCuts();
}


void MUonEGeantConfiguration::SetupPostInit(const char* mcEngine)
{
    if (!fPostInitFlag) {
        LOG(info) << "FairYamlVMCConfig::SetupPostInit() OFF.";
        return;
    }

    if (0 != strcmp(mcEngine, "TGeant4")) {
        LOG(fatal) << "FairYamlVMCConfig::SetupPostInit() only valid for TGeant4.";
        return;
    }


    LOG(info) << "Running post initialization setup.";

    if (m_yamlConfig["Geant4_PostInit_Commands"]) {

        std::vector<std::string> g4commands = m_yamlConfig["Geant4_PostInit_Commands"].as<std::vector<std::string>>();

        for ( const auto& value: g4commands ) {

            LOG(info) << " execute command \"" << value;
            TGeant4* geant4 = dynamic_cast<TGeant4*>(TVirtualMC::GetMC());
            geant4->ProcessGeantCommand(value.data());
        }
    }
}


void MUonEGeantConfiguration::SetupGeant4()
{
    LOG(info) << "MUonEGeantConfiguration::SetupGeant4() called";

    if (!m_yamlConfig["Geant4_UserGeometry"]) {
        LOG(fatal) << "User geometry not provided";
    }
    if (!m_yamlConfig["Geant4_PhysicsList"]) {
        LOG(fatal) << "Physics list not provided";
    }
    if (!m_yamlConfig["Geant4_SpecialProcess"]) {
        LOG(fatal) << "Special processy not provided";
    }

    bool specialStacking = false;
    if (m_yamlConfig["Geant4_SpecialStacking"])
        specialStacking = m_yamlConfig["Geant4_SpecialStacking"].as<bool>();

    bool mtMode = FairRunSim::Instance()->IsMT();

    if (m_yamlConfig["Geant4_Multithreaded"]) 
        mtMode = m_yamlConfig["Geant4_Multithreaded"].as<bool>();

    TG4RunConfiguration* run_configuration;

    if(m_yamlConfig["UseFastSim"] && true == m_yamlConfig["UseFastSim"].as<Bool_t>()) 
        run_configuration = new FairFastSimRunConfiguration(m_yamlConfig["Geant4_UserGeometry"].as<std::string>(),
                                            m_yamlConfig["Geant4_PhysicsList"].as<std::string>(),
                                            m_yamlConfig["Geant4_SpecialProcess"].as<std::string>(),
                                            specialStacking,
                                            mtMode);
    else
        run_configuration = new MUonEGeantRunConfiguration(m_yamlConfig["Geant4_UserGeometry"].as<std::string>(),
                                            m_yamlConfig["Geant4_PhysicsList"].as<std::string>(),
                                            m_yamlConfig["Geant4_SpecialProcess"].as<std::string>(),
                                            specialStacking,
                                            mtMode,
                                            m_particlesToBias,
                                            m_processesToBias);        
    

    TGeant4* geant4 = new TGeant4("TGeant4", "The Geant4 Monte Carlo", run_configuration);

    if (m_yamlConfig["Geant4_MaxNStep"]) {

        LOG(info) << " execute SetMaxNStep (" << m_yamlConfig["Geant4_MaxNStep"].as<int>() << ")";    
        geant4->SetMaxNStep(m_yamlConfig["Geant4_MaxNStep"].as<int>());
    }

    if (m_yamlConfig["Geant4_Commands"]) {

        std::vector<std::string> g4commands = m_yamlConfig["Geant4_Commands"].as<std::vector<std::string>>();

        for ( const auto& value: g4commands ) {

            LOG(info) << " execute command \"" << value;
            geant4->ProcessGeantCommand(value.data());
        }
    }

    LOG(info) << geant4->GetName() << " MonteCarlo engine created!.";
}


void MUonEGeantConfiguration::SetupStack()
{
    MUonEStack *stack = new MUonEStack(1000, m_saveMCTracks, m_saveSignalTracks, m_savePileupTracks);

    //parse and set any stack related configuration here, currently there's none

    TVirtualMC::GetMC()->SetStack(stack);
}

void MUonEGeantConfiguration::SetCuts()
{
    if (m_yamlConfig["MonteCarlo_Process"]) {

        YAML::Node mcProcess = m_yamlConfig["MonteCarlo_Process"];
        TVirtualMC* MC = TVirtualMC::GetMC();

        for (auto it = mcProcess.begin(); it != mcProcess.end(); ++it)
            MC->SetProcess(it->first.as<std::string>().c_str(), it->second.as<int>());
    }

    if (m_yamlConfig["MonteCarlo_Cut"]) {

        YAML::Node mcProcess = m_yamlConfig["MonteCarlo_Cut"];
        TVirtualMC* MC = TVirtualMC::GetMC();

        for (auto it = mcProcess.begin(); it != mcProcess.end(); ++it)
            MC->SetCut(it->first.as<std::string>().c_str(), it->second.as<double>());
    }
}

ClassImp(MUonEGeantConfiguration);


