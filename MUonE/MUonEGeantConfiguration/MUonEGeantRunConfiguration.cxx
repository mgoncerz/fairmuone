
#include "MUonEGeantRunConfiguration.h"

#include "TG4ComposedPhysicsList.h"
#include "G4GenericBiasingPhysics.hh"

#include "TG4ExtraPhysicsList.h"
#include "TG4Globals.h"
#include "TG4EmPhysicsList.h"
#include "TG4ExtraPhysicsList.h"
#include "TG4HadronPhysicsList.h"
#include "TG4SpecialPhysicsList.h"

#include "G4PhysListFactory.hh"

#include <fairlogger/Logger.h>

MUonEGeantRunConfiguration::MUonEGeantRunConfiguration(const TString& userGeometry,
    const TString& physicsList,
    const TString& specialProcess,
    Bool_t specialStacking, Bool_t mtApplication,
    std::vector<std::string> const& particlesToBias,
    std::vector<std::vector<std::string>> const& processesToBias)
        : TG4RunConfiguration(userGeometry, physicsList, specialProcess, specialStacking, mtApplication),
        m_particlesToBias(particlesToBias), m_processesToBias(processesToBias)
        {}



G4VUserPhysicsList* MUonEGeantRunConfiguration::CreatePhysicsList() {

  TG4ComposedPhysicsList* builder = new TG4ComposedPhysicsList();

  G4PhysListFactory phys_list_factory;
  G4String phys_list_name(fPhysicsListSelection); 

  if(!phys_list_factory.IsReferencePhysList(phys_list_name)) {

    LOG(fatal) << "Physics list: " + phys_list_name + " is not a reference list";
    return nullptr;
  }

  auto phys_list = phys_list_factory.GetReferencePhysList(phys_list_name);

  if(!m_particlesToBias.empty()) {

    G4GenericBiasingPhysics* biasingPhysics = new G4GenericBiasingPhysics();

    for(Int_t i = 0; i < m_particlesToBias.size(); ++i) {

      auto particle_name = G4String(m_particlesToBias[i]);
      auto processes = std::vector<G4String>(m_processesToBias[i].begin(), m_processesToBias[i].end());

      biasingPhysics->Bias(particle_name, processes);
      LOG(info) << "Biasing enabled in physics list for particle " + particle_name + " and processes:";
      for(auto const& p : processes)
        LOG(info) << p;
    }

    phys_list->RegisterPhysics(biasingPhysics);   
  }
  

  builder->AddPhysicsList(phys_list);

  G4cout << "Adding SpecialPhysicsList " << fSpecialProcessSelection.Data()
         << G4endl;
  builder->AddPhysicsList(
    new TG4SpecialPhysicsList(fSpecialProcessSelection.Data()));

  return builder;

}


ClassImp(MUonEGeantRunConfiguration);


